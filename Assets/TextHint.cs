﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TextHint : EventTrigger {

    public string text;

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        BattleUI.instance.ShowHint(GetComponent<RectTransform>(), text);
    }
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        BattleUI.instance.HideHint();
    }

    public void OnEnter()
    {
        BattleUI.instance.ShowHint(GetComponent<RectTransform>(), text);
    }
    public void OnExit()
    {
        BattleUI.instance.HideHint();
    }
}
