﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

class SpellPanel : BattleUIBase
{
    public Text manaText;
    public AbstractGrid grid;
    
    public void Init(PlayerState state)
    {
        manaText.SetContent("Mana: {0}/" + settings.ManaMax, state.mana);
        grid.Link(gameData.spells, "SpellButton", (spell, viewObj) =>
        {
            var view = viewObj as SpellButton;
            view.name.text = spell.name;
            view.cost.text = "Mana: " + spell.manaCost;
            var available =
                from m in state.mana
                from u in BattleUI.instance.currentSelection
                select m >= spell.manaCost && u != null
                       && ((state.side != u.side) ^ (spell.targetType == SpellTargetType.AllyUnit) 
                       || spell.targetType == SpellTargetType.AnyUnit);
            view.connections.add = view.button.SetInteractable(available);
            view.connections.add = view.button.ClickStream()
                .Listen(() =>
                {
                    if (BattleUI.instance.currentSelection.value.gameObject == null) return;
                    state.CmdCastSpell(gameData.spells.IndexOf(spell),
                        BattleUI.instance.currentSelection.value.gameObject);
                });
        });
    }
}
