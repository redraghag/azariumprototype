﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class StatsView : BattleUIBase
{
    private ConnectionCollector connections = new ConnectionCollector();
    public Image attackType;
    public Image defenceType;
    public Text attackValue;
    public Text armorValue;
    public Text attackSpeed;
    public GameObject statsNode;
    public Image attackSpeedIcon;
    public Text hpValue;
    public Image hpIcon;

    public Text abilityDescription;

    public GameObject azariumNode;
    public List<AzariumButton> azButtons;
    public IntCell azSelect = new IntCell(0);
    public Stream<int> azUpgradeRequest = new Stream<int>();
    public GameObject costDescNode;
    public Text azUpText;
    public bool demoView;

    string AttackDescFromTimeToCast(float time)
    {
        if (time >= 3) return "Very Slow";
        else if (time >= 2) return "Slow";
        else if (time >= 1.5) return "Normal";
        else if (time >= 1) return "Fast";
        else return "Very Fast";
    }

    public void Init(BuildingInfo info)
    {
        string abilDesc = "";
        foreach (var abil in info.abilityDescriptionShort)
        {
            abilDesc += "* " + abil + "\n\n";
        }
        if (abilDesc.Length > 3)
            abilDesc = abilDesc.Remove(abilDesc.Length - 2, 2);
        abilityDescription.text = abilDesc;

        if (string.IsNullOrEmpty(info.unitToSpawn))
        {
            statsNode.SetActiveSafe(false);
        }
        else
        {
            statsNode.SetActiveSafe(true);
            UnitInfo uinfo = gameData.uInfo[info.unitToSpawn]; 
            attackType.sprite = UIRes.instance.attackSprites[(int)uinfo.damageType];
            attackType.GetComponent<TextHint>().text = TextForAttack(uinfo.damageType);
            defenceType.sprite = UIRes.instance.armorSprites[(int)uinfo.armorType];
            defenceType.GetComponent<TextHint>().text = TextForDefence(uinfo.armorType);
            hpValue.text = uinfo.hp.ToString();
            attackSpeedIcon.GetComponent<TextHint>().text = "Attack speed";
            hpIcon.GetComponent<TextHint>().text = "Health";

            attackValue.text = uinfo.baseDamage.ToString();
            armorValue.text = uinfo.armor.ToString();

            attackSpeed.text =
                AttackDescFromTimeToCast((uinfo.actions.First(a => a is SimpleStayStillAction) as SimpleStayStillAction).timeToCast)
            ;
        }

        if (info.azariupUpgradeDecription.Count == 0)
        {
            //azariumNode.SetActiveSafe(false);   
            for (int i = 0; i < 2; i++)
            {
                azButtons[i].SetActiveSafe(false);
            } 
            costDescNode.SetActiveSafe(false);
        }
        else
        {
            costDescNode.SetActiveSafe(true);
            //azariumNode.SetActiveSafe(true);
            for (int i = 0; i < info.azariupUpgradeDecription.Count; i++)
            {
                azButtons[i].SetActiveSafe(true);
                azButtons[i].text.text = info.azariupUpgradeDecriptionShort[i];
                azButtons[i].SetState(demoView ? AzarioumButtonState.Normal : AzarioumButtonState.Locked);
            }
        }
    }

    public void DisableAzButtons()
    {
        for (int i = 0; i < 2; i++)
        {
            azButtons[i].button.interactable = false;
        } 
    }

    string TextForAttack(DamageType type)
    {
        return string.Format(
            "{0}\n" +
            "Deals {1} % damage to light armor\n" +
            "Deals {2} % damage to magic armor\n" +
            "Deals {3} % damage to heavy armor\n"
            , "Attack type: " + type 
            , Battle.ClashDamageTypes(type, ArmorType.light) * 100
            , Battle.ClashDamageTypes(type, ArmorType.heavy) * 100
            , Battle.ClashDamageTypes(type, ArmorType.magic) * 100
            );
    }

    string TextForDefence(ArmorType type)
    {
        return string.Format(
            "{0}\n" +
            "Takes {1} % damage from light attack\n" +
            "Takes {2} % damage from magic attack\n" +
            "Takes {3} % damage from heavy attack\n"
            , "Defence type: " + type 
            , Battle.ClashDamageTypes(DamageType.light, type) * 100
            , Battle.ClashDamageTypes(DamageType.magic, type) * 100
            , Battle.ClashDamageTypes(DamageType.heavy, type) * 100
            );
    }

    IntCell selectedAzUp = new IntCell();

    public void InitAzariumState(ICell<int> state)
    {
        connections.DisconnectAll();
        selectedAzUp.value = -1;
        connections.add = state.WhateverBind((val, coll) =>
        {
            if (val == 0)
            {
                azButtons.ForeachWithIndices((b, index) =>
                {
                    b.SetState(AzarioumButtonState.Normal);
                    coll.add =
                        player.azarium.Select(a => a >= settings.AbilityAzariumCost)
                            .Bind(v => azUpText.color = v ? Color.white : Color.red);
                    //coll.add = (
                    //    from enough in player.azarium.Select(a => a >= settings.AbilityAzariumCost)
                    //    from selection in selectedAzUp
                    //    select new {enough, selection}).Bind(v =>
                    //    {
                    //        //if (v.selection == index && v.enough) b.SetState(AzarioumButtonState.ReadyToBuild);
                    //        //else if (v.selection == -1 && v.enough) b.SetState(AzarioumButtonState.CanBeBought);
                    //        //else if (v.enough == false) b.SetState(AzarioumButtonState.NotEnoughAzarium);
                    //        //else b.SetState(AzarioumButtonState.Locked);
                    //        //b.SetEnoughAz(v.enough);
                    //    });

                    coll.add = b.button.ClickStream().Listen(() =>
                    {
                        //if (selectedAzUp.value == index && player.azarium.value >= settings.AbilityAzariumCost)
                            azUpgradeRequest.Send(index + 1);
                        //else
                        //    selectedAzUp.value = index;
                    });
                });
                return;
            }
            var bought = azButtons[val - 1];
            bought.SetState(AzarioumButtonState.Bought);
            var ignored = azButtons[2 - val];
            ignored.SetState(AzarioumButtonState.Locked);
            costDescNode.SetActiveSafe(false);
        });
    }
}
