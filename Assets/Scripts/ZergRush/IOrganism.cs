﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

public interface IConnectionCollector
{
    void Collect(IDisposable connection);
}

public interface ILivingState
{
    bool isAlive { get; }
}

// Interfece implementation must guarantee disconnect all collected connections when it goes asleep.
public interface ILiving : ILivingState
{
    IOnceEmptyStream fellAsleep { get; }
    void DisposeWhenAsleep(IDisposable disposable);
}

public interface IOrganism : ILiving
{
    void Setup(IOrganism carrier, IRoot root);
    void WakeUp();
    void Spread();
    void ToSleep();
}

public interface IRoot : IOrganism
{

}

public interface IOrganism<out TCarrier, out TRoot> : IOrganism
    where TCarrier : class, IOrganism
    where TRoot : class, IRoot
{
    TCarrier carrier { get; }
    TRoot root { get; }
}

public static class OrganismExtension
{
    public static void Arise(this IOrganism self, IOrganism carrier, IRoot root)
    {
        self.Setup(carrier, root);
        self.WakeUp();
        self.Spread();
    }
}
