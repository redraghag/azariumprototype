﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;

[Serializable]
public class BioCell<T> : Cell<T>, IOrganism<IOrganism, IRoot>, ISerializable
    where T : class, IOrganism
{
    public BioCell()
    { 

    }
    public BioCell(T val)
    {
        value = val;
    }

    public BioCell(SerializationInfo info, StreamingContext context)
    {
        value = (T) info.GetValue("v", typeof(T));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("v", base.value, typeof(T));
    }

    bool wokeUp;

    [NonSerialized]
    IOrganism carrier_;
    [NonSerialized]
    IRoot root_;
    public IOrganism carrier { get { return carrier_; } set { carrier_ = value; } }
    public IRoot root { get { return root_; } set { root_ = value; } }

    [NonSerialized]
    bool alive = false;
    OnceEmptyStream fellAsleep_;
    public IOnceEmptyStream fellAsleep { get { return fellAsleep_ ?? (fellAsleep_ = new OnceEmptyStream()); } }
    public void DisposeWhenAsleep(IDisposable disposable)
    {
        Collect(disposable);
    }

    public bool isAlive { get { return alive; } }

    public virtual void Setup(IOrganism c, IRoot r)
    {
        carrier = c;
        root = r;
        if (value != null) value.Setup(c, r);
    }

    public void WakeUp()
    {
        if (value != null) value.WakeUp();
        wokeUp = true;
    }
    public void Spread()
    {
        if (value != null)
        {
            value.Spread();
            var temp = value;
            prevToSleepConnection = temp.fellAsleep.Listen(() => {
                if (this.value == temp)
                    this.value = null;
            });
        }
        alive = true;
    }

    IDisposable prevToSleepConnection_;
    IDisposable prevToSleepConnection
    {
        get { return prevToSleepConnection_; }
        set
        {
            if (value != null && prevToSleepConnection_ != null)
            {
                UnityEngine.Debug.LogError("error");
            }
            prevToSleepConnection_ = value;
        }
    }

    public override T value
    {
        get
        {
            return base.value;
        }
        set
        {
            if (implanting)
            {
                base.value = value;
                return;
            }

            T prevVal = base.value;
            if (object.Equals(prevVal, value)) return;
            if (prevVal != null)
            {
                if (prevToSleepConnection != null)
                {
                    prevToSleepConnection.Dispose();
                    prevToSleepConnection = null;
                }
                prevVal.ToSleep();
            }
            
            if (value != null)
            {
                if (carrier != null)
                {
                    value.Setup(carrier, root);
                }
                if (wokeUp)
                {
                    value.WakeUp();
                }
                if (alive)
                {
                    value.Spread();
                    var temp = value;
                    prevToSleepConnection = temp.fellAsleep.Listen(() => {
                        if (this.value == temp)
                            this.value = null;
                    });
                }
            }
            base.value = value;
        }
    }
    T innerVal { get { return base.value; } }

    bool implanting;

    public void Swap(BioCell<T> other)
    {
        var otherVal = other.value;
        var curr = value;
        
        other.implanting = true;
        if (other.prevToSleepConnection != null)
        {
            other.prevToSleepConnection.Dispose();
            other.prevToSleepConnection = null;
        }
            
        implanting = true;
        if (prevToSleepConnection != null)
        {
            prevToSleepConnection.Dispose();
            prevToSleepConnection = null;
        }
            

        other.value = curr;
        value = otherVal;

        other.implanting = false;
        if (curr != null)
        other.prevToSleepConnection = curr.fellAsleep.Listen(() => {
            if (this.value == curr)
                this.value = null;
        });

        implanting = false;
        if (otherVal != null)
            prevToSleepConnection = otherVal.fellAsleep.Listen(() => {
            if (this.value == otherVal)
                this.value = null;
        });
    }

    public void Collect(IDisposable tail)
    {
        if (innerVal != null) innerVal.DisposeWhenAsleep(tail);
        else tail.Dispose();
    }

    virtual public void ToSleep()
    {
        if (prevToSleepConnection != null)
        {
            prevToSleepConnection.Dispose();
            prevToSleepConnection = null;
        }
        if (innerVal != null) innerVal.ToSleep();
        alive = false;
        wokeUp = false;
        if (fellAsleep_ != null) fellAsleep_.Send();
    }
}
