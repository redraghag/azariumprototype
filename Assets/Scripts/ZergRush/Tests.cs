﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Tests
{
    public static void TestReactive()
    {
        //test(newData);

        Cell<int> c1 = new Cell<int>(3);
        Cell<int> c2 = new Cell<int>(7);

        Cell<ICell<int>> superCell = new Cell<ICell<int>>(c1);

        var v = from v1 in c1
                from v2 in c2
                select v1 + v2;

        //var v = superCell.Join();

        var disconnect = v.Bind(val =>
        {
            Debug.Log("val: " + val);
        });
        //var disconnect2 = v.Bind(val => Debug.Log("val2: " + val));

        Debug.Log(v.value == 10);

        c1.value = 5;
        c1.value = 30;
        Debug.Log(v.value == 37);
        c2.value = 40;
        Debug.Log(v.value == 70);

        //disconnect2.Dispose();
        c2.value = 100;
        disconnect.Dispose();

        Debug.Log(v.value == 130);

        c1.value = 666;

        Debug.Log(v.value == 766);

        c2.value = 777;

        Debug.Log(v.value == (666 + 777));

        Debug.Log("finish");
    }
}
