﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;

public class Astray<Root> : Organism<IOrganism, Root>
    where Root : class, IRoot
{

}

public class Blind<Carrier> : Organism<Carrier, IRoot>
    where Carrier : class, IOrganism
{

}

[Serializable]
public class Root<TCarrier, TRoot> : Organism<TCarrier, TRoot>, IRoot
    where TCarrier : class, IOrganism
    where TRoot : class, IRoot
{

}

[Serializable]
public class NotAChild : Attribute
{
    
}

[Serializable]
public abstract class Organism<TCarrier, TRoot> : IOrganism<TCarrier, TRoot>, IConnectionCollector
    where TCarrier : class, IOrganism
    where TRoot : class, IRoot
{
    [NonSerialized] protected ConnectionCollector tails;
    [NonSerialized] TCarrier carrier_;
    [NonSerialized] TRoot root_;
    public TCarrier carrier { get { return carrier_; } set { carrier_ = value; } }
    public TRoot root { get { return root_; } set { root_ = value; } }

    protected bool notACarrier;

    [NonSerialized] bool alive = false;
    [NonSerialized] OnceEmptyStream fellAsleep_;

    public IOnceEmptyStream fellAsleep { get { return fellAsleep_ ?? (fellAsleep_ = new OnceEmptyStream()); } }
    public void DisposeWhenAsleep(IDisposable disposable)
    {
        Collect(disposable);
    }

    public bool isAlive { get { return alive; } }

    public virtual bool primal { get { return false; } }

    public virtual void Setup(IOrganism c, IRoot r)
    {
        carrier = c as TCarrier;
        root = r as TRoot;

        if (carrier == null && c != null)
        {
            UnityEngine.Debug.LogError("invalid carrier object");
        }
        if (root == null && r != null)
        {
            UnityEngine.Debug.LogError("invalid root object");
        }
        
        RegisterChildren();
        if (children != null)
        {
            children.ForEach(child =>
            {
                IRoot ifRoot = this as IRoot;
                child.Setup(notACarrier ? c : this, ifRoot != null ? ifRoot : root);
            });
        }
    }

    public virtual void WakeUp()
    {
        children.ForEach(child =>
        {
            child.WakeUp();
        });
    }

    public virtual void Spread()
    {
        children.ForEach(child =>
        {
            child.Spread();
        });
        alive = true;
    }
    
    public T ReachCarrier<T> () 
        where T : class, IOrganism
    {
        IOrganism current = carrier;
        T target = null;
        while (target == null && current != null)
        {
            target = current as T;
            current = (current as IOrganism<IOrganism, IRoot>).carrier;
        }
        return target;
    }
    
    public void Collect(IDisposable tail)
    {
        if (tails == null)
        {
            tails = new ConnectionCollector();
        }
        tails.Add(tail);
    }

    public IDisposable collect { set {
            if (tails == null) tails = new ConnectionCollector();
            tails.Add(value);
        } }

    virtual public void ToSleep()
    {
        if (!alive) return;
        alive = false;

        if (fellAsleep_ != null)
            fellAsleep_.Send();

        if (children != null)
        {
            children.ForEach(child =>
            {
                child.ToSleep();
            });
            children.Clear();
        }

        if (tails != null)
        {
            tails.ForEach(d => d.Dispose());
            tails.Clear();
        }
    }

    [NonSerialized]
    List<IOrganism> children;

    public void Put(IOrganism organism)
    {
        if (organism == null)
        {
            UnityEngine.Debug.Log("null children alert");
            return;
        }
        if (children == null) children = new List<IOrganism>();
        children.Add(organism);
    }

    public void Take(IOrganism organism)
    {
        if (children == null)
        {
            UnityEngine.Debug.LogError("there is no children here");
        }
        bool removed = children.Remove(organism);
        if (removed)
        {
            organism.ToSleep();
        }
    }

    protected virtual IOrganism ChangeRoot(TRoot r)
    {
        return r;
    }

    protected virtual void RegisterChildren()
    {
        if (children == null) children = new List<IOrganism>();
        children.Clear();
        foreach (var field in this.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
        {
            if (typeof(IOrganism).IsAssignableFrom(field.FieldType))
            {
                if (Attribute.IsDefined(field, typeof(NotAChild))) continue;
                Put((IOrganism)field.GetValue(this));
            }
        }
    }
}

public static class OrganismWorkReactives
{
    public static void BindWith<T>(this IOrganism org, ICell<T> cell, Action<T> action)
    {
        org.DisposeWhenAsleep(cell.Bind(action));
    }

    public static void ReactOn<T>(this IOrganism org, IStream<T> stream, Action<T> action)
    {
        org.DisposeWhenAsleep(stream.Listen(action));
    }

    //public static T AddTo<T>(this T disp, IOrganism org) where T : IDisposable
    //{
    //    org.DisposeWhenAsleep(disp);
    //    return disp;
    //}
    public static T AddTo<T>(this T disp, IConnectionCollector org) where T : IDisposable
    {
        org.Collect(disp);
        return disp;
    }
}
