using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text;

// No children, no spread, no connections, no live at all, only solitude and hierarchy propagation
public class HierarchyLeaf<TCarrier, TRoot> : IOrganism<TCarrier, TRoot> 
    where TCarrier : class, IOrganism
    where TRoot : class, IRoot
{
    [NonSerialized] TCarrier carrier_;
    [NonSerialized] TRoot root_;
    public TCarrier carrier { get { return carrier_; } set { carrier_ = value; } }
    public TRoot root { get { return root_; } set { root_ = value; } }

    [NonSerialized] bool alive = false;
    [NonSerialized] OnceEmptyStream fellAsleep_;

    public IOnceEmptyStream fellAsleep { get { return fellAsleep_ ?? (fellAsleep_ = new OnceEmptyStream()); } }
    public void DisposeWhenAsleep(IDisposable disposable)
    {
        fellAsleep.Listen(disposable.Dispose);
    }

    public virtual void Setup(IOrganism c, IRoot r)
    {
        carrier = c as TCarrier;
        root = r as TRoot;

        if (carrier == null && c != null)
        {
            UnityEngine.Debug.LogError("invalid carrier object");
        }
        if (root == null && r != null)
        {
            UnityEngine.Debug.LogError("invalid root object");
        }
    }

    public void Spread()
    {
    }

    public void ToSleep()
    {
    }

    public virtual void WakeUp()
    {
    }

    public bool isAlive
    {
        get { return alive; } 
    }
}

