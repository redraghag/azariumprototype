﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEngine;
using System.Text;
using CellUtils;
using UniRx;

public interface IProcessExecuter
{
    IProcessExecution Execute(IProcess proc);
}

public interface IProcessExecution
{
    void Stop();
    bool finished { get; } 
}

public class EndedExecution : IProcessExecution
{
    public void Stop()
    {
        
    }
    public bool finished { get { return true; } }
}

public interface IProcess
{
    bool finished { get; }
    void Tick(float dt);
    void Dispose();
}

public interface IProcess<T> : IProcess
{
    T result { get; }
}

public class MetaProcessBase : IProcess, IEnumerable
{
    protected List<IProcess> children = new List<IProcess>();

    public MetaProcessBase()
    {
        children = new List<IProcess>(2);
    }

    public MetaProcessBase(IEnumerable<IProcess> coroutines)
    {
        children = coroutines.ToList();
    }
    public void Add(IProcess proc)
    {
        children.Add(proc);
    }

    public virtual bool finished
    {
        get
        {
            return children.All(c => c.finished);
        }
    }
    public virtual void Tick(float dt)
    {
        children.ForEach(c => c.Tick(dt));
    }
    public virtual void Dispose()
    {
        children.ForEach(c => c.Dispose());
    }
    public IEnumerator GetEnumerator() { return children.GetEnumerator(); }
}

public class CoroMutex : IProcess
{
    class LockProc : IProcess
    {
        public CoroMutex parent;
        public bool locked = true;
        public bool finished {get { return !locked; }}
        public void Tick(float dt){}
        public void Dispose(){ parent.RemoveLock(this);}
    }

    public Cell<bool> locked = new BoolCell();
    List<LockProc> locks = new List<LockProc>();

    void RemoveLock(LockProc proc)
    {
        Debug.Log("unlock on dispose");
        locks.Remove(proc);
        if (locks.Count > 0)
        {
            locks.First().locked = false;
        }
        else
        {
            locked.value = false;
        }
    }
    public IProcess Lock(out IDisposable unlock)
    {
        var l = new LockProc();
        l.parent = this;
        locks.Add(l);
        //Debug.Log("lock ");
        unlock = Disposable.Create(() =>
        {
            //Debug.Log("unlock");
            locks.Remove(l);
            if (locks.Count > 0)
            {
                locks.First().locked = false;
            }
            else
            {
                locked.value = false;
            }
        });
        locked.value = true;
        return locks.Count == 1 ? null : l;
    }

    public IProcess Lock(IConnectionCollector collector)
    {
        IDisposable disp;
        var l = Lock(out disp);
        collector.Collect(disp);
        return l;
    }

    public IProcess Lock()
    {
        var l = new LockProc();
        locks.Add(l);
        locked.value = true;
        return locks.Count == 1 ? null : l;
    }

    public IDisposable UnlockAsDisposable()
    {
        return Disposable.Create(() =>
        {
            UnlockCurrent();
        });
    }

    public void UnlockCurrent()
    {
        if (locks.Count == 0)
        {
            Debug.LogError("no locks");
            locked.value = false;
            return;
        }
        locks.RemoveAt(0);
        if (locks.Count > 0)
        {
            locks.First().locked = false;
        }
        else
        {
            locked.value = false;
        }
    }

    public bool finished { get { return !locked.value; } }
    public void Tick(float dt){}
    public void Dispose(){}

    public void Clear()
    {
        foreach (var @lock in locks)
        {
            @lock.locked = false;
        }
        locks.Clear();
        locked.value = false;
    }
}

class AllFinished : MetaProcessBase
{

}

class AnyFinished : MetaProcessBase
{
    public override bool finished { get { return children.Any(val => val.finished); } }
}

public class CoroProcess : IProcess
{
    IEnumerator generator;
    IProcess current;
    ILivingState owner;

    public CoroProcess(IEnumerator iterator, ILivingState inOwner)
    {
        if (owner != null && !owner.isAlive)
        {
            Debug.LogError("Coro process with not alive parent");
        }
        owner = inOwner;
        generator = iterator;
        finished = !generator.MoveNext();
        if (!finished)
        {
            current = generator.Current as IProcess;
        }
    }

    public bool finished { get; set; }


    public void Tick(float dt)
    {
        if (finished) return;
        if (owner != null && !owner.isAlive)
        {
            finished = true;
            return;
        }

        if (current != null && current.finished)
        {
            current.Dispose();
            current = null;
        }
        if (current == null)
        {
            finished = !generator.MoveNext();
            if (finished) return;
            var currentObj = generator.Current;
            if (currentObj != null)
            {
                current = currentObj as IProcess;
                if (current == null)
                {
                    UnityEngine.Debug.LogError("Something different than ICoro was yielded");
                    return;
                }
            }
        }
        if (current != null)
            current.Tick(dt);
    }
    public void Dispose()
    {
        if (current != null) current.Dispose();
    }
}

public static class CoroExtensions
{
    class ExecutionWait : IProcess
    {
        IProcessExecution exec;
        public ExecutionWait(IProcessExecution exec_)
        {
            exec = exec_;
        }
        public bool finished { get { return exec.finished; } }
        public void Tick(float dt) { }
        public void Dispose() { }
    }

    public static IProcess Wait(this IProcessExecution exec)
    {
        //if (exec is CoroProcess) return exec as CoroProcess;
        return new ExecutionWait(exec);
    }
}

public abstract class FiniteTimeProc : IProcess
{
    public float timeTotal;
    float timeLeft;

    public FiniteTimeProc(float duration)
    {
        timeTotal = duration;
        timeLeft = timeTotal;
    }
    public void Tick(float dt)
    {
        timeLeft = timeLeft - dt;
        Update(1 - timeLeft / timeTotal);
    }
    virtual public bool finished { get { return timeLeft <= 0; } }
    virtual public void Dispose() { }
    public abstract void Update(float relativeTime);
}

public class ChronoLerp : FiniteTimeProc
{
    float from;
    float to;
    Action<float> action;

    public ChronoLerp(float duration, float inFrom, float inTo, Action<float> inAction) : base(duration)
    {
        from = inFrom;
        to = inTo;
        action = inAction;
    }
    public override void Update(float relativeTime)
    {
        action(from + (to - from) * relativeTime);
    }
}

public class Pulse : FiniteTimeProc
{
    float from;
    float to;
    private float wave;
    Action<float> action;

    public Pulse(float duration, float inFrom, float inTo, float waveLength, Action<float> inAction) : base(duration)
    {
        from = inFrom;
        to = inTo;
        action = inAction;
        wave = waveLength;
    }
    public override void Update(float relativeTime)
    {
        action(Mathf.Lerp(from, to, Mathf.PingPong(relativeTime / (wave * 0.5f), 1.0f)));
    }

    public override void Dispose()
    {
        base.Dispose();
        action(0.0f);
    }
}

public class InfinitePulse : Pulse
{
    public InfinitePulse(float inFrom, float inTo, float waveLength, Action<float> inAction) : base(1.0f, inFrom, inTo, waveLength, inAction)
    {
    }

    public override bool finished { get { return false; } }
}


public abstract class TimeScalerBase : FiniteTimeProc
{
    protected FiniteTimeProc proc;
    public TimeScalerBase(FiniteTimeProc inProc) : base(inProc.timeTotal)
    {
        proc = inProc;
    }
    //public override bool finished { get { return proc.finished; } }
    public override void Dispose() { proc.Dispose(); }
}

public class Elastic : TimeScalerBase
{
    public Elastic(FiniteTimeProc inProc) : base(inProc) { }
    public override void Update(float t)
    {
        var p = 0.3F;
        proc.Update(Mathf.Pow(2, -10 * t) * Mathf.Sin((t - p / 4) * (2 * Mathf.PI) / p) + 1);
    }
}


public class Executer : IProcessExecuter, IDisposable
{
    protected LinkedList<IProcess> bearutines = new LinkedList<IProcess>();
    protected Action disposeAction = null;
    
    public  IProcessExecution Execute(IEnumerator bearutine, ILivingState owner, Action dispose = null)
    {

        var coro = new CoroProcess(bearutine, owner);

        if (bearutine == null)
        {
            Debug.Log("Wtf?");
        }

        bearutines.AddLast(coro);
        disposeAction = dispose;
        return new Execution { coro = coro, parent = this };
    }

    public  IProcessExecution Execute(IProcess bearutine)
    {
        if (bearutine == null)
        {
            Debug.Log("Wtf?");
        }

        bearutines.AddLast(bearutine);
        return new Execution { coro = bearutine, parent = this };
    }

    public IProcessExecution Delay(Action act, float time, ILivingState owner = null)
    {
        return Execute(DelayCoro(act, time), owner);
    }

    IEnumerator DelayCoro(Action act, float time)
    {
        yield return new BattleWait(time);
        act();
    }

    public void Terminate(IProcess coro)
    {
        if (coro == null) return;
        coro.Dispose();
        bearutines.Remove(coro);
    }

    protected class Execution : IProcessExecution
    {
        public IProcess coro;
        public Executer parent;
        public void Stop()
        {
            parent.Terminate(coro);
            coro = null;
        }

        public bool finished
        {
            get
            {
                if (coro == null)
                {
                    Debug.Log("fuck");
                }
                return coro.finished;
            }
        }
    }

    public virtual void Tick(float dt)
    {
        if (bearutines.Count == 0) return;
        var node = bearutines.First;
        while(true)
        {
            var co = node.Value;
            co.Tick(dt);
            if (co.finished)
            {
                co.Dispose();
                bearutines.Remove(co);
            }
            if (node.Next == null) return;
            node = node.Next;
        }
    }

    public bool empty { get { return bearutines.Count == 0; } }

    public void Dispose()
    {
        while (bearutines.Count > 0)
        {
            var node = bearutines.First.Value;
            node.Dispose();
            bearutines.Remove(node);
        }

        if (disposeAction != null)
            disposeAction();

        disposeAction = null;
    }
}


public class IgnorantExecuter : Executer
{
    public override void Tick(float dt)
    {
        if (bearutines.Count == 0) return;
        var node = bearutines.First;
        while (true)
        {
            var co = node.Value;

            if (co is BattleWait)
            {
                co.Dispose();
                bearutines.Remove(co);
            }
            else
            {
                co.Tick(dt);
                if (co.finished)
                {
                    co.Dispose();
                    bearutines.Remove(co);
                }
            }
            if (node.Next == null) return;
            node = node.Next;
        }
    }
}
