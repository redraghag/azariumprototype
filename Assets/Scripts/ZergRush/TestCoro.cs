﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;


public class BattleWait : IProcess
{
    public float time;
    public BattleWait(float timeToWait) { time = timeToWait; }
    public bool finished { get { return time <= 0; } }
    public void Tick(float dt)
    {
        time -= dt;
    }
    public void Dispose() { }
}

public class BattleWaitAnimation : IProcess
{
    private readonly Animator _animator;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="animator"></param>
    /// <param name="stateId">stateId - must contain layer name, example: "Base.spawn"</param>
    public BattleWaitAnimation(Animator animator, string stateId)
    {
        _animator = animator;
        hash = Animator.StringToHash(stateId);
    }

    public BattleWaitAnimation(Animator animator, int stateHash)
    {
        _animator = animator;
        hash = stateHash;
    }

    private bool waitAnimation = true;
    protected float time = 1;

    private int hash;

    public bool finished { get { return time <= 0; } }
    public void Tick(float dt)
    {
        if (waitAnimation)
        {
            var currentBaseState = _animator.GetCurrentAnimatorStateInfo(0);

            if (currentBaseState.nameHash == hash)
            {
                time = currentBaseState.length;
                waitAnimation = false;
                return;
            }
        }
        time -= dt;
    }
    public void Dispose() { }
}

public class WaitForStreamEvent : IProcess
{
    protected IDisposable connection;
    public WaitForStreamEvent(IEmptyStream stream)
    {
        //Debug.Log("constructor WaitForStreamEvent : " + this.GetHashCode().ToString());
        connection = stream.FirstOnly().Listen(() =>
        {
            this.finished_ = true;
        });
    }
    protected bool finished_;
    public bool finished
    {
        get
        {
            return finished_;
        }
    }
    public void Tick(float dt) { }
    public void Dispose() { connection.Dispose(); }
}

public class WaitForStreamEvent<T> : IProcess
{
    protected IDisposable connection;

    public WaitForStreamEvent(IStream<T> stream, Action<T> act)
    {
        //Debug.Log("constructor WaitForStreamEvent : " + this.GetHashCode().ToString());
        connection = stream.FirstOnly().Listen(val =>
        {
            act(val);
            this.finished_ = true;
        });
    }

    protected bool finished_;
    public bool finished
    {
        get
        {
            return finished_;
        }
    }
    public void Tick(float dt) { }
    public void Dispose() { connection.Dispose(); }
}

public class WhoIsFirst : MetaProcessBase
{
    Action<int> callResult;
    IEnumerable<IProcess> processes;
    public WhoIsFirst(Action<int> result, IEnumerable<IProcess> inProcesses)
    {
        //Debug.Log("constructor : " + firstIsTrue.GetHashCode().ToString());
        callResult = result;
        processes = inProcesses;
        children.AddRange(processes);
    }
    public override bool finished
    {
        get
        {
            foreach (var child in processes)
            {
                if (child.finished)
                {
                    SetFinished(child);
                    return true;
                }
            }
            return false;
        }
    }
    void SetFinished(IProcess result)
    {
        if (callResult == null) return;
        int i = 0;
        foreach (var proc in processes)
        {
            if (ReferenceEquals(result, proc))
            {
                callResult(i);
            }
            i++;
        }
        callResult = null;
    }
}

class TestCoro : MonoBehaviour
{

    IEnumerator Test()
    {
        Debug.Log("start");
        yield return 1;
        Debug.Log("1");
        yield return 2;
        Debug.Log("2");
        yield return 3;
        Debug.Log("3");
    }

    Executer testExecuter;

    // Use this for initialization
    void Start()
    {
        var it = Test2();
        it.MoveNext();
        Debug.Log(it.Current.ToString());
        it.MoveNext();
        Debug.Log(it.Current.ToString());
        it.MoveNext();
        Debug.Log(it.Current.ToString());
        it.MoveNext();
        Debug.Log(it.Current.ToString());
        it.MoveNext();
        Debug.Log(it.Current.ToString());
        it.MoveNext();
        Debug.Log(it.Current.ToString());

        testExecuter = new Executer();
        testExecuter.Execute(Bearutine(), null);

    }

    IEnumerator<int> Test2()
    {
        yield return 30;
        for (int i = 0; i < 30; i++)
        {
            yield return i;
        }
        yield return 20;
    }

    IEnumerator Bearutine()
    {
        Debug.Log("choroval!");
        yield return new Elastic(new ChronoLerp(2f, 0, 1, val => Debug.Log("chronoVal: " + val.ToString())));

        Debug.Log("started wait 3 sec");
        yield return new BattleWait(3);
        Debug.Log("wait child coroutine");
        yield return testExecuter.Execute(Bearutine2(), null);
        Debug.Log("wait first 2 or 20 sec");
        bool longIsFirst = true;
        var coroChild = testExecuter.Execute(Bearutine2(), null);
        yield return coroChild.Wait();
        //yield return new WhoIsFirst(
        //    val => longIsFirst = val,
        //    coroChild.Wait(),
        //    new BattleWait(1)
        //);
        if (!longIsFirst) Debug.Log("3 sec was first, ok");
        else Debug.Log("20 sec was first, error");

        Debug.Log("all finished test");
        yield return new AllFinished { new BattleWait(0.4f), new BattleWait(3), new BattleWait(2) };
        Debug.Log("all finished should be in 3 sec");

        Debug.Log("any finished test");
        yield return new AnyFinished { new BattleWait(0.4f), new BattleWait(3), new BattleWait(2) };
        Debug.Log("any finished should be in 0.4 sec");
    }

    IEnumerator Bearutine2()
    {
        yield return new BattleWait(3);
        Debug.Log("child first");
        yield return new BattleWait(3);
        Debug.Log("child second");
        yield return new BattleWait(3);
        Debug.Log("child finish");
    }


    void Update()
    {
        testExecuter.Tick(Time.deltaTime);
    }
}



