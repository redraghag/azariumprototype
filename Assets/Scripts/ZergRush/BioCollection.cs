﻿//using System;

using System;
using System.Collections.Generic;
using System.Collections;
using UniRx;
using ZergRush;
using UnityEngine;

public class CollectionRemoveEvent<T>
{
    public int Index { get; private set; }
    public T Value { get; private set; }

    public CollectionRemoveEvent(int index, T value)
    {
        this.Index = index;
        this.Value = value;
    }

    public override string ToString()
    {
        return string.Format("Index:{0} Value:{1}", Index, Value);
    }
}

public class CollectionMoveEvent<T>
{
    public int OldIndex { get; private set; }
    public int NewIndex { get; private set; }
    public T Value { get; private set; }

    public CollectionMoveEvent(int oldIndex, int newIndex, T value)
    {
        this.OldIndex = oldIndex;
        this.NewIndex = newIndex;
        this.Value = value;
    }

    public override string ToString()
    {
        return string.Format("OldIndex:{0} NewIndex:{1} Value:{2}", OldIndex, NewIndex, Value);
    }
}

public class CollectionReplaceEvent<T>
{
    public int Index { get; private set; }
    public T OldValue { get; private set; }
    public T NewValue { get; private set; }

    public CollectionReplaceEvent(int index, T oldValue, T newValue)
    {
        this.Index = index;
        this.OldValue = oldValue;
        this.NewValue = newValue;
    }

    public override string ToString()
    {
        return string.Format("Index:{0} OldValue:{1} NewValue:{2}", Index, OldValue, NewValue);
    }
}

[Serializable]
public class BioCollection<T> : System.Collections.ObjectModel.Collection<T>, IOrganism<IOrganism, IRoot>, IObservableCollection<T>
    where T : class, IOrganism<IOrganism, IRoot>
{
    public BioCollection()
    {

    }

    public IProcess Start(IEnumerator bearutine) { throw new System.Exception("Bio collection can't start bearutine"); }

    public BioCollection(IEnumerable<T> collection)
    {
        if (collection == null) throw new ArgumentNullException("collection");

        foreach (var item in collection)
        {
            Add(item);
        }
    }

    public BioCollection(List<T> list)
        : base(list != null ? new List<T>(list) : null)
    {
    }

    [NonSerialized]
    List<IDisposable> tails;
    [NonSerialized]
    bool wokeUp;
    [NonSerialized]
    bool alive = false;
    [NonSerialized]
    OnceEmptyStream fellAsleep_;
    public IOnceEmptyStream fellAsleep { get { return fellAsleep_ ?? (fellAsleep_ = new OnceEmptyStream()); } }
    public void DisposeWhenAsleep(IDisposable disposable)
    {
        Collect(disposable);
    }

    public bool isAlive { get { return alive; } }


    [NonSerialized]
    IOrganism carrier_;
    [NonSerialized]
    IRoot root_;
    public IOrganism carrier { get { return carrier_; } set { carrier_ = value; } }
    public IRoot root { get { return root_; } set { root_ = value; } }
    public IOrganism Carrier() { return carrier; }

    public virtual void Setup(IOrganism c, IRoot r)
    {
        carrier = c;
        root = r;

        this.ForEach(child =>
        {
            child.Setup(carrier, root);
        });
    }

    public virtual void WakeUp()
    {
        this.ForEach(child =>
        {
            child.WakeUp();
        });
        wokeUp = true;
    }

    public virtual void Spread()
    {
        this.ForEach(child =>
        {
            child.Spread();
            Collect(child.fellAsleep.Listen(() => Remove(child)));
        });
        alive = true;
    }

    public void ToSleep()
    {
        if (tails != null)
        {
            tails.ForEach(d => d.Dispose());
            tails.Clear();
        }

        this.ForEach(child =>
        {
            child.ToSleep();
        });

        wokeUp = false;
        alive = false;
    }

    public void ForEach(Action<T> action)
    {
        for (int i = 0; i < Count; ++i)
        {
            action(this[i]);
        }
    }

    public void Collect(IDisposable tail)
    {
        if (tails == null)
        {
            tails = new List<IDisposable>();
        }
        tails.Add(tail);
    }

    protected override void ClearItems()
    {
        var beforeCount = Count;

        if (tails != null)
        {
            foreach (var tail in tails)
            {
                tail.Dispose();
            }
            tails.Clear();
        }

        ForEach(child =>
        {
            if (child != null)
            {
                child.ToSleep();
            }
            else
            {
                Debug.LogError("null child in bio collection");
            }
        });

        base.ClearItems();

        if (collectionReset != null) collectionReset.Send(UniRx.Unit.Default);
        if (beforeCount > 0)
        {
            if (countChanged != null) countChanged.Send(0);
        }
        if (updates != null) updates.value = this;
    }

    protected override void InsertItem(int index, T item)
    {
        base.InsertItem(index, item);

        if (object.ReferenceEquals(transplantTarget, item)) return;

        if (carrier != null)
        {
            item.Setup(carrier, root);
        }
        if (wokeUp)
        {
            item.WakeUp();
        }
        if (alive)
        {
            item.Spread();
            Collect(item.fellAsleep.Listen(() => Remove(item)));
        }

        if (collectionAdd != null) collectionAdd.Send(new CollectionAddEvent<T>(index, item));
        if (countChanged != null) countChanged.Send(Count);
        if (updates != null) updates.value = this;
    }

    public void Move(int oldIndex, int newIndex)
    {
        MoveItem(oldIndex, newIndex);
    }

    public void Implant(IOrganism intruder, T orphan)
    {
        Add(orphan);
        intruder.fellAsleep.Listen(() => Remove(orphan));
    }

    public void AddRange(IEnumerable<T> range)
    {
        foreach (var r in range)
        {
            Add(r);
        }
    }

    T transplantTarget;
    public void TransplantOrganTo(T organ, BioCollection<T> anotherBody)
    {
        if (anotherBody.carrier != this.carrier)
        {
            Debug.LogError("can not transplant to body with other carrier");
        }
        transplantTarget = organ;
        if (!Remove(organ))
        {
            Debug.LogError("is no an organ of this collection");
            transplantTarget = null;
            return;
        }
        anotherBody.transplantTarget = organ;
        anotherBody.Add(organ);
        anotherBody.transplantTarget = null;
    }

    protected virtual void MoveItem(int oldIndex, int newIndex)
    {
        T item = this[oldIndex];
        base.RemoveItem(oldIndex);
        base.InsertItem(newIndex, item);

        if (collectionMove != null) collectionMove.Send(new CollectionMoveEvent<T>(oldIndex, newIndex, item));
        if (updates != null) updates.value = this;
    }

    protected override void RemoveItem(int index)
    {
        T item = this[index];

        if (!object.ReferenceEquals(transplantTarget, item))
        {
            if (item.isAlive)
                item.ToSleep();
            else
                base.RemoveItem(index);
        }
        else
            base.RemoveItem(index);

        if (collectionRemove != null) collectionRemove.Send(new CollectionRemoveEvent<T>(index, item));
        if (countChanged != null) countChanged.Send(Count);
        if (updates != null) updates.value = this;
    }

    protected override void SetItem(int index, T item)
    {
        T oldItem = this[index];
        base.SetItem(index, item);

        if (collectionReplace != null) collectionReplace.Send(new CollectionReplaceEvent<T>(index, oldItem, item));
        if (updates != null) updates.value = this;
    }


    [NonSerialized]
    Stream<int> countChanged = null;
    public IStream<int> ObserveCountChanged()
    {
        return countChanged ?? (countChanged = new Stream<int>());
    }

    [NonSerialized]
    Stream<UniRx.Unit> collectionReset = null;
    public IStream<UniRx.Unit> ObserveReset()
    {
        return collectionReset ?? (collectionReset = new Stream<UniRx.Unit>());
    }

    

    [NonSerialized]
    Stream<CollectionAddEvent<T>> collectionAdd = null;
    public IStream<CollectionAddEvent<T>> ObserveAdd()
    {
        return collectionAdd ?? (collectionAdd = new Stream<CollectionAddEvent<T>>());
    }

    [NonSerialized]
    Stream<CollectionMoveEvent<T>> collectionMove = null;
    public IStream<CollectionMoveEvent<T>> ObserveMove()
    {
        return collectionMove ?? (collectionMove = new Stream<CollectionMoveEvent<T>>());
    }

    [NonSerialized]
    Stream<CollectionRemoveEvent<T>> collectionRemove = null;
    public IStream<CollectionRemoveEvent<T>> ObserveRemove()
    {
        return collectionRemove ?? (collectionRemove = new Stream<CollectionRemoveEvent<T>>());
    }

    [NonSerialized]
    Stream<CollectionReplaceEvent<T>> collectionReplace = null;
    public IStream<CollectionReplaceEvent<T>> ObserveReplace()
    {
        return collectionReplace ?? (collectionReplace = new Stream<CollectionReplaceEvent<T>>());
    }

    [NonSerialized]
    Cell<ICollection<T>> updates;

    public ICell<ICollection<T>> asCell
    {
        get
        {
            if (updates == null)
            {
                updates = new Cell<ICollection<T>>();
                updates.checkEquality = false;
                updates.value = this;
            }

            return updates;
        }
    }
}
