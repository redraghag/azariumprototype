﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CellUtils;
using UniRx;
using UnityEngine;

interface ISubstance
{
    void ClearIntruder(IConnectionCollector intruder, object influence);
}

class TagCollector : ILiving
{
    public bool isAlive { get { return true; } }
    public IOnceEmptyStream fellAsleep { get { return null; } }
    public void DisposeWhenAsleep(IDisposable disposable)
    {
        
    }
}

public class BioProcessor<T>
{
    List<Instruction> instructions;
    public BioProcessor(){}

    public IDisposable Affect(ILiving intruder, Func<T, T> processor)
    {
        if (instructions == null) instructions = new List<Instruction>();
        var instruction = new Instruction { intruder = intruder, instruction = processor };
        instructions.Add(instruction);

        var disposable = new DisposableInstruction { instruction = instruction, substance = this };
        intruder.DisposeWhenAsleep(disposable);

        return disposable;
    }

    public IDisposable AffectUnsafe(Func<T, T> processor)
    {
        return Affect(new TagCollector(), processor);
    }

    class Instruction
    {
        public ILiving intruder;
        public Func<T, T> instruction;
        public bool toDelete;
    }

    class DisposableInstruction : IDisposable
    {
        public Instruction instruction;
        public BioProcessor<T> substance;
        public void Dispose()
        {
            if (substance != null) substance.ClearInstruction(instruction);
            substance = null;
            instruction = null;
        }
    }

    void ClearInstruction(Instruction intr)
    {
        if (processing)
            intr.toDelete = true;
        else
            instructions.Remove(intr);
    }

    [NonSerialized]
    bool processing = false;
    public T Process(T value)
    {
        if (instructions == null)
            return value;
        processing = true;
        foreach (var processor in instructions)
        {
            value = processor.instruction(value);
        }
        processing = false;
        instructions.RemoveAll(val => val.toDelete);
        if (output_ != null) output_.Send(value);
        return value;
    }

    Stream<T> output_;

    public IStream<T> output
    {
        get
        {
            return output_ ?? (output_ = new Stream<T>());
        }
    }
}

[Serializable]
public abstract class SubstanceBase<T, InfT> : ICell<T>
{
    [SerializeField]
    protected T localBase;

    ICell<T> externalBase;
    IDisposable externalBaseConnection;

    Stream<T> update;
    T result;
    protected List<Intrusion> intrusions;

    public SubstanceBase(){}

    public SubstanceBase(T baseVal)
    {
        localBase = baseVal;
        result = localBase;
    }

    public T baseValue
    {
        get
        {
            return localBase;
        }
        set
        {
            if (externalBaseConnection != null)
            {
                externalBaseConnection.Dispose();
                externalBaseConnection = null;
            }
            localBase = value;
            UpdateAll();
        }
    }

    public ICell<T> baseValueReactive
    {
        set
        {
            externalBaseConnection = value.Bind(val => baseValue = val);
        }
    }
    public Type valueType {
        get { return typeof (T); }
    }

    public IDisposable Bind(Action<T> action, Priority p)
    {
        if (update == null) update = new Stream<T>();
        action(result);
        return update.Listen(action, p);
    }

    public IDisposable OnChanged(Action action, Priority p)
    {
        return ListenUpdates(_ => action(), p);
    }

    public object valueObject { get { return value; } set {throw new Exception("cant set substance");} }

    public IDisposable ListenUpdates(Action<T> action, Priority p)
    {
        if (update == null) update = new Stream<T>();
        return update.Listen(action, p);
    }
    public IStream<T> updates { get { return update ?? (update = new Stream<T>()); } }
    public T value
    {
        get
        {
            if (Transaction.calculationMode)
            {
                Transaction.AddTouchedCell(this);
            }
            return result;
        }
    }

    Intrusion AffectInner(ILiving intruder, InfT influence)
    {
        if (intruder == null)
        {
            Debug.LogError("intruder is zero");
            return new Intrusion();
        }
        if (intrusions == null) intrusions = new List<Intrusion>();
        var intrusion = new Intrusion { intruder = intruder, influence = influence };
        intrusions.Add(intrusion);
        OnAdd(intrusion);
        //intrusion.awakeConnection = intruder.awakened.ListenUpdates(awake =>
        //{
        //    if (awake)
        //        OnAdd(intrusion);
        //    else
        //        OnRemove(intrusion);
        //});
        return intrusion;
    }



    public virtual IDisposable AffectUnsafe(InfT influence)
    {
        return Affect(new TagCollector(), influence);
    }

    public virtual IDisposable AffectUnsafe(ICell<InfT> influence)
    {
        return Affect(new TagCollector(), influence);
    }

    public virtual IDisposable Affect(ILiving intruder, InfT influence)
    {
        if (!intruder.isAlive) return new EmptyDisposable();
        var intrusion = AffectInner(intruder, influence);
        var disposable = new DisposableAffect { intrusion = intrusion, substance = this};
        intruder.DisposeWhenAsleep(disposable);
        return disposable;
    }

    public virtual IDisposable Affect(ILiving intruder, ICell<InfT> influence)
    {
        if (!intruder.isAlive) return new EmptyDisposable();
        var intrusion = AffectInner(intruder, influence.value);
        intrusion.updateConnection = influence.ListenUpdates(val => {
            intrusion.influence = val;
            OnUpdate(intrusion);
        });

        var disposable = new DisposableAffect { intrusion = intrusion, substance = this };
        intruder.DisposeWhenAsleep(disposable);
        return disposable;
    }

    public virtual IDisposable Affect(ILiving intruder, InfT influence, IEmptyStream update)
    {
        if (!intruder.isAlive) return new EmptyDisposable();
        var intrusion = AffectInner(intruder, influence);
        intrusion.updateConnection = update.Listen(() =>
        {
            OnUpdate(intrusion);
        });

        var disposable = new DisposableAffect { intrusion = intrusion, substance = this };
        intruder.DisposeWhenAsleep(disposable);
        return disposable;
    }

    protected virtual void OnAdd(Intrusion intrusion)
    {
        UpdateAll();
    }

    protected virtual void OnRemove(Intrusion intrusion)
    {
        UpdateAll();
    }

    protected virtual void OnUpdate(Intrusion intrusion)
    {
        UpdateAll();
    }

    protected virtual void UpdateAll()
    {
        var r = Result();
        if (!object.Equals(r, result))
        {
            result = r;
            if (update != null) update.Send(result);
        }

    }

    protected abstract T Result();

    protected class Intrusion
    {
        public ILiving intruder;
        public InfT influence;
        public IDisposable awakeConnection;
        public IDisposable updateConnection;
    }

    class DisposableAffect : IDisposable
    {
        public Intrusion intrusion;
        public SubstanceBase<T, InfT> substance;
        public void Dispose()
        {
            if (substance != null) substance.ClearIntrusion(intrusion);
            substance = null;
            intrusion = null;
        }
    }

    protected virtual void ClearIntrusion(Intrusion intr)
    {
        intrusions.Remove(intr);
        if (intr.awakeConnection != null) intr.awakeConnection.Dispose();
        if (intr.updateConnection != null) intr.updateConnection.Dispose();
        OnRemove(intr);
    }
}

[Serializable]
public class ValueAdditive : SubstanceBase<double, double>
{
    public ValueAdditive() : base(0) { }
    public ValueAdditive(double val) : base(val) { }

    protected override double Result()
    {
        if (intrusions == null) return localBase;
        double sum = 0;
        foreach (var intr in intrusions)
            sum += intr.influence;
        return localBase + sum;
    }
}

[Serializable]
public class AdditiveSubstance : SubstanceBase<float, float>
{
    public AdditiveSubstance() : base(0) { }
    public AdditiveSubstance(float val) : base(val) { }

    protected override float Result()
    {
        if (intrusions == null) return localBase;
        float sum = 0;
        foreach (var intr in intrusions)
            sum += intr.influence;
        return localBase + sum;
    }
}

[Serializable]
public class MultiplicativeSubstance : SubstanceBase<float, float>
{
    public MultiplicativeSubstance() : base(1) { }
    public MultiplicativeSubstance(float val) : base(val) { }

    protected override float Result()
    {
        if (intrusions == null) return localBase;
        return intrusions.Aggregate(localBase, (current, intr) => current * intr.influence);
    }
}

//[Serializable]
//public class SubstanceCollection<T> : SubstanceBase<UniRx.InternalUtil.ImmutableList<T>, T>
//{
//    public SubstanceCollection() : base(new UniRx.InternalUtil.ImmutableList<T>()) { }

//    protected override UniRx.InternalUtil.ImmutableList<T> Result()
//    {
//        var arr = new T[intrusions.Count];
//        int index = 0;
//        foreach (var intr in intrusions)
//        {
//            arr[index] = intr.influence;
//            index++;
//        }
//        return new UniRx.InternalUtil.ImmutableList<T>(arr);
//    }
//}

[Serializable]
public class Substance<T> : SubstanceBase<T, Func<T, T>>
{
    protected override T Result()
    {
        T current = localBase;
        foreach (var processor in intrusions)
        {
           // if (processor.intruder.awakened.value)
                current = processor.influence(current);
        }
        return current;
    }
}

[Serializable]
public class OrSubstance : SubstanceBase<bool, bool>
{
    public OrSubstance() : base(false){}

    protected override bool Result()
    {
        return intrusions.Any(processor => processor.influence);
    }
}

[Serializable]
public class LastValueSubstance<T> : SubstanceBase<T, T>
{
    public LastValueSubstance() : base(default(T)) { }
    public LastValueSubstance(T t) : base(t) { }

    protected override T Result()
    {
        if (intrusions.Count == 0) return baseValue;
        return intrusions[intrusions.Count - 1].influence;
    }
}

[Serializable]
public class AndSubstance : SubstanceBase<bool, bool>
{
    public AndSubstance() : base(true){ }
    protected override bool Result()
    {
        return intrusions.All(processor => processor.influence);
    }
}

[Serializable]
public class ArraySubstance<T> : SubstanceBase<IEnumerable<T>, T>
{
    public ArraySubstance() : base(new List<T>()) { }
    protected override IEnumerable<T> Result()
    {
        var list = new List<T>();
        list.AddRange(intrusions.Select(i => i.influence));
        return list;
    }
}


[Serializable]
public class AverageValue : SubstanceBase<float, Func<float>>
{
    protected override float Result()
    {
        float total = 0;
        foreach (var processor in intrusions)
        {
            total += processor.influence();
        }
        total /= intrusions.Count;
        return total;
    }
}
