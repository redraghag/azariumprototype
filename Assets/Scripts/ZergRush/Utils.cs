﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UniRx;
using UnityEngine.EventSystems;

public static class DoubleExtensions
{
    public static string ToFortranDouble(this double value)
    {
        return value.ToFortranDouble(4);
    }

    public static string ToFortranDouble(this double value, int precision)
    {
        return string.Format(value.ToString(
            string.Format("\\0.{0}E0", new string('#', precision))
            ));
    }
}

public class ExecuterComponent : MonoBehaviour, ILivingState
{
    public Executer exec = new Executer();
    public void Update()
    {
        exec.Tick(Time.deltaTime);
    }

    public bool isAlive { get { return gameObject.activeInHierarchy; } }
}

public static class ExecuterExtension
{
    public static IProcessExecution Execute(this MonoBehaviour obj, IEnumerator func)
    {
        ExecuterComponent executionComponent = obj.gameObject.GetOrAddComponent<ExecuterComponent>();
        //Debug.Log(executionComponent.ToString());
        return executionComponent.exec.Execute(func, executionComponent);
    }
}

public static class ImageExtension
{
    public static void MakeUniqueMaterial(this Image img)
    {
        img.material = UnityEngine.Object.Instantiate(img.material) as Material;
    }

    public static void SetBlackAndWhite(this Image img, float val, float darken = 0.0f)
    {
        if (img.material == null || img.material.name != "UIGreyable")
            img.material = UnityEngine.Object.Instantiate(Resources.Load("Images/FX/Materials/UIGreyable")) as Material;
        img.material.SetFloat("isGrey", val);
        img.material.SetFloat("darken", darken);
    }
}

public static class OrderedDictionaryExt
{
    public static int GetIndex(this OrderedDictionary dictionary, string key)
    {
        int i = 0;
        foreach (var k in dictionary.Keys)
        {
            if (k as string == key)
            {
                return i;
            }
            i++;
        }
        return -1;
    }
}


public static class Utils
{
    public class Wrapper<T>
    {
        public T value;
    }

    public static string FormatEach<T>(this IEnumerable<T> self, string format)
    {
        return FormatEach(self, format, arg1 => arg1);
    }

    public static string FormatEach<T>(this IEnumerable<T> self)
    {
        return FormatEach(self, "{0}", arg1 => arg1);
    }
     
    public static string FormatEach<T>(this IEnumerable<T> self, Func<T, object> parameter)
    {
        return FormatEach(self, "{0}", parameter);
    }

    public static string FormatEach<T>(this IEnumerable<T> self, string format, params Func<T, object>[] parameters)
    {
        var builder = new StringBuilder();

        foreach (var value in self)
        {
            builder.AppendFormat(format + "\n", parameters.Select(func => func(value)).ToArray());
        }
        return builder.ToString();
    }

    public static T CastTo<T>(this string str)
    where T : new()
    {
        if (str.Length > 0)
        {
            if (str.Contains("%"))
            {
                str = str.Replace("%", "");
                double tempDouble = Convert.ToDouble(str) / 100.0;
                str = tempDouble.ToString();
            }

            try
            {
                return (T)Convert.ChangeType(str, typeof(T));
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message + " : " + str + "; " + e.StackTrace);
                return new T();
            }
        }
        else return (T)Convert.ChangeType(0.0f, typeof(T));
    }

    //static float AttributeValue(FieldInfo f, object attr)
    //{
    //    var value = f.GetValue(attr);
    //    if (value is AbilityStat) return (value as AbilityStat).value;
    //    if (value is float) return (float) value;
    //    if (value is int) return (int)value;
    //    return 0f;
    //}

    //static string ComposeAttrString(FieldInfo f, object value, float baseDamage = 0)
    //{
    //    if (f.GetCustomAttributes(typeof (EnemyDamageCoeff), true).Any())
    //    {
    //        var coeff = 1f;
    //        var ab = value as EnemyAbility;
    //        if (ab != null)
    //        {
    //            if (ab.isAlive)
    //            {
    //                coeff = ab.carrier.mi.damageMult * ab.carrier.outcomeDamageBoost.value;
    //            }
    //        }
    //        return Mathf.RoundToInt(AttributeValue (f, value) * BattleDesign.damagePerMonsterAttack * coeff).ToString();
    //    }
    //    else if (f.GetCustomAttributes(typeof(EnemyHealCoeff), true).Any())
    //    {
    //        return Mathf.RoundToInt(AttributeValue(f, value) * BattleDesign.baseMonsterHp).ToString();
    //    }
    //    else if (f.GetCustomAttributes(typeof(IsPercentValue), true).Any())
    //    {
    //        return Mathf.RoundToInt(AttributeValue(f, value) * 100).ToString() + "%";
    //    }
    //    else if (f.GetCustomAttributes(typeof(DamageMultAttr), true).Any())
    //    {
    //        return Mathf.RoundToInt(AttributeValue(f, value) * baseDamage).ToString();
    //    }
    //    return Mathf.RoundToInt(AttributeValue(f, value)).ToString();
    //}

    public static string DamageColorTag(string value)
    {
        return "<b><color=#FFFF44>" + value + " </color></b>";
    }

    public static string FormatGameNumber(float number)
    {
        if (number > 10) return number.ToString("0.");
        else if (number > 1) return number.ToString("0.0");
        else return number.ToString("0.00");
    }

    //public static string ComposeDescription(string description, object objWithAttributes, float baseDamage = 0) {
    //    var attributes =
    //        from field in objWithAttributes.GetType().GetFields()
    //        let attrib = (ParseAttrib) field.GetCustomAttributes(typeof (ParseAttrib), true).FirstOrDefault()
    //        where attrib != null
    //        orderby attrib.index
    //        select DamageColorTag(ComposeAttrString(field, objWithAttributes, baseDamage));

    //    var attrs = attributes.ToList();

    //    return string.Format(description, attrs.ToArray());
    //}

    public static Wrapper<T> Wrap<T>(T value)
    {
        return new Wrapper<T> { value = value };
    }

    public static IEnumerable<T> Some<T>(params T[] elements)
    {
        return elements;
    }

    public static int Loop(int i, int cycle)
    {
        int r = i % cycle;
        if (r < 0) r += cycle;
        return r; 
    }

    public static bool HasAttribute<T>(this FieldInfo field) where T : Attribute
    {
        return Attribute.IsDefined(field, typeof (T));
    }

    public static IDisposable OnPointerDown(this EventTrigger self, Action act)
    {
        var entry = new EventTrigger.Entry { eventID = EventTriggerType.PointerDown };

        entry.callback.AddListener(data =>
        {
            act();
        });

        self.triggers.Add(entry);
        return Disposable.Create(() => self.triggers.Remove(entry));
    }

    public static T FirstFilteredOrFirst<T>(this IEnumerable<T> enumerable, Func<T, bool> filter)
    {
        var enumerable1 = enumerable as T[] ?? enumerable.ToArray();
        var firstOrDefault = enumerable1.FirstOrDefault(filter);
        if (firstOrDefault == null)
        {
            return enumerable1.First();
        }
        return firstOrDefault;
    }

    public static void ForeachWithIndices<T>(this IEnumerable<T> value, Action<T, int> act, bool backwards = false)
    {
        int ix = 0;
        foreach (var e in backwards ? value.Reverse() : value)
        {
            act(e, ix);
            ix++;
        }
    }

    public static void Remove<T>(this List<T> value, Func<T, bool> act)
    {
        var target = value.FirstOrDefault(act);
        while (target != null){
            value.Remove(value.FirstOrDefault(act));
            target = value.FirstOrDefault(act);
        }
    }

    public static List<List<T>> Split<T>(this IEnumerable<T> value, int countOfEachPart)
    {
        int cnt = value.Count() / countOfEachPart;
        List<List<T>> result = new List<List<T>>();
        for (int i = 0; i <= cnt; i++)
        {
            List<T> newPart = value.Skip(i * countOfEachPart).Take(countOfEachPart).ToList();
            if (newPart.Any())
                result.Add(newPart);
            else
                break;
        }

        return result;
    }

    public static IEnumerable<IDictionary<TKey, TValue>> Split<TKey, TValue>(this IDictionary<TKey, TValue> value, int countOfEachPart)
    {
        IEnumerable<Dictionary<TKey, TValue>> result = value.ToArray()
                                                            .Split(countOfEachPart)
                                                            .Select(p => p.ToDictionary(k => k.Key, v => v.Value));
        return (IEnumerable<IDictionary<TKey, TValue>>)result;
    }


    public static IEnumerable<T> TakeAllButLast<T>(this IEnumerable<T> source)
    {
        var it = source.GetEnumerator();
        bool hasRemainingItems = false;
        bool isFirst = true;
        T item = default(T);

        do
        {
            hasRemainingItems = it.MoveNext();
            if (hasRemainingItems)
            {
                if (!isFirst) yield return item;
                item = it.Current;
                isFirst = false;
            }
        } while (hasRemainingItems);
    }

    public static int IndexOf<T>(this IEnumerable<T> list, T elem)
    {
        return list.IndexOf(val => object.ReferenceEquals(val, elem));
    }

    public static int IndexOf<T>(this IEnumerable<T> list, Func<T, bool> predicate)
    {
        int index = 0;
        foreach (var elem in list)
        {
            if (predicate(elem)) return index;
            index++;
        }
        return -1;
    }

    public static void SetLayerRecursively(this GameObject go, int layerId)
    {
        go.layer = layerId;
        foreach (Transform c in go.transform)
        {
            c.gameObject.layer = layerId;
        }
    }

    //static System.Random rand = new System.Random();

    public static List<float> NormalizeFloatRange(this IEnumerable<float> range)
    {
        var magnitude = range.Sum();
        if (magnitude == 0) return range.ToList();
        return range.Select(val => val / magnitude).ToList();
    }

    public static T WeightedRandomElement<T>(this IEnumerable<T> coll, Func<T, float> weightFunc)
    {
        var list = coll.ToList();
        if (list.Count > 0)
            return list.ElementAt(GetRandomIndexFromWeghts(list.Select(weightFunc)));
        else
            return default(T);
    }

    public static T WeightedRandomElement<T>(this IEnumerable<T> coll, System.Random generator, Func<T, float> weightFunc)
    {
        var list = coll.ToList();
        if (list.Count > 0)
            return list.ElementAt(GetRandomIndexFromWeghts(list.Select(weightFunc), generator));
        else
            return default(T);
    }
    public static int GetRandomIndexFromWeghts(this IEnumerable<float> probabilities, System.Random generator)
    {
        var tempProb = NormalizeFloatRange(probabilities);

        double diceRoll = generator.NextDouble();
        double accumulated = 0.0f;

        for (int i = 0; i < tempProb.Count; i++)
        {
            if (diceRoll >= accumulated && diceRoll <= accumulated + tempProb[i])
            {
                //Debug.Log("Random rolled " + diceRoll.ToString());
                return i;
            }
            accumulated += tempProb[i];
        }

        return 0;
    }
    public static int GetRandomIndexFromWeghts(this IEnumerable<float> probabilities)
    {
        var tempProb = NormalizeFloatRange(probabilities);

        float diceRoll = UnityEngine.Random.value;
        float accumulated = 0.0f;

        for (int i = 0; i < tempProb.Count; i++)
        {
            if (diceRoll >= accumulated && diceRoll <= accumulated + tempProb[i])
            {
                //Debug.Log("Random rolled " + diceRoll.ToString());
                return i;
            }
            accumulated += tempProb[i];
        }

        return 0;
    }

    public static T GetRandomEnum<T>()
    {
        var possibilities = Enum.GetNames(typeof(T)).ToList();
        return (T)Enum.Parse(typeof(T), possibilities[UnityEngine.Random.Range(0, possibilities.Count)]);
    }

    static public T GetOrAddComponent<T>(this GameObject obj) where T : Component
    {
        return obj.GetComponent<T>() ?? obj.AddComponent<T>();
    }

    static public float EaseOut(float t)
    {
        return -t * (t - 2);
    }

    static public float EaseIn(float t)
    {
        return t * t * t;
    }

    static public float EaseElasticOut(float t)
    {
        var p = 0.3F;
        return Mathf.Pow(2, -10 * t) * Mathf.Sin((t - p / 4) * (2 * Mathf.PI) / p) + 1;
    }

    static public float EaseOut(float from, float to, float t)
    {
        return Mathf.Lerp(from, to, -t * (t - 2));
    }

    static public float EaseIn(float from, float to, float t)
    {
        return Mathf.Lerp(from, to, t * t * t);
    }

    static string[] numberNames = { "", "K", "M", "B", "KB", "MB", "BB", "KBB" };
    static string[] formats = { "0", "0.#", "0.##", "0.###", "0.####", "0.#####" };

    public static string GetNumberRepresentation(long number, int maxDigits = 3, int maxDigitsAfterDot = 3)
    {
        //maxDigits = Mathf.Max(3, Mathf.Min(maxDigits, formats.Length));
        //float fNumber = number;
        //int i = 0;
        //while (fNumber > 1000f)
        //{
        //    fNumber /= 1000f;
        //    ++i;
        //}
        //int mainDigits = 1;
        //while (fNumber / Mathf.Pow(10, mainDigits) > 1f) mainDigits++;
        //string result = fNumber.ToString(formats[Mathf.Min(maxDigits - mainDigits, maxDigitsAfterDot)]) + numberNames[i];
        if (number >= 10000)
            return number.ToString("0.0e0");
        else
            return number.ToString();
    }

    public static string GetNumberRepresentation(double number, int maxDigits = 3, int maxDigitsAfterDot = 3)
    {
        //maxDigits = Mathf.Max(3, Mathf.Min(maxDigits, formats.Length));
        //int i = 0;
        //while (number > 1000f)
        //{
        //    number /= 1000f;
        //    ++i;
        //}
        //int mainDigits = 1;
        //while (number / Mathf.Pow(10, mainDigits) > 1f) mainDigits++;
        //string result = number.ToString(formats[Mathf.Min(maxDigits - mainDigits, maxDigitsAfterDot)]) + numberNames[i];
        if (number >= 10000.0)
            return number.ToString("0.0e0");
        else
            return number.ToString("0.");
    }

    public static float TenPower(int power)
    {
        return Mathf.Pow(10, power);
    }

    public static int[] RandomNonoverlappedIndices(int max, int count)
    {
        if (max <= count)
        {
            int[] r = new int[max];
            for (int i = 0; i < max; i++)
            {
                r[i] = i;
            }
            return r;
        }
        int[] result = new int[count];
        var range = Enumerable.Range(0, max).ToList();
        for (int i = 0; i < count; ++i)
        {
            int randIndex = UnityEngine.Random.Range(0, max - i);
            int rand = range[randIndex];
            result[i] = rand;
            range[randIndex] = range[max - i - 1];
        }

        return result;
    }

    public static IEnumerable<T> RandomElements<T>(this List<T> list, int count)
    {
        return RandomNonoverlappedIndices(list.Count, count).Select(i => list[i]);
    }
    public static IEnumerable<T> RandomElements<T>(this T[] list, int count)
    {
        return RandomNonoverlappedIndices(list.Length, count).Select(i => list[i]);
    }

    public static E RandomEnumValue<E>()
    {
        var array = System.Enum.GetValues(typeof(E));
        return (E)array.GetValue(UnityEngine.Random.Range(0, array.Length));
    }

    public static V TryGetOrNew<K, V>(this Dictionary<K, V> dict, K key)
        where V : class, new()
    {
        V val = null;
        if (!dict.TryGetValue(key, out val))
        {
            val = new V();
            dict[key] = val;
        }
        return val;
    }
    public static V AtOrDefault<K, V>(this Dictionary<K, V> dict, K key)
        where V : class, new()
    {
        V val = null;
        if (!dict.TryGetValue(key, out val))
        {
            return default(V);
        }
        return val;
    }

    public static T Best<T>(this IEnumerable<T> coll, Func<T, float> predicate) where T : class 
    {
        var best = (T)null;
        var curr = float.MinValue;
        foreach (var v in coll)
        {
            var r = predicate(v);
            if ( r > curr)
            {
                curr = r;
                best = v;
            }
        }
        return best;
    }

    public static T Clone<T>(this T source) where T : class
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }

    public static bool AddIfNotContains<T>(this IList<T> list, T item)
    where T : class
    {
        if (list.Contains(item)) return false;
        list.Add(item);
        return true;
    }

    public static bool AddIfNotContainsType<T>(this IList<T> list, T item)
    where T : class
    {
        foreach (var obj in list)
        {
            if (obj.GetType() == item.GetType())
            {
                return false;
            }
        }
        list.Add(item);
        return true;
    }

    public static IEnumerable<T> Lift<T>(this T self)
    {
        yield return self;
    }

    public static void AddTo<TKey>(this Dictionary<TKey, float> dict, TKey key, float value)
    {
        if (!dict.ContainsKey(key))
        {
            dict[key] = 0;
        }
        dict[key] += value;
    }

    public static TValue GetOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue def)
    {
        return !dict.ContainsKey(key) ? def : dict[key];
    }


    public static T Take<T>(this List<T> list, int index)
    {
        T t = list[index];
        list.RemoveAt(index);
        return t;
    }

    public static T RandomElement<T>(this IEnumerable<T> list)
    {
        return list.ToList().RandomElement();
    }

    public static T RandomElement<T>(this ICollection<T> list)
    {
        if (list.Count < 1)
            return default(T);

        return list.ElementAt(UnityEngine.Random.Range(0, list.Count));
    }

    public static T MakeInstance<T>(this GameObject obj)
        where T : Component
    {
        return (UnityEngine.Object.Instantiate(obj) as GameObject).GetComponent<T>();
    }

    public static GameObject MakeInstance(this GameObject obj)
    {
        return (UnityEngine.Object.Instantiate(obj) as GameObject);
    }

    public static void ResetTransform(this GameObject obj)
    {
        ResetTransform(obj.transform);
    }

    public static void ResetTransform(this Component c)
    {
        c.transform.localPosition = Vector3.zero;
        c.transform.localRotation = Quaternion.identity;
        c.transform.localScale = Vector3.one;
    }

    public static void SetPositionX(this Transform t, float x)
    {
        var position = t.position;
        position.x = x;
        t.position = position;
    }
    public static void SetLocalPositionX(this Transform t, float x)
    {
        var position = t.localPosition;
        position.x = x;
        t.localPosition = position;
    }

    public static void SetPositionY(this Transform t, float y)
    {
        var position = t.position;
        position.y = y;
        t.position = position;
    }
    public static void SetLocalPositionY(this Transform t, float y)
    {
        var position = t.localPosition;
        position.y = y;
        t.localPosition = position;
    }


    public static void SetPositionZ(this Transform t, float z)
    {
        var position = t.position;
        position.z = z;
        t.position = position;
    }
    public static void SetLocalPositionZ(this Transform t, float z)
    {
        var position = t.localPosition;
        position.z = z;
        t.localPosition = position;
    }

    public static List<T> AddSome<T>(this List<T> list, int count, System.Func<T> elem)
    {
        for (int i = 0; i < count; ++i) list.Add(elem());
        return list;
    }

    public static List<T> AddOne<T>(this List<T> list, T elem)
    {
        list.Add(elem);
        return list;
    }

    public static void ZipIterate<T1, T2>(this IEnumerable<T1> coll1, IEnumerable<T2> coll2, Action<T1, T2> func)
    {
        var it1 = coll1.GetEnumerator();
        var it2 = coll2.GetEnumerator();
        while (it1.MoveNext() && it2.MoveNext())
        {
            func(it1.Current, it2.Current);
        }
    } 

    public static void InitWithCorrespondingList<TData, TView>(this ICollection<TView> views, ICollection<TData> data, Action<TData, TView> fillFunc) where TView : MonoBehaviour
    {
        for (int i = 0; i < views.Count; i++)
        {
            var view = views.ElementAt(i);
            if (data.Count > i)
            {
                var item = data.ElementAt(i);
                view.SetActiveSafe(true);
                fillFunc(item, view);
            }
            else
            {
                view.SetActiveSafe(false);    
            }
        }
    }

    public static Sprite LoadSprite(string name)
    {
        Sprite result = null;
        WWW www = new WWW(WWW.EscapeURL("file://" + Application.dataPath + "/" + name));
        while (!www.isDone) { }

        if (www.texture != null)
            result = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));
        else
            Debug.Log("#Failed loading sprite " + name);

        return result;
    }

    public static IEnumerable<T> FlagsToList<T>(int mask)
    {
        if (typeof(T).IsSubclassOf(typeof(Enum)) == false)
            throw new ArgumentException();

        return Enum.GetValues(typeof(T))
                             .Cast<int>()
                             .Where(Mathf.IsPowerOfTwo)
                             .Where(m => ((int)mask & (int)m) != 0)
                             .Cast<T>();
    }
}

