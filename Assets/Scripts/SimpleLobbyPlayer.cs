﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Networking;
using UnityEngine;

class SimpleLobbyPlayer : NetworkLobbyPlayer
{
    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();
        //SendReadyToBeginMessage();
    }

    public override void OnClientReady(bool readyState)
    {
        base.OnClientReady(readyState);
        Debug.Log("on client ready: " + readyState);
    }
}
