﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public Transform target;
    public Vector3 targetPos;
    public Transform tr;
    public float speed;
    public Vector3 offset;
	// Use this for initialization
	void Start ()
    {
        tr = transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (BattleSystem.instance.paused.value) return;
	    if (target != null) targetPos = target.position;
	    if (Vector3.Distance(tr.position - offset, targetPos) <= 0.5f)
	    {
	        Destroy(gameObject);
	        return;
	    }
	    tr.position = Vector3.MoveTowards(tr.position - offset, targetPos, speed*Time.deltaTime) + offset;
        tr.LookAt(targetPos + offset);
    }
}
