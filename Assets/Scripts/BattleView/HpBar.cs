﻿using UnityEngine;
using System.Collections;

public class HpBar : MonoBehaviour
{
    private Transform tr;
    private Vector3 startScale;
    
    void Awake()
    {
        tr = transform;
        startScale = tr.localScale;
    }

    public void SetSide(Side s)
    {
        GetComponent<MeshRenderer>().material.color = s == Side.Blue ? Color.blue : Color.red;
    }

    public void SetFill(float fill)
    {
        if (tr == null) return;
        var vCopy = startScale;
        vCopy.x *= fill;
        tr.localScale = vCopy;
    }
}
