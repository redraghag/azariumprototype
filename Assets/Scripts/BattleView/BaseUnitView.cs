﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BaseUnitView : MonoBehaviour
{
    public HpBar hpBar;
    public TextMesh name;
    public Animator animator;
    public Transform modelRoot;
    public BattleUnit owner;
    public Transform selection;

    void Awake()
    {
    }

    protected IEnumerator Start()
    {
        owner = transform.parent.GetComponent<BattleUnit>();
        if (owner == null)
        {
            UnityEngine.Debug.LogError("Battle unit component not found");
            yield break;
        }
        owner.UpdateInfo();
        if (owner.info == null)
        {
            Debug.Log("WTFWTF");
            yield break;
        }
        hpBar.SetSide(owner.side);
        if (name != null)
            name.text = owner.info.name;
        owner.hpRelative.Bind(hpBar.SetFill);

        var prefab = Resources.Load<GameObject>(ModelSearchPath() + owner.info.model);
        if (prefab == null)
        {
            Debug.LogError("model :" + owner.info.model + " not found");
            yield break;
        }
        var obj = Instantiate(prefab, transform, false) as GameObject;
        obj.transform.localPosition = Vector3.zero;
        animator = obj.GetComponent<Animator>();
        modelRoot = obj.transform;
        if (modelRoot == null)
        {
            Debug.Log("WTF");
        }
        if (selection != null)
            selection.GetComponent<MeshRenderer>().material.color = Color.green;
        BattleUI.instance.currentSelection.Select(val => val == owner).Bind(val =>
        {
            if (selection != null)
                selection.SetActiveSafe(val);
        });
    }

    protected virtual string ModelSearchPath()
    {
        return "Units/";
    }

    protected virtual float ScaleCorrection()
    {
        return 1;
    }

    private string lastAnim;
    public void ShowAnimation(string animType, bool cycled = true)
    {
        if (lastAnim == animType && cycled) return;
        lastAnim = animType;
        if (animator != null)
            animator.SetTrigger(animType);
    }
}
