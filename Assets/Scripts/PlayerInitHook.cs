﻿using UnityEngine;
using System.Collections;
using Prototype.NetworkLobby;
using UnityEngine.Networking;

public class PlayerInitHook : LobbyHook
{
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        var p = gamePlayer.GetComponent<PlayerState>();
        var lp = lobbyPlayer.GetComponent<LobbyPlayer>();

        Debug.Log("hook with index: " + lp.index);
        p.side = lp.index == 0 ? Side.Blue : Side.Red;
        //p.bot = lp.index == 1;
    }
}
