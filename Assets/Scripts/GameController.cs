﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

enum GameMod
{
    Normal,
    AutoLoadLocal,
    AutoLoadSelfAndBot,
    AutoLoadBots,
}

class GameController : MonoBehaviour
{
    public static GameController instance;
    [NonSerialized]
    public GameData gameData;
    public Executer executiveProducer = new Executer();
    public GameMod launchMod;
    
	// Use this for initialization
	void Awake ()
	{
	    instance = this;
	    gameData = GameDataLoader.getGameData();
        GameObject.DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    executiveProducer.Tick(Time.deltaTime);
	}

    public void StartBattleHost()
    {
        ToBattleScene();       
        DoManager(manager => manager.StartHost());
    }

    public void StartBattleClient()
    {
        ToBattleScene();       
        DoManager(manager => manager.StartClient());
    }

    public void StartBattleFast()
    {
        ToBattleScene();       
        DoManager(manager => manager.StartMatchMaker());
    }

    void DoManager(Action<NetworkManager> act)
    {
        StartCoroutine(ManagerCoro(act));
    }

    IEnumerator ManagerCoro(Action<NetworkManager> act)
    {
        yield return null;
        yield return null;
        act(FindObjectOfType<NetworkManager>());
    }

    public void ToBattleScene()
    {
        if (FindObjectOfType<BattleSystem>() == null)
        {
            SceneManager.LoadScene(1);
        }
    }

    void Start()
    {
        if (launchMod == GameMod.Normal) return;
        StartBattleHost();
    }
}
