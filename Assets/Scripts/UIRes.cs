﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class UIRes : MonoBehaviour
{
    public static UIRes instance;

    void Awake()
    {
        instance = this;
    }

    public Sprite gold;
    public Sprite az;

    public Sprite upArrow;
    public Sprite upArrowAz;
    public Sprite plus;
    public Sprite notEnoughGoldIcon;
    public Sprite hammerRed;
    public Sprite hammerGreen;
    public Sprite locked;

    public  List<Sprite> attackSprites = new List<Sprite>();
    public  List<Sprite> armorSprites = new List<Sprite>();
}
