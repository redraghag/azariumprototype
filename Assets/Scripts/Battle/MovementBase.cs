﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public abstract class MovementBase : MonoBehaviour
{
    private float speed_;
    public float speed
    {
        get { return speed_; }
        set { speed_ = value; }
    }

    public Transform enemyCastle;
    [NonSerialized]
    protected Transform tr;
    private CharacterController cc;
    private BattleUnit owner;


    protected void Awake()
    {
        owner = GetComponent<BattleUnit>();
        cc = GetComponent<CharacterController>();
        tr = GetComponent<Transform>();
        StartCoroutine(ForceUpdateCoro());
    }

    IEnumerator ForceUpdateCoro()
    {
        yield return new WaitForSeconds(0.1f+ UnityEngine.Random.value / 4);
        while (true)
        {
            forceUpdate = true;
            yield return new WaitForSeconds(4f+ UnityEngine.Random.value / 4);
        }
    } 

    public bool stopped;

    public void Stop()
    {
        if (stopped) return;
        owner.RpcIdle(tr.position);
        Lock();
        stopped = true;
    }

    private bool collisions_ = true;

    public bool collisions
    {
        get
        {
            return collisions_;
        }
        set
        {
            collisions_ = value;
            if (cc != null) cc.detectCollisions = value;
        }
    }

    void InitMovement()
    {
        if (stopped == false) return;
        stopped = false;
        Unlock();
    }

    public void Engage(Fighter target)
    {
        InitMovement();
        EngageInner(target);
    }

    public void MoveDirection(Vector2 direction)
    {
        InitMovement();
        MoveDirectionInner(direction);
    }


    public abstract void ReparseNodes();
    public abstract System.Action ReparseAction();

    protected abstract void UpdateMovement(float dt);
    protected abstract void Lock();
    protected abstract void Unlock();
    protected abstract void EngageInner(Fighter engageTarget);
    protected abstract void MoveDirectionInner(Vector2 direction);

    protected Vector2 lastMoveDirection;
    protected float lastSpeed;
    protected float updateDirDelta;
    private bool forceUpdate;

    public void MoveTo(Vector2 toGo, float dt)
    {
        bool move = speed != 0 && !stopped;
        if (!move && lastSpeed != speed)
        {
            owner.RpcIdle(tr.position);
        }
        else if (move)
        {
            Vector2 curr = tr.position.ToFlatVector();
            if (cc != null && collisions_)
            {
                Vector2 dir = (toGo - curr);
                dir.Normalize();
                dir *= speed;
                cc.SimpleMove(dir.ToVolumeVector());
            }
            else
            {
                var pos = Vector2.MoveTowards(curr, toGo, speed * dt).ToVolumeVector();
                pos.y = tr.position.y;
                tr.position = pos;
            }
            if (toGo != lastMoveDirection || lastSpeed != speed || forceUpdate)
            {
                owner.RpcUpdateTargetMovement(tr.position.ToFlatVector(), toGo, speed,
                    BattleSystem.instance.serverTime.value);
                forceUpdate = false;
            }
        }
    
        lastMoveDirection = toGo;
        lastSpeed = speed;
    }

    void FixedUpdate()
    {
        if (owner.isServer)
            UpdateMovement(Time.deltaTime);   
    }

    void OnDisable()
    {
        if (stopped) Unlock();
    }
}
