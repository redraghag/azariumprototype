﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;

public class MovementAI : PathAIBase
{
    private Seeker seeker;
    protected Vector2 direction;
    protected bool moveDirection;

    protected void Awake()
    {
        base.Awake();
        seeker = GetComponent<Seeker>();
    }

    protected override void EngageInner(Fighter engageTarget)
    {
        moveDirection = false;
        SetPath(null);
        currTarget = engageTarget;
        if (engageTarget == null) return;
        pathEnabled = collisions;
        if (pathEnabled)
            StartCoroutine(EngagePathUpdate(0.5f));
    }

    private bool pathEnabled = true;

    IEnumerator EngagePathUpdate(float interval)
    {
        while (currTarget != null && currTarget.valid)
        {
            if (currTarget == null) yield break;
            if (currTarget.tr == enemyCastle)
            {
                var p = XPath.Construct(tr.position, currTarget.tr.position, null);
                var r = enemyCastle.GetComponent<CapsuleCollider>().radius;
                p.endingCondition = new EndingConditionProximity(p, r + 0.5f);
                seeker.StartPath(p, path =>
                {
                    if (currTarget.valid == false) return;
                    SetPath(path);
                });
            }
            else
            {
                seeker.StartPath(tr.position, currTarget.tr.position, path =>
                {
                    if (currTarget.valid == false) return;
                    SetPath(path, 1);
                });
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    protected override void MoveDirectionInner(Vector2 direction)
    {
        this.direction = tr.position.ToFlatVector() + direction.normalized * 1000;
        moveDirection = true;
        currPath = null;
        currTarget = null;
    }

    void SetPath(Path path, int wpShift = 0)
    {
        if (currPath != null)
        {
            //currPath.Release(this);
            currPath = null;
        }
        if (pathEnabled == false) return;
        if (path != null && path.error) return;
        if (path != null && path.vectorPath[0].y > 2)
        {
            Debug.Log("Path error");
            return;
        }
        currPath = path;
        if (path == null)
        {
            return;
        }
        currWaypoint = wpShift;
    }

    [NonSerialized]
    private Path currPath;
    private int currWaypoint;
    [NonSerialized]
    Fighter currTarget;
    private float waypointDelta = 0.2f;

    protected override void UpdateMovement(float dt)
    {
        if (moveDirection)
        {
            MoveTo(direction, dt);
            return;
        }
        if (currTarget == null || currTarget.valid == false) return;
        if (currPath == null)
        {
            MoveTo(currTarget.position, dt);
            return;
        }
        if (Vector2.Distance(new Vector2(tr.position.x, tr.position.z), 
            new Vector2(currPath.vectorPath[currWaypoint].x, currPath.vectorPath[currWaypoint].z)) < waypointDelta)
        {
            currWaypoint++;
        }
        if (currWaypoint >= currPath.vectorPath.Count)
        {
            currPath = null;
            currTarget = null;
            return;
        }
        MoveTo(currPath.vectorPath[currWaypoint].ToFlatVector(), dt);
    }
}
