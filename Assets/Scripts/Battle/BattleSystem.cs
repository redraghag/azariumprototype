﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DG.Tweening;
using Prototype.NetworkLobby;
using UniRx;
using UnityEngine.Networking;
using UnityEngine;

enum BattleState
{
    WaitingForPlayers = 0,
    Started = 1,
    Paused = 2,
    Finished = 3
}

public class BattleSystem : BattleBehaviour
{
    public static BattleSystem instance;

    [NetworkDynamic] public IntCell state = new IntCell();
    [NetworkDynamic] public FloatCell serverTime = new FloatCell();
    [NetworkDynamic] public FloatCell lastSpawn = new FloatCell(-15);
    [NetworkDynamic] public IntCell azariumMineState = new IntCell();
    [NetworkDynamic] public IntCell waveNumner = new IntCell();
    [NonSerialized, HideInInspector] public GameData gameData;

    public SideState red;
    public SideState blue;

    private List<PlayerState> players = new List<PlayerState>();

    public List<BattleUnit> units = new List<BattleUnit>(); 
    public List<GlobalObject> objects = new List<GlobalObject>();

    public BoolCell paused = new BoolCell();
    public List<PlayerState> pausedPlayers = new List<PlayerState>(); 
    public void Pause(bool pause, PlayerState player)
    {
        if (pause) pausedPlayers.Add(player);
        else pausedPlayers.Remove(player);
        paused.value = pausedPlayers.Count > 0;
    }

    void Awake()
    {
        instance = this;
        gameData = GameController.instance.gameData;
    }

    void Start()
    {
        instance = this;
    }

    public override void OnStartServer()
    {
        Debug.Log("Battle server start");
        instance = this;
        InitTransport();
        StartCoroutine(WaitForPlayers());
    }

    IEnumerator WaitForPlayers()
    {
        var map = Map.current;
        while (players.Count != map.needPlayers)
        {
            players = FindObjectsOfType<PlayerState>().ToList();
            Debug.Log("Waiting players");
            yield return new WaitForSeconds(0.5f);
        }
        foreach (var playerState in players)
        {
            playerState.mana.value = settings.ManaStartValue;
            playerState.RpcInitStuff();
        }
        var psList = players.Split(map.needPlayers / 2);
        if (psList[0][0].side == Side.Blue)
        {
            blue.Init(psList[0]);
            red.Init(psList[1]);
        }
        else
        {
            blue.Init(psList[1]);
            red.Init(psList[0]);
        }

        Debug.Log("after load pause");
        yield return new WaitForSeconds(1f);
        StartCoroutine(SpawnCoro());
        StartCoroutine(UpdateAzariumCenterCoro());
        state.value = state.value = (int) BattleState.Started;
        yield break;
    }

    private float serverTimeBuffer;

    public float boom = 3f;

    [Server]
    void Update()
    {
        if (state.value != (int)BattleState.Started) return;
        if (paused.value) return;
        var dt = Time.deltaTime;
        serverTimeBuffer += dt;
        if (serverTimeBuffer > Battle.dt)
        {
            serverTimeBuffer -= Battle.dt;
            UpdateLogic(Battle.dt);
            //boom -= Battle.dt;
            //if (boom <= 0)
            //{
            //    red.tower.hp.value = 0;
            //}
        }
    }

    void UpdateLogic(float dt)
    {
        serverTime.value += dt;
        foreach (var p in players)
        {
            if (p == null)
            {
                Debug.Log("Player disconnected");
                LobbyManager.s_Singleton.GoBackButton();
                return;
            }
            p.PushTime(dt);
        }
        // Check if units are dead.
        for (int i = units.Count - 1; i >= 0; i--)
        {
            var unit = units[i];
            if (unit.hp.value <= 0)
            {
                OnUnitKilled(unit);
                units.RemoveAt(i);
            }
        }
        
        //TODO make good mevement update
        //foreach (var unit in units)
        //{
        //    if (unit.fighter.movement != null)
        //    {
        //        unit.fighter.movement.canMove
        //    }
        //}
        
        for (int i = objects.Count - 1; i >= 0; i--)
        {
            var obj = objects[i];
            obj.Update(dt);
            if (obj.Finished())
            {
                obj.OnFinished();
                objects.RemoveAt(i);
            }
        }
        // Updating behaviour logic.
        foreach (var unit in units)
        {
            unit.fighter.UpdateBehaviour(dt);
        }
        // Update auras, objects and interrupt effects
        foreach (var unit in units)
        {
            unit.fighter.Update(dt);
        }

        SideState victory = null;
        SideState defeate = null;
        if (red.tower.hp.value <= 0)
        {
            victory = blue;
            defeate = red;
        }
        else if (blue.tower.hp.value <= 0)
        {
            victory = red;
            defeate = blue;
        }
        if (victory != null)
        {
            foreach (var playerState in victory.players)
            {
                playerState.RpcOnFinish(true);
            }
            foreach (var playerState2 in defeate.players)
            {
                playerState2.RpcOnFinish(false);
            }
            state.value = (int) BattleState.Finished;
        }
    }

    public void RegisterGlobalEffect(GlobalObject obj)
    {
        objects.Add(obj);
    }
    
    public void InitGroundBurn(GlobalGroundBurn proj)
    {
        objects.Add(proj);
        RpcShowGlobalGroundBurn(proj.range, proj.timeLeft, proj.position.ToVolumeVector());
    }

    public void InitGlobalProjectile(GlobalProjectile proj, string model)
    {
        objects.Add(proj);
        RpcShowGlobalProjectile(model, proj.speed, proj.currentPos.ToVolumeVector(), proj.target.ToVolumeVector());
    }

    [ClientRpc]
    void RpcShowGlobalProjectile(string model, float speed, Vector3 startPos, Vector3 targetPos)
    {
        var prefab = Resources.Load("Projectiles/" + model);
        var projView = (Instantiate(prefab) as GameObject).GetComponent<global::Projectile>();
        var offset = new Vector3(0, 1, 0);
        projView.targetPos = targetPos;
        projView.speed = speed;
        projView.offset = offset;
        projView.transform.position = startPos + offset;
    }

    [ClientRpc]
    void RpcShowGlobalGroundBurn(float range, float duration, Vector3 startPos)
    {
        var prefab = Resources.Load("BurningGround");
        var projView = (Instantiate(prefab) as GameObject);
        var offset = new Vector3(0, 1, 0);
        projView.transform.position = startPos;
        projView.GetComponent<MeshRenderer>().material.color = Color.red;
        projView.transform.localScale = new Vector3(range * 2, 0.2f, range * 2);
        Destroy(projView, duration);
    }

    IEnumerator SpawnCoro()
    {
        foreach (var p in players)
        {
            p.gold.value = settings.StartGold;
            //p.gold.value += 300;
            //p.azarium.value += 30;
        }
        while (true)
        {
            var gold = (int)UnityEngine.Random.Range(settings.BaseMinGain + settings.BaseMinGainGrowth * waveNumner.value,
                settings.BaseMaxGain + settings.BaseMaxGainGrowth * waveNumner.value);
            foreach (var p in players)
            {
                p.gold.value += gold;
            }
            yield return new WaitForSeconds(waveNumner.value == 0 ? 25 :settings.WaveSpawnInterval);
            foreach (var p in players)
            {
                p.SpawnUnitsFromAllBuildings();
            }
            lastSpawn.value = serverTime.value;
            waveNumner.value++;
        }
    }


    IEnumerator UpdateAzariumCenterCoro()
    {
        Transform[] flags = {map.redCylinder, map.neutralCylinder, map.blueCylinder};
        Func<int, bool> hasUnits =
            mask => Physics.CheckSphere(map.center.transform.position, 10, mask);
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            int val = hasUnits(Battle.layerMaskBlue) ? 1 : 0;
            val += hasUnits(Battle.layerMaskRed) ? -1 : 0;
            if (val == 0) continue;
            azariumMineState.value = val;
            RpcUpdateFlagState(val);
        }
    }


    [ClientRpc]
    void RpcUpdateFlagState(int value)
    {
        Transform[] flags = {map.redCylinder, map.neutralCylinder, map.blueCylinder};
        for (int i = 0; i < 3; i++)
        {
            flags[i].DOMoveY((value + 1 == i) ? 1 : -8, 1);
        }
    }

    public static T BattlePrefabInst<T>(string name) where T : NetworkBehaviour
    {
        return Instantiate(Resources.Load<GameObject>("Battle/" + name)).GetComponent<T>();
    }

    public static T BattlePrefabInst<T>(string name, Transform parent) where T : NetworkBehaviour
    {
        var obj =(Instantiate(Resources.Load<GameObject>("Battle/" + name), parent, false) as GameObject).GetComponent<T>();
        obj.transform.localPosition = Vector3.zero;
        return obj;
    }
    public static void NetworkInstUnit(BattleUnit obj)
    {
        instance.units.Add(obj);
        NetworkServer.Spawn(obj.gameObject);
    }

    public void OnUnitKilled(BattleUnit unit)
    {
        unit.fighter.dead.Send();
        unit.fighter.ToSleep();
        //var goldGain = unit.info.totalCost.gold * instance.settings.GoldUnitKillCoeff;
        var goldGain = 0;
        var ps = MyEnemySide(unit.side).players;
        foreach (var player in ps)
        {
            player.gold.value += goldGain / ps.Count;
        }
        var pathAI = unit.GetComponent<MovementBase>();
        // we need to destroy unit and then reparse location where it was.
        Action reparsePath = null;
        if (unit.GetComponent<BuildingAI>() != null)
            reparsePath = pathAI.ReparseAction();
        Destroy(unit.gameObject);
        if (reparsePath != null)
            reparsePath();
    }

    public static void NetworkInst<T>(T obj) where T : NetworkBehaviour
    {
        NetworkServer.Spawn(obj.gameObject);
    }

    public SideState MySide(Side mySide)
    {
        return mySide == Side.Blue ? blue : red;
    }
    public SideState MyEnemySide(Side mySide)
    {
        return mySide == Side.Blue ? red : blue;
    }

    public BattleUnit WhatIsMyTarget(Side mySide)
    {
        return MyEnemySide(mySide).castle;
    }
}
