﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using ZergRush;

public class PlayerState : BattleBehaviour
{
    [HideInInspector]
    public RaceInfo race;

    [NetworkStatic]
    public int slotIndex;
    [NetworkStatic]
    public Side side;
    [NetworkStatic]
    public bool bot;

    [NetworkDynamic] public FloatCell gold = new FloatCell(400);
    [NetworkDynamic] public FloatCell mana = new FloatCell(400);
    [NetworkDynamic] public FloatCell azarium = new FloatCell(0);

    public InorganicCollection<Building> buildings = new InorganicCollection<Building>();
    public static PlayerState localPlayer;

    [HideInInspector]
    public PlayerSlot slot;

    public bool CanPay(Cost cost)
    {
        return gold.value >= cost.gold && azarium.value >= cost.azarium;
    }

    public ICell<bool> CanPayCell(Cost cost)
    {
        return gold.Merge(azarium, (g, az) => g >= cost.gold && az >= cost.azarium);
    }

    public void Pay(Cost cost)
    {
        gold.value -= cost.gold;
        azarium.value -= cost.azarium;
    }

    [ClientRpc]
    public void RpcInitStuff()
    {
        var s = BattleSystem.instance.MySide(this.side);
        slot = s.slot.playerSlots[slotIndex];
        race = gameData.availableRace[0];
        if (!isLocalPlayer) return;
        if (localPlayer != null)
        {
            bot = true;
            StartCoroutine(AIPlayerController.Coro(this));
        }
        else
        {
            localPlayer = this;
            BattleUI.instance.Init(this);
            builtForUi.Listen(BattleUI.instance.ShowNormalState);
        }
        foreach (var uslot in slot.buildingSlots)
        {
            uslot.spawnPoint.gameObject.SetActiveSafe(false);
        }
    }

    void Start()
    {
        Debug.Log("simple start called");
    }

    public override void OnStartClient()
    {
    }

    public override void OnStartServer()
    {
        InitTransport();
    }

    public override void OnStartLocalPlayer()
    {
    }

    private float tTotal;
    public void PushTime(float dt)
    {
        if (!isServer)
            return;
        tTotal += dt;
        if (tTotal > 5)
        {
            int val = (int) tTotal;
            //gold.value += val;
            var mineState = BattleSystem.instance.azariumMineState.value;
            var azMult =
                side == Side.Red && mineState == -1 ||
                side == Side.Blue && mineState == 1
                    ? 2
                    : 1;
            azarium.value += val*settings.AzariumRegen*azMult;
            tTotal -= val;

            mana.value = Mathf.Min(mana.value + settings.ManaRegen * val, settings.ManaMax);
        }

        //var currTime = system.serverTime.value;
        //foreach (var building in buildings)
        //{
        //    if (currTime - building.currentSpawnBeginning.value > settings.WaveSpawnInterval)
        //    {
        //        building.SpawnUnit();
        //        building.currentSpawnBeginning.value += settings.WaveSpawnInterval;
        //    }
        //}
    }

    public void SpawnUnitsFromAllBuildings()
    {
        foreach (var building in buildings)
        {
            building.SpawnUnit();
        }
    }

    public bool TryBuild(string buildingId)
    {
        if (buildings.Count == slot.buildingSlots.Count)
        {
            Debug.LogError("All slots are full");
            return false;
        }
        var prototype = race.bInfo.AtOrDefault(buildingId);
        if (prototype == null)
        {
            Debug.LogError("building with id not found: " + buildingId);
            return false;
        }
        if (!prototype.initial)
        {
            Debug.LogError("building with id: " + buildingId + " is upgrade");
        }
        if (!CanPay(prototype.cost))
        {
            Debug.LogError("player does not have enough resources");
            return false;
        }
        Pay(prototype.cost);
        Build(prototype);
        return true;
    }

    [Command]
    public void CmdPause(bool pause)
    {
        system.Pause(true, this);   
    }

    [Command]
    public void CmdBuild(string buildingId)
    {
        if (TryBuild(buildingId))
            RpcOnBuildingBuilt();
    }

    [Command]
    public void CmdUpgrade(int buildingIndex, int upIdex)
    {
        if (TryUpgrade(buildingIndex, upIdex))
            RpcOnBuildingBuilt();
    }

    [Command]
    public void CmdAzUnlock(int buildingIndex, int azAbility)
    {
        if (TryAzarioumUnlock(buildingIndex, azAbility))
            RpcOnAzUnlocked();
    }

    [Command]
    public void CmdSetSpawnPosition(GameObject spawnPoint, Vector3 pos)
    {
        spawnPoint.GetComponent<Transform>().position = pos;
    }

    EmptyStream builtForUi = new EmptyStream();
    [ClientRpc]
    protected virtual void RpcOnBuildingBuilt() { builtForUi.Send(); }

    [ClientRpc]
    protected virtual void RpcOnAzUnlocked()
    {
        //builtForUi.Send();
    }

    [Command]
    public void CmdCastSpell(int index, GameObject unit)
    {
        var spell = gameData.spells[index];
        if (mana.value < spell.manaCost)
        {
            Debug.LogError("not enough mana");
            return;
        }
        mana.value -= spell.manaCost;
        var u = unit.GetComponent<BattleUnit>().fighter;
        spell.Activate(u);
    }

    [Server]
    public bool TryUpgrade(int bIndex, int upIndex)
    {
        if (buildings.Count <= bIndex)
        {
            Debug.LogError("There is no building with index: " + bIndex);
            return false;
        }
        var building = buildings[bIndex];
        var ups = building.info.value.next.Select(val => race.bInfo.AtOrDefault(val)).ToList();
        
        if (ups.Count <= upIndex)
        {
            Debug.LogError("There is no up for building:" + building.info.value.name + " with this index: " + upIndex);
            return false;
        }
        var up = ups[upIndex];
        if (up == null)
        {
            Debug.LogError("this up is not found");
            return false;
        }
        if (!CanPay(up.cost))
        {
            Debug.LogError("player does not have enough resources");
            return false;
        }
        Pay(up.cost);
        building.UpdateInfo(up);
        if (!string.IsNullOrEmpty(up.unitToSpawn))
            slot.buildingSlots[building.slotIndex].spawnPoint.RpcInhabit(building.slotIndex, building.info.value.name);
        return true;
    }

    [Server]
    public bool TryAzarioumUnlock(int bIndex, int azAbility)
    {
        if (azAbility <= 0 || azAbility > 2)
        {
            Debug.LogError("Az ability index is out of range");
            return false;
        }
        if (buildings.Count <= bIndex)
        {
            Debug.LogError("There is no building with index: " + bIndex);
            return false;
        }
        var building = buildings[bIndex];
        var hasAzUps = building.info.value.azariupUpgradeDecription.Count > 0;
        
        if (hasAzUps == false)
        {
            Debug.LogError("There is no azarioum upgrades for this building");
            return false;
        }

        if (building.azariumUp.value != 0)
        {
            Debug.LogError("Azarium upgrade already unlocked for this building");
            return false;
        }

        if (azarium.value < settings.AbilityAzariumCost)
        {
            Debug.LogError("player does not have enough azarium");
            return false;
        }
        Debug.Log("Azarium ability: " + building.info.value.azariupUpgradeDecription[azAbility - 1] + ". Unlocked");
        azarium.value -= settings.AbilityAzariumCost;
        building.azariumUp.value = azAbility;
        return true;
    }


    [Server]
    void Build(BuildingInfo info)
    {
        var newB = BattleSystem.BattlePrefabInst<Building>("Building");
        var building = newB.GetComponent<Building>(); 
        building.slotIndex = buildings.Count;
        building.owner = this;
        building.currentSpawnBeginning.value = BattleSystem.instance.serverTime.value - info.spawnInterval;
        building.UpdateInfo(info);
        if (!string.IsNullOrEmpty(info.unitToSpawn))
            slot.buildingSlots[building.slotIndex].spawnPoint.RpcInhabit(building.slotIndex, building.info.value.name);
        buildings.Add(building);

        BattleSystem.NetworkInst(building);
        RpcBuildingComplete(newB.gameObject);
    }

    [ClientRpc]
    public void RpcBuildingComplete(GameObject newB)
    {
        if (isServer) return;
        var building = newB.GetComponent<Building>();
        buildings.Add(building);
    }

    [ClientRpc]
    public void RpcOnFinish(bool victory)
    {
        if (bot) return;
        if (isLocalPlayer == false) return;
        BattleUI.instance.ShowResult(victory);
    }
}
