﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class TestSpawner : MonoBehaviour
{
    public Side side;
    public Castle enemyBase;
    private int spawned;
	// Use this for initialization
	IEnumerator Start ()
	{
	    List<UnitInfo> infos = new List<UnitInfo>
	    {
	        new UnitInfo
	        {
	        },
            //new UnitInfo
            //{
            //    actions = new List<Ability> {new RangeAttack {range = 10f} }
            //},
            //new UnitInfo
            //{
            //    actions = new List<Ability> {new RangeSplashAttack {range = 10f} }
            //},
            //new UnitInfo
            //{
            //    actions = new List<Ability> {new HealAlly(), new RangeAttack {range = 6f} }
            //},
        };
	    var prefab = Resources.Load<GameObject>("UnitTest");
	    var tr = transform;
	    while (spawned < 50)
	    {
	        var unit = (Instantiate(prefab, tr.parent, false) as GameObject).GetComponent<SolderAI>();
	        unit.info = infos.RandomElement().Clone();
	        unit.info.model = "Archer";
	        unit.gameObject.layer = side.LayerForSide();
	        unit.side = side;
	        unit.transform.position = tr.position;
	        unit.GetComponent<MovementAI>().enemyCastle = enemyBase.transform;
	        spawned++;
	        yield return new WaitForSeconds(2.5f);
	    }
	}
	
}
