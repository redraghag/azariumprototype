﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniRx;

[Serializable]
public abstract class Aura : PassiveAbility, IAzariumLockable
{
    public int azariumNeed { get; set; }

    public virtual TargetType target
    {
        get { return TargetType.Friends; }
    }

    public float range;

    private float buffer;
    private float step = 0.5f;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.tick.Listen(dt =>
        {
            buffer += dt;
            if (buffer > step)
            {
                buffer -= step;
                UnityEngine.Debug.Log("check aura");
                var targets = target == TargetType.Enemies
                    ? carrier.EnemiesInRange(range, FlyingMask.All)
                    : carrier.AlliesInRange(range, true);
                foreach (var t in targets)
                {
                    t.GiveEffect(Affect(t), 1.2f, carrier, false);
                }
            }
        });
    }

    public abstract Func<PassiveAbility> Affect(Fighter target);
}


[Serializable]
public class RegenerationAura : Aura
{
    [ParseAttrib(1)] public float rate;
    public override bool isPositive
    {
        get { return true; }
    }

    public override Func<PassiveAbility> Affect(Fighter target)
    {
        return () => new Regeneration {rate = rate};
    }
}

[Serializable]
public class ArmorAura : Aura
{
    [ParseAttrib(1)] public float addArmor;
    public override bool isPositive
    {
        get { return true; }
    }

    public override Func<PassiveAbility> Affect(Fighter target)
    {
        return () => new AddArmor {armor = addArmor};
    }
}