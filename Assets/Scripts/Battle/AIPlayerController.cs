﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class AIPlayerController
{
    public static IEnumerator Coro(PlayerState player)
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            var bCOuntToPbserv = 5;
            IEnumerable<BuildingInfo> buildings = player.race.bInfo.Values.Where(b => b.initial);
            if (player.buildings.Count >= 9) yield break;
            for (int i = 0; i < bCOuntToPbserv; i++)
            {
                if (buildings.Take(bCOuntToPbserv).All(b => player.CanPay(b.cost)))
                {
                    player.CmdBuild(buildings.Take(bCOuntToPbserv).RandomElement().name);
                }
            }
            //for (int i = 0; i < 5; i++)
            //{
            //    player.CmdBuild("Apprentice");
            //}

            //for (int i = 0; i < 2; i++)
            //{
            //    player.CmdBuild("Rookie");
            //    player.CmdUpgrade(i, 0);
            //    player.CmdUpgrade(i, 0);
            //    player.CmdAzUnlock(i, 2);
            //}
            //yield break;
        }    
    }
}
