﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public abstract class GlobalObject
{
    public abstract void Update(float dt);
    public abstract bool Finished();
    public abstract void OnFinished();
}

public class GlobalProjectile : GlobalObject
{
    public Vector2 currentPos;
    public Vector2 target;
    public float speed = 40;
    public Action<Vector2> onHitGround;

    public override void Update(float tick)
    {
        currentPos = Vector2.MoveTowards(currentPos, target, speed * tick);
    }

    public override bool Finished()
    {
        return currentPos == target;
    }

    public override void OnFinished()
    {
        onHitGround(target);
    }
}

public class GlobalGroundBurn : GlobalObject
{
    public Vector2 position;
    public float range;
    public float damagePerSec;
    public float timeLeft;
    public int mask;

    private float step = 0.5f;
    private float buffer;
    public override void Update(float dt)
    {
        timeLeft -= dt;
        buffer += dt;
        if (buffer > step)
        {
            buffer -= step; 
            var units = Battle.UnitsInRange(position, mask, range, FlyingMask.Ground);
            foreach (var fighter in units)
            {
                fighter.ReceiveDamage(new DamageInfo
                {
                    damage = damagePerSec * step,
                    type = DamageType.magic,
                    armorIgnore = true,
                });
            }
        }
    }

    public override bool Finished()
    {
        return timeLeft <= 0;
    }

    public override void OnFinished()
    {
    }
}
