﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UniRx;
using UnityEngine;

public class TargettedProjectile
{
    public float speed = 20;
    public Vector2 currPos;
    public Fighter source;
    public Action<Fighter> affect;
    public bool reflected;
    public bool positive;
}

public class Fighter : Root<IOrganism, IRoot>
{
    public Side side;
    public FloatCell hp = new FloatCell();
    public AdditiveSubstance damage = new AdditiveSubstance();
    public MultiplicativeSubstance damageMultiplier = new MultiplicativeSubstance();
    public AdditiveSubstance armor = new AdditiveSubstance();
    public AdditiveSubstance maxHp = new AdditiveSubstance();
    public MultiplicativeSubstance debuffTimeMult = new MultiplicativeSubstance();
    public MultiplicativeSubstance speed = new MultiplicativeSubstance();
    public MultiplicativeSubstance attackSpeedDebuff = new MultiplicativeSubstance();
    public LastValueSubstance<float> overrideSpeed = new LastValueSubstance<float>(); 

    public BioCollection<Behaviour> actions = new BioCollection<Behaviour>();
    public BioCollection<PassiveAbility> passives = new BioCollection<PassiveAbility>();
    public BioCollection<Aura> auras = new BioCollection<Aura>();

    public List<PassiveAbility> passivesWithDuration = new List<PassiveAbility>(); 

    public Stream<Behaviour> activation = new Stream<Behaviour>();
    public EmptyStream dead = new EmptyStream();
    public Stream<float> tick = new Stream<float>(); 
    // This is to alter damage that this fighter deal in any way
    public Stream<DamageInfo> damageReceivePreparing = new Stream<DamageInfo>(); 
    // This is to alter damage that this fighter deal in any way
    public Stream<DamageInfo> damageDealPreparing = new Stream<DamageInfo>(); 
    // This is to react to damage this fighter deal but not change it 
    public Stream<DamageInfo> damageDealing = new Stream<DamageInfo>(); 

    public Stream<DamageInfo> directDamageReceived = new Stream<DamageInfo>(); 
    public Stream<DamageInfo> sideEffectDamageReceived = new Stream<DamageInfo>(); 
    public Stream<DamageInfo> anyDamageReceived = new Stream<DamageInfo>(); 
    public Stream<DamageInfo> anyDamageFinalized = new Stream<DamageInfo>(); 
    public Stream<TargettedProjectile> onProjectileHit = new Stream<TargettedProjectile>();
    public OrSubstance stunnImmune = new OrSubstance();

    public UnitInfo info;
    // Unity stuff
    public BattleUnit owner;
    [NonSerialized] public Transform tr;
    [NonSerialized] public int enemyMask;
    [NonSerialized] public int friendMask;
    [NonSerialized] public MovementBase movement;
    public bool valid { get { return owner != null; } }

    public void Init(BattleUnit owner)
    {
        side = owner.side;
        info = owner.info;
        tr = owner.transform;
        this.owner = owner;
        movement = owner.GetComponent<MovementBase>();
        ResetSpeed();
        if (movement != null)
        {
            speed.Bind(v => movement.speed = v, Priority.Post);
        }
        enemyMask = 1 << (side == Side.Blue ? Battle.layerRed : Battle.layerBlue);
        friendMask = 1 << (side == Side.Red ? Battle.layerRed : Battle.layerBlue);
        maxHp.baseValue = info.hp;

        armor.baseValue = info.armor;
        damage.baseValue = info.baseDamage;
        //owner.azariumBought.value = 1;

        foreach (var ability in info.actions)
        {
            if (ability.azariumNeed != 0 && ability.azariumNeed != owner.azariumBought.value)
                continue;
            var ab = ability.Clone();
            for (int i =  ability.damageMod.Count - 1; i >= 0; i--)
            {
                var azNeed = ability.damageMod[i].azariumNeed;
                if (azNeed != 0 && owner.azariumBought.value != azNeed)
                {
                    ab.damageMod.RemoveAt(i);
                }
            }
            if (ab is BaseAttack == false)
                ab.activation.Listen(() => owner.RpcAbilityActivated(ab.GetType().Name));
            actions.Add(ab);
        }
        foreach (var effect in info.effects)
        {
            if (effect.azariumNeed != 0 && effect.azariumNeed != owner.azariumBought.value)
                continue;
            passives.Add(effect.Clone());
        }
        foreach (var aura in info.auras)
        {
            if (aura.azariumNeed != 0 && aura.azariumNeed != owner.azariumBought.value)
                continue;
            auras.Add(aura.Clone());
        }

        hp.value = maxHp.value;
        maxHp.ListenUpdates(val => hp.value = Mathf.Min(hp.value, val), Priority.Normal);
        attackSpeedDebuff.ListenUpdates(owner.RpcSetAnimSpeed, Priority.Normal);

        this.owner = owner;
        this.Arise(null, null);
    }

    public void SetCollision(bool enable)
    {
        movement.collisions = enable;
    }

    public void SetSpeed(float s)
    {
        speed.baseValue = s;
    }

    public void GiveEffect(PassiveAbility effect, bool silent = false)
    {
        if (info.type == UnitType.Building) return;
        if (silent == false) owner.RpcEffectAdded(effect.GetType().Name);
        passives.Add(effect);
        effect.silent = silent;
        effect.removable = true;
        if (effect.timeLeft > 0) passivesWithDuration.Add(effect);
    }
    public void GiveEffect(Func<PassiveAbility> effect, float duration, Fighter effOwner, bool silent = false)
    {
        var alreadyExists = passives.FirstOrDefault(p => p.owner == effOwner);
        if (alreadyExists != null)
        {
            alreadyExists.timeLeft = Mathf.Max(alreadyExists.timeLeft, duration);
        }
        else
        {
            var eff = effect();
            eff.timeLeft = duration;
            eff.owner = effOwner;
            GiveEffect(eff, silent);
        }
    }

    public void AddCooldownToBehaviours(float time)
    {
        foreach (var behaviour in actions)
        {
            if (behaviour.cooldown > 0) behaviour.currentCooldown += time;
        }
    }

    public Vector2 position { get { return tr.position.ToFlatVector(); } }

    public void GiveAura(Aura aura)
    {
        auras.Add(aura);
        if (aura.timeLeft > 0) passivesWithDuration.Add(aura);
    }


    public void Stop()
    {
        movement.Stop();
    }

    public void ResetSpeed()
    {
        speed.baseValue = info.movementSpeed;
    }

    public void InitMoveTo(Fighter target)
    {
        if (info.movementSpeed == 0)
        {
            Debug.LogError("Speed of object is 0 it cant move");
            return;
        }
        movement.Engage(target);
    }

    public void InitMoveDirection(Vector2 direction)
    {
        movement.MoveDirection(direction.ToVolumeVector());
        owner.RpcOnMove(tr.position + direction.ToVolumeVector());
    }

    public void Interrupt()
    {
        ClearNehaviour();
    }

    BioCell<Behaviour> customBehav = new BioCell<Behaviour>(); 
    public void SetCustomBehaviour(Behaviour behaviour)
    {
        customBehav.value = behaviour;
        Behave(behaviour);
    }

    public void ClearNehaviour()
    {
        if (currentBehaviour == null) return;

        customBehav.value = null;
        currentBehaviour.currentCooldown = currentBehaviour.cooldown;
        currentBehaviour.Exit();
        currentBehaviour = null;
        currentBehaviourCoro = null;
        currBehaviourNextStep = 0;
    }

    public void Stun(float time)
    {
        stunPending = Mathf.Max(stunPending, time * debuffTimeMult.value);
    }

    public float stunPending;
    void ApplyStun(float time)
    {
        if (stunnImmune.value)
        {
            owner.RpcCannotStun();
            return;
        }
        Interrupt();       
        owner.RpcStunned(time);
        SetCustomBehaviour(new DoNothing {duration = time});
    }


    List<TargettedProjectile> projectiles = new List<TargettedProjectile>();
    public void CatchProjectile(TargettedProjectile projectile, string model = "Arrow")
    {
        projectiles.Add(projectile);
        owner.RpcNewProjectile(model, projectile.speed, projectile.source.tr.position);
    }

    public bool isFLying {get { return info.type == UnitType.Flying; }}
    public bool isGround { get { return !isFLying; } }
    public bool isMelee {get { return (info.type & UnitType.Melee) != 0; }}
    public bool healed {  get { return hp.value >= maxHp.value; } }

    public void Update(float dt)
    {
        for (int i = projectiles.Count - 1; i >= 0; i--)
        {
            var p =  projectiles[i];
            var selfPos = tr.position.ToFlatVector();
            p.currPos = Vector2.MoveTowards(p.currPos, selfPos, p.speed * dt);
            if (Vector2.Distance(p.currPos, selfPos) <= 0.5f)
            {
                onProjectileHit.Send(p);
                if (p.reflected && p.positive == false)
                {
                    var reflected = new TargettedProjectile
                    {
                        affect = p.affect, currPos = p.currPos, speed = p.speed, source = this
                    }; 
                    p.source.CatchProjectile(reflected);
                }
                else
                {
                    p.affect(this);
                }
                projectiles.RemoveAt(i);
            }
        }

        tick.Send(dt);

        bool passiveAsleep = true;
        while (passiveAsleep)
        {
            passiveAsleep = false;
            foreach (var passiveAbility in passivesWithDuration)
            {
                var pDt = dt;
                if (passiveAbility.isPositive == false)
                {
                    if (debuffTimeMult.value == 0) passiveAbility.timeLeft = 0;
                    else pDt /= debuffTimeMult.value;
                }
                passiveAbility.timeLeft -= pDt;
                if (passiveAbility.timeLeft <= 0)
                {
                    if (passiveAbility.silent == false) owner.RpcEffectRemoved(passiveAbility.GetType().Name);
                    passiveAbility.ToSleep();
                    passivesWithDuration.Remove(passiveAbility);
                    passiveAsleep = true;
                    break;
                }
            }
        }

        if (stunPending > 0)
        {
            ApplyStun(stunPending);
            stunPending = 0;
        }
    }

    [NotAChild]
    private Behaviour currentBehaviour;
    [NonSerialized]
    private IEnumerator<float> currentBehaviourCoro;
    [NonSerialized]
    private float currBehaviourNextStep;

    void Behave(Behaviour behaviour)
    {
        ClearNehaviour();
        currentBehaviour = behaviour;
        currentBehaviourCoro = currentBehaviour.Update();
        BehavStep(0);
    }

    void BehavStep(float dt)
    {
        currBehaviourNextStep -= dt * attackSpeedDebuff.value;
        if (currBehaviourNextStep <= 0)
        {
            if (currentBehaviourCoro.MoveNext())
            {
                currBehaviourNextStep = currentBehaviourCoro.Current;
            }
            else
            {
                ClearNehaviour();
            }
        }
    }

    public void UpdateBehaviour(float time)
    {
        foreach (var act in actions)
        {
            act.currentCooldown -= time;
        }
        if (currentBehaviour != null)
        {
            BehavStep(time);
        }
        bool hasSupportBehavMatch = false;
        if (currentBehaviour == null || currentBehaviour.supportBehaviour)
        {
            for (int i = actions.Count - 1; i >= 0; i--)
            {
                var act = actions[i];

                if (act.currentCooldown > 0 ) continue;
                if (act == currentBehaviour) hasSupportBehavMatch = true;
                if (act.supportBehaviour && hasSupportBehavMatch) continue;
                if (act.Prepare())
                {
                    Behave(act);
                    //TODO need to think about corectness of setting cooldown here
                    break;
                }
            }
        }
    }
}
