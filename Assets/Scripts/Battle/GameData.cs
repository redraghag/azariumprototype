﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class ParseAttrib : Attribute
{
    public int index;
    public string name;
    public ParseAttrib(int index)
    {
        this.index = index;
    }
    public ParseAttrib(string name)
    {
        this.name = name;
    }
}

class DistanceAttrib : Attribute
{
    
}

public interface IAzariumLockable
{
    int azariumNeed { get; set; }
}

[Serializable]
public class GameSettings
{
    public float WeaponTypeMatchBuff;
    public float WeaponTypeUnmatchBuff;
    public float AbilityAzariumCost;
    public float StartGold;
    public float BaseMinGain;
    public float BaseMaxGain;
    public float BaseMinGainGrowth;
    public float BaseMaxGainGrowth;
    public float StartAzarium;
    public float GoldUnitKillCoeff;
    public float ManaStartValue;
    public float ManaMax;
    public float ManaRegen;
    public float AzariumRegen;
    public float CenterCapturedBuff;
    public float WaveSpawnInterval;
    public float MeleeAttackRange;
    public float CastleHp;
    public float CastleArmor;
    public float TowerHp;
    public float TowerRange;
    public float TowerDamage;
    public float TowerAttackCooldown;
    public float DistanceMultiplier;
    public float AggroRange;
    public float MeleeMoveToEngageRange;
    public float GlobMovementSpeed;
}

[Flags]
public enum UnitType
{
    Melee = 1,
    Ranged = 2,
    Building = 4,
    Flying = 8,
    GroundTargetting = 16
}

[Serializable]
public class UnitInfo
{
    public string name = "default";
    public string model = "Archer";
    public int hp = 100;
    public float movementSpeed = 0f;
    public int armor;
    public float aggroRange = 20;
    public float size = 0.40f;
    public Cost totalCost = new Cost();
    public ArmorType armorType;
    public DamageType damageType;
    public float baseDamage;
    public List<Behaviour> actions = new List<Behaviour>();
    public List<PassiveAbility> effects = new List<PassiveAbility>();
    public List<Aura> auras = new List<Aura>();
    public UnitType type;
    public bool flying;
}

[Serializable]
public class BuildingInfo
{
    public string name;
    public string icon;
    public Cost cost;
    public string unitToSpawn;
    public float spawnInterval;
    public string model;
    public bool initial;
    public List<string> abilityDescription = new List<string>();
    public List<string> abilityDescriptionShort = new List<string>();
    public List<string> azariupUpgradeDecription = new List<string>();
    public List<string> azariupUpgradeDecriptionShort = new List<string>();
    public List<string> next = new List<string>();
    public List<BuildingsObj> effects = new List<BuildingsObj>();

    public bool functional { get { return string.IsNullOrEmpty(unitToSpawn); } }
}

[Serializable]
public class RaceInfo
{
    public string name;
    public Dictionary<string, BuildingInfo> bInfo;
    public string towerModel;
}

[Serializable]
public class Cost
{
    public float gold;
    public float azarium;

    public override string ToString()
    {
        return "Gold:" + gold + (azarium > 0 ? " Az:" + azarium : "");
    }
}

[Serializable]
public class GameData
{
    public GameSettings settings;
    public List<RaceInfo> availableRace;
    public Dictionary<string, UnitInfo> uInfo;
    public List<Spell> spells = new List<Spell>();
}

