﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngineInternal;

[Serializable]
public class ChargeForward : Behaviour
{
    [ParseAttrib(1)] public float damageMult;
    [ParseAttrib(2), DistanceAttrib] public float speed;
    public float finishRange = 2;

    [NonSerialized, NotAChild] private Fighter target;

    public override bool Prepare()
    {
        var enemies = carrier.UnitsInRangeExcludingCenter(carrier.enemyMask, range, FlyingMask.Ground);
        target = enemies.FirstOrDefault(e => (e.info.type & UnitType.Ranged) != 0);
        if (target == null) return false;
        return true;
    }

    protected virtual void AndDo(Fighter target)
    {
        
    }

    protected List<Fighter> unitsHit = new List<Fighter> {};
    public override IEnumerator<float> Update()
    {
        unitsHit.Clear();
        unitsHit.Add(target);
        carrier.SetCollision(false);
        carrier.SetSpeed(speed);
        carrier.InitMoveTo(target);
        while (carrier.Distance(target) > finishRange)
        {
            yield return 0;
            if (target.valid == false) yield break; 
            foreach (var enemy in carrier.EnemiesInRange(3, FlyingMask.Ground))
            {
                if (unitsHit.Contains(enemy)) continue;
                this.DoBaseDamageTo(enemy, isMelee:true, damageMult:damageMult);
                unitsHit.Add(enemy);
            }
        }
        activation.Send();
        activationOnTarget.Send(target);
        this.DoBaseDamageTo(target, isMelee:true, damageMult:damageMult);
        AndDo(target);
    }

    public override void Exit()
    {
        carrier.ResetSpeed();
        carrier.SetCollision(true);
    }
}

[Serializable]
public class ChargeForwardAndDamageInLine : ChargeForward
{
    [ParseAttrib(3), DistanceAttrib] public float width;
    [ParseAttrib(4), DistanceAttrib] public float depth;
    protected override void AndDo(Fighter target)
    {
        var units = target.position.UnitsInRect(carrier.position, carrier.enemyMask, width, depth, 1f);
        foreach (var t in units)
        {
            if (unitsHit.Contains(t)) continue;
            this.DoBaseDamageTo(t, isMelee:true, isSideEffect:true);
        }
    }
}

[Serializable]
public abstract class SimpleStayStillAction : Behaviour
{
    [NonSerialized, NotAChild]
    public Fighter target;
    public float timeToCast;
    public float startAnimationPart = 0.6f;

    public float startAttackTime { get { return timeToCast*startAnimationPart; } }
    public float endAttackTime { get { return timeToCast*(1 - startAnimationPart); } }

    public override IEnumerator<float> Update()
    {
        carrier.Stop();
        bool hasTarget = target != null;
        yield return startAttackTime;
        this.ActivationProcess(target);
        if (hasTarget == false || target.owner != null)
            Activate();
        yield return endAttackTime;
        target = null;
    }

    public abstract void Activate();
    public override void Exit()
    {
        //carrier.EnableMovement(true);
    }
}

[Serializable]
public class MoveToEnemyBase : Behaviour
{
    public override bool supportBehaviour
    {
        get { return true; }
    }

    public override bool Prepare()
    {
        return true;
    }

    public override IEnumerator<float> Update()
    {
        carrier.InitMoveDirection(new Vector3(carrier.side == Side.Blue ? -1 : 1, 0, 0));
        while (true)
        {
            yield return 1;
        }
    }

    public override void Exit()
    {
        carrier.Stop();
    }
}

[Serializable]
public class MoveToEngage : Behaviour
{
    public override bool supportBehaviour
    {
        get { return true; }
    }
    [NonSerialized, NotAChild]
    private Fighter target;
    public override bool Prepare()
    {
        target = carrier.NearesEnemy(range);
        return target != null;
    }

    public override IEnumerator<float> Update()
    {
        carrier.InitMoveTo(target);
        var cd = 2f;
        while (target.owner != null && cd >= 0)
        {
            yield return 1;
            cd -= 1;
        }
    }

    public override void Exit()
    {
        carrier.Stop();
    }
}

[Serializable]
public class DoNothing : Behaviour
{
    public float duration;
    public override bool Prepare()
    {
        return true;
    }

    public override IEnumerator<float> Update()
    {
        carrier.Stop();
        yield return duration;
    }

    public override void Exit()
    {
    }
}

[Serializable]
public abstract class SingleTargetEngagement : SimpleStayStillAction
{
    public override bool Prepare()
    {
        if (carrier.HasEnemiesInRange(range) == false) return false;
        if (targetType == TargetType.Friends)
            target = carrier.AlliesInRange(range, false)
                .Best(a => carrier.side == Side.Blue ? -a.position.x : a.position.x);
        else 
            target = carrier.NearesEnemy(range);
        if (target == null) return false;
        owner.RpcDoAttack(target.tr.position);
        return true;
    }
}

[Serializable]
public abstract class EnemyInRangeEngagement : SimpleStayStillAction
{
    [NonSerialized]
    private Vector3 pos;
    public override bool Prepare()
    {
        target = carrier.NearesEnemy(range);
        if (target == null) return false;
        pos = target.position;
        owner.RpcDoAttack(target.tr.position);
        return true;
    }

    public override void Activate()
    {
        activation.Send();
        if (target.valid)
        {
            activationOnTarget.Send(target);
            pos = target.position;
        }
        Activate(pos, target.valid ? target : null);
    }
    public abstract void Activate(Vector2 pos, Fighter target);
}

[Serializable]
public abstract class BaseAttack : SingleTargetEngagement
{
}

[Serializable]
public class MeleeAttack : BaseAttack
{
    public override void Activate()
    {
        this.DoBaseDamageTo(target, isMelee: true);
    }
}

[Serializable]
public class RangeAttack : BaseAttack
{
    public float projectileSpeed = 20;
    public override void Activate()
    {
        DoRangeAttack(target);
    }

    public void DoRangeAttack(Fighter unit)
    {
        var d = this.ComposeBaseAttack();
        unit.CatchProjectile(new TargettedProjectile
        {
            currPos = carrier.tr.position.ToFlatVector(),
            source = carrier,
            speed = projectileSpeed,
            affect = t => this.DoAbilityDamageTo(t, d)
        });
    }
}

[Serializable]
public class RangeAttackAddFlying : RangeAttack
{
    public override void Activate()
    {
        DoRangeAttack(target);
        var addTarget = (Fighter) null;
        if (target.isFLying == false)
        {
            addTarget = carrier.EnemiesInRange(range, FlyingMask.Flying).FirstOrDefault();
        }
        else
        {
            addTarget = carrier.EnemiesInRange(range, FlyingMask.Ground).FirstOrDefault();
        }
        if (addTarget != null) DoRangeAttack(addTarget);
    }
}

[Serializable]
public class Stun : SingleTargetEngagement
{
    [ParseAttrib(1)] public float stunTime;
    public override void Activate()
    {
        target.Stun(stunTime);
    }
}

[Serializable]
public class GiveEffectAlly : SingleTargetEngagement
{
    [ParseAttrib(1)] public float duration;
    [ParseAttrib(2), NotAChild] public PassiveAbility effect;

    public override TargetType targetType
    {
        get { return TargetType.Friends; }
    }

    public override void Activate()
    {
        var eff = effect.Clone();
        eff.timeLeft = duration;
        target.GiveEffect(eff);
    }
}


[Serializable]
class RangeSplashAttack : BaseAttack
{
    public float arrowSpeed = 15f;
    public int maxTargetCount = 3;

    public override void Activate()
    {
        var units = target.AlliesInRange(range, true);
        for (int i = 0; i < Mathf.Min(maxTargetCount, units.Length); i++)
        {
            var uCopy = units[i];
            var d = this.ComposeBaseAttack();
            uCopy.CatchProjectile(new TargettedProjectile
            {
                currPos = carrier.position,
                source = carrier,
                speed = 20,
                affect = t => this.DoAbilityDamageTo(t, d)
            });
        }
    }
}

[Serializable]
abstract class RangeGlobalAttackBase : EnemyInRangeEngagement
{
    public override void Activate(Vector2 pos, Fighter target)
    {
        var targetPos = pos;
        var startPos = carrier.position;
        
        BattleSystem.instance.InitGlobalProjectile(new GlobalProjectile
        {
            currentPos = carrier.position,
            target = targetPos,
            onHitGround = p => Affect(startPos, p),
        },"LargeSphere");
    }

    public abstract void Affect(Vector2 positionStart, Vector2 positionEnd);
}


[Serializable]
class RangeAoeAttack : RangeGlobalAttackBase
{
    [ParseAttrib(1), DistanceAttrib] public float aoeRange = 10;

    public override void Affect(Vector2 posStart, Vector2 position)
    {
        var units = position.UnitsInRange(carrier.enemyMask, aoeRange, FlyingMask.Ground);
        foreach (var t in units)
        {
            this.DoBaseDamageTo(t, isSideEffect:true);
        }
    }
}
[Serializable]
class RangeBurningGround : RangeGlobalAttackBase
{
    [ParseAttrib(1), DistanceAttrib] public float aoeRange;
    [ParseAttrib(2)] public float burnPercent;
    [ParseAttrib(3)] public float duration;

    public override void Affect(Vector2 posStart, Vector2 position)
    {
        var units = position.UnitsInRange(carrier.enemyMask, aoeRange, FlyingMask.Ground);
        foreach (var t in units)
        {
            this.DoBaseDamageTo(t, isSideEffect:true);
        }
        BattleSystem.instance.InitGroundBurn(new GlobalGroundBurn
        {
            range = aoeRange,
            damagePerSec = carrier.damage.value * burnPercent / duration,
            mask = carrier.enemyMask,
            position = position,
            timeLeft = duration
        });
    }
}

[Serializable]
class RangeSquareAoe : RangeGlobalAttackBase
{
    [ParseAttrib(1), DistanceAttrib] public float width = 10;
    [ParseAttrib(2), DistanceAttrib] public float depth = 10;

    public override void Affect(Vector2 posStart, Vector2 position)
    {
        var units = position.UnitsInRect(posStart, carrier.enemyMask, width, depth, 0.2f);
        foreach (var t in units)
        {
            this.DoBaseDamageTo(t, isSideEffect:true);
        }
    }
}

[Serializable]
class HealSelfOrAlly : SimpleStayStillAction
{
    [ParseAttrib(1)]
    public float healAmmount = 7;

    public override TargetType targetType
    {
        get { return TargetType.Friends; }
    }

    public override bool Prepare()
    {
        target = carrier.AlliesInRange(range, true).Where(f => f.hp.value < f.maxHp.value && f.info.type != UnitType.Building).Best(f => f.hp.value / f.info.hp);
        return target != null;
    }

    public override void Activate()
    {
        var d = new DamageInfo {damage = -healAmmount};
        target.CatchProjectile(new TargettedProjectile
        {
            currPos = carrier.tr.position,
            source = carrier,
            affect = t => this.DoAbilityDamageTo(t, d),
            positive = true
        }, "Sphere");
    }
}

[Serializable]
class WhenEnemiesInRange : SingleTargetEngagement
{
    public override void Activate()
    {
        
    }
}

[Serializable]
class AddEffectAoeEnemy : SimpleStayStillAction
{
    [ParseAttrib(1), DistanceAttrib] public float activationRange;
    [ParseAttrib(2)] public float duration;
    [ParseAttrib(3), NotAChild] public PassiveAbility effect;
    public override bool Prepare()
    {
        return carrier.HasEnemiesInRange(activationRange);
    }

    public override void Exit()
    {
    }

    public override void Activate()
    {
        foreach (var fighter in carrier.EnemiesInRange(range,FlyingMask.All))
        {
            var eff = effect.Clone();
            eff.timeLeft = duration;
            fighter.GiveEffect(eff);
        }
    }
}
