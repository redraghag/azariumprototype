﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class FighterSubOrgan : Organism<Fighter, Fighter>
{
    public BattleUnit owner { get { return carrier.owner; } }
    public UnitInfo info { get { return carrier.owner.info; } }
    public GameSettings settings { get { return BattleSystem.instance.settings; } }
}

[Serializable]
public abstract class Behaviour : FighterSubOrgan, IAzariumLockable
{
    // Static info.
    public float cooldown;
    public DamageType damageType;
    public float range;

    public BioCollection<DamageModifier> damageMod = new BioCollection<DamageModifier>();

    public Stream<Fighter> activationOnTarget = new Stream<Fighter>();
    public EmptyStream activation = new EmptyStream();
    public Stream<DamageInfo> damagePreparing = new Stream<DamageInfo>(); 
    public Stream<DamageInfo> damageDealing = new Stream<DamageInfo>(); 

    // dynamic info.
    public float currentCooldown;
    public abstract bool Prepare();
    public virtual TargetType targetType {get {return TargetType.Enemies;} }
    // All support behaviours will be interrupted buy other behaviours if any
    public virtual bool supportBehaviour {get {return false;} }
    public abstract IEnumerator<float> Update();
    public abstract void Exit();
    public int azariumNeed { get; set; }
}
