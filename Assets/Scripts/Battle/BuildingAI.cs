﻿using UnityEngine;
using System.Collections;

public class BuildingAI : BattleUnit
{
    private MovementBase pathAI;
	public override void OnStartServer()
    {
	    pathAI = GetComponent<MovementBase>();
        pathAI.ReparseNodes();
	    // Dirty hack due to bad physics.
	    StartCoroutine(ReduceSize());
    }

    IEnumerator ReduceSize()
    {
        yield return new WaitForSeconds(1);
        var hasCapsule = GetComponent<CapsuleCollider>();
        if (hasCapsule != null)
	        hasCapsule.radius -= 0.3f;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        var viewPrefab = Resources.Load<GameObject>("TowerView");
        if (viewPrefab == null)
        {
            Debug.LogError("prefab load error");
            return;
        }
        var viewObj = Instantiate(viewPrefab, transform, false) as GameObject;
        viewObj.transform.localPosition = Vector3.zero;
        clientView = viewObj.GetComponent<BaseUnitView>();
    }
}
