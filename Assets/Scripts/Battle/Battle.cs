﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public enum FlyingMask
{
    Ground,
    All,
    Flying
}

public static class Battle
{
    public static float dt = 0.1f;
    public static int layerRed = 8;
    public static int layerBlue = 9;

    public static int layerMaskRed = 1 << layerRed;
    public static int layerMaskBlue = 1 << layerBlue;

    public static int LayerForSide(this Side side)
    {
        if (side == Side.Blue) return layerBlue;
        return layerRed;
    }

    public static int Mask(this Side side)
    {
        if (side == Side.Blue) return layerMaskBlue;
        return layerMaskRed;
    }

    public static Side Opposite(this Side side)
    {
        if (side == Side.Blue) return Side.Red;
        return Side.Blue;
    }

    public static bool NeedGroundEnemy(this UnitType type)
    {
        return type == UnitType.Melee || (type & UnitType.GroundTargetting) != 0;
    }

    public static void SetMoveDirection(this Transform tr, Vector3 dir)
    {
        if (tr == null) return;
        //var moveTo = dir.Flatterned();
        //moveTo.y = currPos.y;
        tr.LookAt(dir);
        var angles = tr.localEulerAngles;
        angles.x = 0;
        angles.z = 0;
        tr.localEulerAngles = angles;
    }

    public static Vector2 ToFlatVector(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    public static Vector3 Flatterned(this Vector3 v)
    {
        return new Vector3(v.x, 0, v.z);
    }

    public static Vector3 ToVolumeVector(this Vector2 v, float y = 0)
    {
        return new Vector3(v.x, y, v.y);
    }

    public static int MaskForTarget(this TargetType target, GameObject self)
    {
        int mask = 0;
        switch(target)
        {
            case TargetType.Friends: mask = 1 << self.layer; break;
            case TargetType.Enemies: mask = 1 << ((self.layer == layerRed) ? layerBlue : layerRed); break;
            case TargetType.All: mask = 1 << layerRed & 1 << layerBlue; break;
        }
        return mask;
    }

    public static Fighter[] UnitsInRange(this Vector2 center, int layerMask, float range, FlyingMask flying)
    {
        var units = CollidersInRange(center.ToVolumeVector(), range, flying, layerMask);
        Fighter[] result = new Fighter[units.Length];
        for (int i = 0; i < units.Length; i++)
        {
            result[i] = units[i].GetComponent<BattleUnit>().fighter;
        }
        return result;
    }

    // persentDirectionOffset 0.5 means rect center will be on target center, 1 means center will be on beginning of strike zone.
    public static Fighter[] UnitsInRect(this Vector2 center, Vector2 sourceDirection, int layerMask, float width, float depth, float persentDirectionOffset)
    {
        center = Vector2.MoveTowards(center, sourceDirection, depth * (0.5f - persentDirectionOffset));
        var units =Physics.OverlapBox(center.ToVolumeVector(), new Vector3(width, 1, depth),
            Quaternion.LookRotation(sourceDirection.ToVolumeVector(), center.ToVolumeVector()), layerMask);
        Fighter[] result = new Fighter[units.Length];
        for (int i = 0; i < units.Length; i++)
        {
            result[i] = units[i].GetComponent<BattleUnit>().fighter;
        }
        return result;
    }

    public static Fighter[] EnemiesInRange(this Fighter fighter, float range, FlyingMask flying)
    {
        return UnitsInRange(fighter.position, fighter.enemyMask, range, flying);
    }
    public static Fighter[] AlliesInRange(this Fighter fighter, float range, bool includeSelf)
    {
        if (includeSelf)
            return UnitsInRange(fighter.position, fighter.friendMask, range, FlyingMask.All);
        else 
            return UnitsInRangeExcludingCenter(fighter, fighter.friendMask, range, FlyingMask.All);
    }

    public static Fighter NearesEnemy(this Fighter fighter, float range)
    {
        float dist;
        return NearestUnit(fighter, fighter.side.Opposite(), range, fighter.info.type.NeedGroundEnemy() ? FlyingMask.Ground : FlyingMask.All, out dist);
    }

    public static Fighter[] UnitsInRangeExcludingCenter(this Fighter center, int layerMask, float range, FlyingMask flying)
    {
        var transform = center.tr;
        var units = CollidersInRange(transform.position, range, flying, layerMask);
        if (units.Length == 0) return new Fighter[0];

        Fighter[] result = new Fighter[units.Length - 1];
        int j = 0;
        for (int i = 0; i < units.Length; i++)
        {
            var u = units[i].GetComponent<BattleUnit>().fighter;
            if (u != center)
            {
                if (j >= result.Length)
                {
                    Debug.Log("wtf");
                    continue;
                }
                result[j] = u;
                j++;
            }
        }
        return result;
    }

    public static bool HasEnemiesInRange(this Fighter fighter, float range)
    {
        if (fighter.isMelee)
            return CollidersInRange(fighter.tr.position, range, FlyingMask.Ground, fighter.enemyMask).Any();
        return Physics.CheckCapsule(fighter.tr.position - new Vector3(0, 10, 0), fighter.tr.position + new Vector3(0, 10, 0), range, fighter.enemyMask);
    }
    static Collider[] CollidersInRange(Vector3 pos, float range, FlyingMask flying, int mask)
    {
        var units = Physics.OverlapCapsule(pos - new Vector3(0, 10, 0), pos + new Vector3(0, 10, 0), range, mask);
        if (flying == FlyingMask.Ground)
            units = units.Where(u => u.GetComponent<BattleUnit>().fighter.isFLying == false).ToArray();
        else if (flying == FlyingMask.Flying)
            units = units.Where(u => u.GetComponent<BattleUnit>().fighter.isFLying).ToArray();
        return units;
    }
    
    public static Fighter NearestUnit(this Fighter center, Side side, float range, FlyingMask flying, out float distance)
    {
        var transform = center.tr;
        var units = CollidersInRange(transform.position, range, flying, side.Mask());
        // if center unit mask is different than searching one fast return unit if it is single.
        if (center.side != side && units.Length == 1)
        {
            distance = Vector3.Distance(transform.position, units[0].transform.position);
            return units[0].GetComponent<BattleUnit>().fighter;
        }
        // if mask is same than one unit found is the center itself.
        if (units.Length <= 1)
        {
            distance = 0;
            return null;
            //distance = Vector3.Distance(transform.position, units[0].transform.position); 
            //return units[0].GetComponent<BattleUnit>();
        }
        var nearest = (Collider)null;
        float minDistance = range + 100; 
        for (int i = 0; i < units.Length; i++)
        {
            var unit = units[i];
            var uTransform = unit.transform;
            if (uTransform == transform) continue;
            var dist = Vector3.Distance(transform.position, uTransform.position);
            if (dist < minDistance)
            {
                nearest = unit;
                minDistance = dist;
            }
        }
        distance = minDistance;
        return nearest.GetComponent<BattleUnit>().fighter;
    }

    public static void DoAbilityDamageTo(this Behaviour source, Fighter target, DamageInfo damage)
    {
        damage.target = target;
        damage.sourceAbility = source;
        damage.source = source.carrier;
        DamageInfoCommit(damage, target);
    }

    public static void DoGeneralDamageTo(this Fighter source, Fighter target, DamageInfo damage)
    {
        damage.target = target;
        damage.source = source;
        DamageInfoCommit(damage, target);
    }

    public static void DamageInfoCommit(this DamageInfo info, Fighter target)
    {
        info.target = target;
        if (info.damage > 0)
        {
            if (info.sourceAbility != null)
                info.sourceAbility.damagePreparing.Send(info);
            if (info.source != null)
                info.source.damageDealPreparing.Send(info);
        }
        if (target.valid == false) return;
        {
            ReceiveDamage(target, info);
        }
        if (info.applyAoe)
        {
            var fmask = target.info.type == UnitType.Flying ? FlyingMask.Flying : FlyingMask.Ground;
            foreach (var unit in target.position.UnitsInRange(target.friendMask, info.aoeRange, fmask)
                .Take(info.aoeMaxEnemyCount > 0 ? info.aoeMaxEnemyCount : 100))
            {
                var newInfo = info.Copy();
                newInfo.isSideEffect = true;
                newInfo.applyAoe = false;
                newInfo.target = unit;
                ReceiveDamage(unit, info);
            }
        }
    }

    //none, light, heavy, magic
    private static float[][] damageTable =
    {
        new float[]{0,0,0,0},
        new float[]{0,0,-1,1},
        new float[]{0,1,0,-1},
        new float[]{0,-1,1,0},
    };

    public static float ClashDamageTypes(DamageType dType, ArmorType aType)
    {
        var mod = damageTable[(int) dType][(int) aType];
        if (mod == 0) return 1;
        var settings = GameController.instance.gameData.settings;
        return mod == -1 ? settings.WeaponTypeUnmatchBuff : settings.WeaponTypeMatchBuff;
    }

    public static DamageType BestDamageTypeFor(ArmorType armor)
    {
        if (armor == ArmorType.heavy) return DamageType.magic;
        if (armor == ArmorType.light) return DamageType.heavy;
        if (armor == ArmorType.magic) return DamageType.light;
        return DamageType.none;
    }

    public static bool Matched(DamageType dType, ArmorType aType)
    {
        return (int) dType == (int) aType;
    }

    public static void ReceiveDamage(this Fighter target, DamageInfo damage)
    {
        target.damageReceivePreparing.Send(damage);
        // We have a final damage now.
        if (damage.damage > 0)
        {
            if (damage.sourceAbility != null)
                damage.sourceAbility.damageDealing.Send(damage);
            if (damage.source != null)
                damage.source.damageDealing.Send(damage);
            if (damage.isSideEffect)
                target.sideEffectDamageReceived.Send(damage);
            else
                target.directDamageReceived.Send(damage);
            target.anyDamageReceived.Send(damage);
        }
        if (damage.damage == 0)
        {
            Debug.Log("Immune");
        }
        //ApplyDamageTo(target, damage.damage, damage.type, damage.armorIgnore, damage.cantBeHealed);
        ApplyDamageTo(target, damage);
    }

    public static void ApplyDamageTo(Fighter target, DamageInfo info)
    {
        var final = info.damage;
        if (info.armorIgnore == false && info.damage > 0)
            final = Mathf.Max(0, final - target.armor.value);
        final = final * ClashDamageTypes(info.type, target.info.armorType);
        info.damage = final;
        target.anyDamageFinalized.Send(info);
        target.hp.value = Mathf.Min(target.hp.value - final, target.maxHp.value);
        if (final > 0 && info.cantBeHealed) target.maxHp.baseValue -= final;
        target.owner.RpcDamageReceived(final);
    }

    public static void ApplyDamageTo(Fighter target, float damage, DamageType type, bool ignoreArmor = false, bool cantBeHealed = false)
    {
        var final = damage;
        if (ignoreArmor == false && damage > 0)
            final = Mathf.Max(0, final - target.armor.value);
        final = final * ClashDamageTypes(type, target.info.armorType);
        
        target.hp.value = Mathf.Min(target.hp.value - final, target.maxHp.value);
        if (final > 0 && cantBeHealed) target.maxHp.baseValue -= final;
        target.owner.RpcDamageReceived(final);
    }

    public static float Distance(this Fighter unit1, Fighter unit2)
    {
        return Vector2.Distance(unit1.tr.position.ToFlatVector(), unit2.tr.position.ToFlatVector());
    }

    public static DamageInfo ComposeBaseAttack(this Behaviour source, float damageMult = 1, bool isMelee = false, bool isSideEffect = false)
    {
        DamageInfo info = new DamageInfo
        {
            type = source.damageType,
            damage = source.carrier.damage.value * source.carrier.damageMultiplier.value * damageMult,
            isMelee = isMelee,
            isSideEffect = isSideEffect,
        };
        return info;
    }

    public static void DoBaseDamageTo(this Behaviour source, Fighter target, bool isMelee = false, bool isSideEffect = false, float damageMult = 1)
    {
        DamageInfo info = new DamageInfo
        {
            type = source.damageType,
            damage = source.carrier.damage.value * source.carrier.damageMultiplier.value * damageMult,
            isMelee = isMelee,
            isSideEffect = isSideEffect
        };
        DoAbilityDamageTo(source, target, info);
    }

    public static void ActivationProcess(this Behaviour self, Fighter target)
    {
        self.activation.Send();
        if (target != null) self.activationOnTarget.Send(target);
    }

    public static void HealAbility(this Behaviour action, float healValue, Fighter target)
    {
        DamageInfo info = new DamageInfo
        {
            type = DamageType.none,
            damage = -healValue,
        };
        DoAbilityDamageTo(action, target, info);
    }
    public static void Heal(this Fighter self, float healValue, Fighter target)
    {
        DamageInfo info = new DamageInfo
        {
            type = DamageType.none,
            damage = -healValue,
        };
        DoGeneralDamageTo(self, target, info);
    }
}