﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


[Serializable]
public abstract class BuildingsObj : GlobalObject
{
    public bool expired;
    public Side side;
    public float cooldown;

    public override bool Finished()
    {
        return expired;
    }

    public override void OnFinished()
    {
    }
}

[Serializable]
public abstract class ActivatedBuildingsEffect : BuildingsObj
{

    private float buffer;
    public override void Update(float dt)
    {
        buffer += dt;
        if (buffer > cooldown)
        {
            buffer -= cooldown; 
            Activate();
        }
    }

    public abstract void Activate();
}

[Serializable]
public class HealingRandomAllyGlobalEffect : ActivatedBuildingsEffect
{
    [ParseAttrib(1)] public float healAmmount;

    public override void Activate()
    {
        var target = BattleSystem.instance.units.Where(
            u => u.side == side && u.fighter.healed == false && u.info.type != UnitType.Building).RandomElement();
        if (target != null)
            target.fighter.ReceiveDamage(new DamageInfo {damage = -healAmmount});
    }
}
[Serializable]
public class StunRandomEnemyGlobalEffect : ActivatedBuildingsEffect
{
    [ParseAttrib(1)] public float stun;

    public override void Activate()
    {
        var target = BattleSystem.instance.units.Where(
            u => u.side != side && u.info.type != UnitType.Building).RandomElement();
        if (target != null)
            target.fighter.Stun(stun);
    }
}