﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

class SyncronizedCell : Attribute
{
    
}

class NetworkRef : Attribute
{
    
}

class NetworkStatic : Attribute
{
    
}

class NetworkDynamic : Attribute
{
    
}

public class BadassNetworkBehaviour : NetworkBehaviour
{
    List<RW> syncList;
    
    struct RW
    {
        public Action<NetworkReader> reader;
        public Action<NetworkWriter> writer;
    }

    object ReadSmth(Type type, NetworkReader nr)
    {
        return null;
    }

    protected void InitTransport()
    {
        if (syncList == null)
        {
            syncList = new List<RW>();
            int i = 0;
            Action<Action<NetworkReader>, Action<NetworkWriter>, ICell> installAction = (nr, nw, cell) =>
            {
                syncList.Add(new RW {reader = nr, writer = nw});
                if (isServer && cell != null)
                {
                    uint iCopy = (uint)1 << i;
                    cell.OnChanged(() =>
                    {
                        SetDirtyBit(syncVarDirtyBits | iCopy);
                    }, Priority.Normal);
                }
                i++;
            };
            foreach (var fieldVar in this.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                var field = fieldVar;
                if (field.HasAttribute<NetworkDynamic>())
                {
                    if (field.FieldType == typeof(IntCell))
                    {
                        var val = (IntCell)field.GetValue(this);
                        Action<NetworkReader> rAll = nr =>
                        {
                            val.value = (int)nr.ReadPackedUInt32();
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            nw.WritePackedUInt32((uint)val.value);
                        };
                        installAction(rAll, wAll, val);
                    }
                    else if (field.FieldType == typeof(FloatCell))
                    {
                        var val = (FloatCell)field.GetValue(this);
                        Action<NetworkReader> rAll = nr =>
                        {
                            var v = nr.ReadSingle();
                            val.value = v;
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            nw.Write(val.value);
                        };
                        installAction(rAll, wAll,val);
                    }
                    else if (field.FieldType == typeof(BoolCell))
                    {
                        var val = (BoolCell)field.GetValue(this);
                        Action<NetworkReader> rAll = nr =>
                        {
                            var v = nr.ReadBoolean();
                            val.value = v;
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            nw.Write(val.value);
                        };
                        installAction(rAll, wAll,val);
                    }
                    else if (field.FieldType.IsGenericType && typeof(ICell).IsAssignableFrom(field.FieldType) && 
                             typeof (MonoBehaviour).IsAssignableFrom(field.FieldType.GetGenericArguments()[0]))
                    {
                        var cell = (ICell)field.GetValue(this);
                        Action<NetworkReader> rAll = nr =>
                        {
                            try
                            {
                                if (nr.ReadByte() == 1)
                                {
                                    var obj = nr.ReadGameObject();
                                    cell.valueObject = obj.GetComponent(cell.valueType);
                                }
                                else
                                {
                                    cell.valueObject = null;
                                }
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.LogError("cant deserialize field: " + e.Message);
                            }
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            if (cell.valueObject == null)
                            {
                                nw.Write((byte) 0);
                            }
                            else
                            {
                                nw.Write((byte) 1);
                                try
                                {
                                    nw.Write((cell.valueObject as MonoBehaviour).gameObject);
                                }
                                catch (Exception e)
                                {
                                    UnityEngine.Debug.LogError("cant serialize field: " + e.Message);
                                }
                            }
                        };
                        installAction(rAll, wAll, cell);
                    }
                }
                else if (field.HasAttribute<NetworkStatic>())
                {
                    if (field.FieldType == typeof (string))
                    {
                        Action<NetworkReader> rAll = nr =>
                        {
                            var str = nr.ReadString();
                            UnityEngine.Debug.Log("setting string: " + str);
                            field.SetValue(this, str);
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            nw.Write((string)field.GetValue(this));
                        };
                        installAction(rAll, wAll, null);
                    }
                    else if (field.FieldType == typeof (int))
                    {
                        Action<NetworkReader> rAll = nr =>
                        {
                            field.SetValue(this, (int)nr.ReadPackedUInt32());
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            nw.WritePackedUInt32((uint)(int)field.GetValue(this));
                        };
                        installAction(rAll, wAll, null);
                    }
                    else
                    {
                        Action<NetworkReader> rAll = nr =>
                        {
                            try
                            {
                                if (field.FieldType.IsEnum || field.FieldType == typeof (int))
                                {
                                    field.SetValue(this, nr.ReadInt32());
                                }
                                else
                                {
                                    BinaryFormatter formatter = new BinaryFormatter();
                                    MemoryStream stream = new MemoryStream(nr.ReadBytesAndSize());
                                    var obj = formatter.Deserialize(stream);
                                    field.SetValue(this, obj);
                                }
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.LogError("cant serialize field: " + e.Message);
                            }
                        };
                        Action<NetworkWriter> wAll = nw =>
                        {
                            try
                            {
                                if (field.FieldType.IsEnum || field.FieldType == typeof (int))
                                {
                                    nw.Write((int) field.GetValue(this));
                                }
                                else
                                {
                                    var obj = field.GetValue(this);
                                    BinaryFormatter formatter = new BinaryFormatter();
                                    MemoryStream stream = new MemoryStream();
                                    formatter.Serialize(stream, obj);
                                    nw.WriteBytesFull(stream.GetBuffer());
                                }
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.LogError("cant deserialize field: " + e.Message);
                            }
                        };
                        installAction(rAll, wAll, null);
                    }
                }
                else if (field.HasAttribute<NetworkRef>())
                {
                    Action<NetworkReader> rAll = nr =>
                    {
                        if (nr.ReadByte() == 1)
                        {
                            field.SetValue(this, nr.ReadNetworkIdentity().GetComponent(field.FieldType));
                        }
                        else
                        {
                            field.SetValue(this, null);
                        }
                    };
                    Action<NetworkWriter> wAll = nw =>
                    {
                        var val = field.GetValue(this);
                        if (val == null)
                        {
                            nw.Write((byte) 0);
                        }
                        else
                        {
                            nw.Write((byte) 1);
                            var obj = val as MonoBehaviour;
                            var identity = obj.GetComponent<NetworkIdentity>();
                            if (identity == null)
                            {
                                UnityEngine.Debug.LogError("obj has no network identity");
                                return;
                            }
                            nw.Write(identity);
                        }
                    };
                    installAction(rAll, wAll, null);
                }
                else if (typeof (ICell).IsAssignableFrom(field.FieldType) && field.HasAttribute<SyncronizedCell>())
                {
                    var cell = (ICell)field.GetValue(this);
                    Action<NetworkReader> rAll = nr =>
                    {
                        try
                        {
                            if (nr.ReadByte() == 1)
                            {
                                BinaryFormatter formatter = new BinaryFormatter();
                                MemoryStream stream = new MemoryStream(nr.ReadBytesAndSize());
                                var obj = formatter.Deserialize(stream);
                                cell.valueObject = obj;
                            }
                            else
                            {
                                cell.valueObject = null;
                            }
                        }
                        catch (Exception e)
                        {
                            UnityEngine.Debug.LogError("cant deserialize field: " + e.Message);
                        }
                    };
                    Action<NetworkWriter> wAll = nw =>
                    {
                        if (cell.valueObject == null)
                        {
                            nw.Write((byte) 0);
                        }
                        else
                        {
                            nw.Write((byte) 1);
                            try
                            {
                                BinaryFormatter formatter = new BinaryFormatter();
                                MemoryStream stream = new MemoryStream();
                                formatter.Serialize(stream, cell.valueObject);
                                nw.WriteBytesFull(stream.GetBuffer());
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.LogError("cant serialize field: " + e.Message);
                            }
                        }
                    };
                    installAction(rAll, wAll, cell);
                }
            }
        }
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        InitTransport();
        if (initialState)
        {
            for (int i = 0; i < syncList.Count; i++)
            {
                syncList[i].writer(writer);
            }
            return true;
        }
        if (syncVarDirtyBits == 0)
        {
            writer.WritePackedUInt32(0);
            return false;
        }
        writer.WritePackedUInt32(syncVarDirtyBits);
        for (int i = 0; i < syncList.Count; i++)
        {
            if (((1 << i)& syncVarDirtyBits) != 0)
            {
                syncList[i].writer(writer);
            }
        }
        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        InitTransport();
        if (initialState)
        {
            for (int i = 0; i < syncList.Count; i++)
            {
                syncList[i].reader(reader);
            }
            return;
        }
        int mask = (int) reader.ReadPackedUInt32();
        for (int i = 0; i < syncList.Count; i++)
        {
            if (((1 << i) & mask) != 0)
            {
                syncList[i].reader(reader);
            }
        }
    }
}
