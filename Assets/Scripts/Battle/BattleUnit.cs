﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using ZergRush;

public class BattleUnit : BattleBehaviour
{
    // static info
    [NetworkStatic] public string id;
    [NetworkStatic] public Side side;
    [NonSerialized] public UnitInfo info;

    //Syncronized fields
    [NetworkDynamic] public IntCell azariumBought = new IntCell();
    [NetworkDynamic] public FloatCell hp = new FloatCell();
    [NetworkDynamic] public FloatCell maxHp = new FloatCell();
    [NetworkDynamic] public FloatCell armor = new FloatCell();
    [NetworkDynamic] public FloatCell attack = new FloatCell();
    [NetworkDynamic] public FloatCell debuffResist = new FloatCell();
    public InorganicCollection<string> buffs = new InorganicCollection<string>();
    public ICell<float> hpRelative {get { return hp.Select(v => v/info.hp); } }

    // Internal fields
    [NonSerialized]
    public BaseUnitView clientView;

    public Fighter fighter;
    public Transform tr;

    [NonSerialized]
    public CharacterController cC;

    public void RefreshStats()
    {
        UpdateInfo();
    }

    void Awake()
    {
        tr = transform;
        cC = GetComponent<CharacterController>();
    }

    public void UpdateInfo()
    {
        if (string.IsNullOrEmpty(id))
        {
            Debug.Log("Unit id is not valid!");
            return;
        }
        if (info == null)
        {
            info = gameData.uInfo[id];
            foreach (var effect in info.effects)
            {
                buffs.Add(effect.GetType().Name);
            }
        }

        if (isServer && fighter == null)
        {
            fighter = new Fighter();
            fighter.Init(this);
            hp.SetInputCell(fighter.hp);
            armor.SetInputCell(fighter.armor);
            debuffResist.SetInputCell(fighter.debuffTimeMult);
            maxHp.SetInputCell(fighter.maxHp);
            attack.SetInputCell(fighter.damage);
        }
    }

    void Start()
    {
        UpdateInfo();
        gameObject.layer = side.LayerForSide();
    }

    public override void OnStartServer()
    {
        UpdateInfo();
        base.OnStartServer();
        InitTransport();
    }


    [ClientRpc]
    public void RpcDamageReceived(float damage)
    {
        if (damage == 0) SayIt("Immune", 1.5f, Color.gray);
        string prefix = (damage > 0 ? "-" : "+");
        if (damage < 0) damage = -damage;
        SayIt(prefix + ((int) damage), damage > 0 ? 1.3f : 1.6f, (side == Side.Red ? Color.red : Color.blue) + Color.gray);
    }

    [ClientRpc]
    public void RpcAbilityActivated(string abilityName)
    {
        SayIt("Activated: " + abilityName, 2, Color.white);
    }

    [ClientRpc]
    public void RpcEffectAdded(string name)
    {
        SayIt("Added: " + name, 2, Color.green + Color.gray);
        buffs.Add(name);
    }
    [ClientRpc]
    public void RpcEffectRemoved(string name)
    {
        SayIt("Removed: " + name, 2, Color.green + Color.gray);
        buffs.Remove(name);
    }
    [ClientRpc]
    public void RpcSetAnimSpeed(float speed)
    {
        clientView.animator.speed = speed;
    }
    [ClientRpc]
    public void RpcStunned(float duration)
    {
        SayIt("Stunned " + duration + "sec", 2, Color.yellow);
        clientView.ShowAnimation("idle");
    }
    [ClientRpc]
    public void RpcCannotStun()
    {
        SayIt("Immune to Stunned", 2, Color.gray);
    }

    private GameObject prefab;
    [Client]
    public void SayIt(string str, float size, Color color)
    {
        if (prefab == null) prefab = Resources.Load<GameObject>("RisingText");
        var text = Instantiate(prefab).GetComponent<TextMesh>();
        text.text = str;
        text.color = color;
        var pos = clientView.hpBar.transform.position;
        text.transform.position = pos;
        pos.y += 1;
        text.transform.DOMove(pos, 1f);
        text.transform.localScale *= size;
        Destroy(text.gameObject, 1.5f);
    } 

    [ClientRpc]
    public void RpcDoAttack(Vector3 activationDirection)
    {
        clientView.ShowAnimation("Attack", false);
        var moveTo = activationDirection.Flatterned();
        if (info.type != UnitType.Building)
            clientView.modelRoot.SetMoveDirection(moveTo);
        lastSpeed = 0;
    }

    [ClientRpc]
    public void RpcIdle(Vector3 currPos)
    {
        //clientView.ShowAnimation("Idle");
        lastSpeed = 0;
        lastTarget = Vector2.zero;
        if (isServer) return;
        tr.position = currPos;
    }

    [ClientRpc]
    public void RpcUpdateTargetMovement(Vector2 currPos, Vector2 target, float speed, float serverTime)
    {
        if (clientView.modelRoot == null) return;
        clientView.modelRoot.SetMoveDirection(target);
        clientView.ShowAnimation("Walk");
        if (isServer) return;
        //tr.position = Vector3.MoveTowards(currPos, target, (system.serverTime.value - serverTime) * speed);
        tr.position = currPos.ToVolumeVector(tr.position.y);
        lastSpeed = speed;
        lastTarget = target;
    }

    [ClientRpc]
    public void RpcOnMove(Vector3 target)
    {
        if (clientView.modelRoot == null) return;
        clientView.modelRoot.SetMoveDirection(target);
        clientView.ShowAnimation("Walk");
    }


    private Vector2 lastTarget;
    private float lastSpeed;


    [ClientRpc]
    public void RpcNewProjectile(string model, float speed, Vector3 startPos)
    {
        var prefab = Resources.Load("Projectiles/" + model);
        var projView = (Instantiate(prefab) as GameObject).GetComponent<global::Projectile>();
        var offset = new Vector3(0, 1, 0);
        projView.target = tr;
        projView.speed = speed;
        projView.offset = offset;
        projView.transform.position = startPos + offset;
    }

    [Client]
    void Update()
    {
        if (lastSpeed != 0)
        {
            Vector2 curr = tr.position.ToFlatVector();
            Vector2 to = lastTarget;

            if (cC != null)
            {
                Vector2 dir = (to - curr);
                dir.Normalize();
                dir *= lastSpeed;
                Vector3 movement = new Vector3(dir.x, 0, dir.y);
                cC.SimpleMove(movement);
            }
            else
            {
                var pos = Vector2.MoveTowards(curr, to, lastSpeed * Time.deltaTime).ToVolumeVector();
                pos.y = tr.position.y;
                tr.position = pos;
            }
        }
    }
}
