﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum SpellTargetType
{
    AllyUnit,
    EnemyUnit,
    AnyUnit
}

[Serializable]
public abstract class Spell
{
    public string name;
    public string desctiption;
    public SpellTargetType targetType;
    public int manaCost;
    public abstract void Activate(Fighter target);
}

[Serializable]
class CastHeal : Spell
{
    [ParseAttrib(1)]
    public float healAmmount;

    public override void Activate(Fighter target)
    {
        target.ReceiveDamage(new DamageInfo {damage = -healAmmount});
    }
}

[Serializable]
class CastSlowness : Spell
{
    [ParseAttrib(1)] public float duration;
    [ParseAttrib(2)] public float percent;

    public override void Activate(Fighter target)
    {
        var eff = new Slowness {percent = percent};
        eff.timeLeft = duration;
        target.GiveEffect(eff);
    }
}
