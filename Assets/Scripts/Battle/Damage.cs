﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum DamageType
{
    none,
    light,
    heavy,
    magic
}

public enum ArmorType
{
    none,
    light,
    heavy,
    magic
}

public class DamageInfo
{
    public Behaviour sourceAbility;
    public Fighter source;
    public Fighter target;
    public DamageType type = DamageType.none;

    public bool isSideEffect;
    public bool isMelee;

    public float damage;

    public bool applyAoe;
    public float aoeRange;
    public int aoeMaxEnemyCount;

    public bool armorIgnore;
    public float critValue;
    public float critChance;

    public bool isCritical;
    public bool cantBeHealed;

    public DamageInfo Copy()
    {
        return new DamageInfo {
            source = source,
            sourceAbility = sourceAbility,
            target = target,
            type = type,
            isSideEffect = isSideEffect,
            isMelee = isMelee,
            damage = damage,
            applyAoe = applyAoe,
            aoeRange = aoeRange,
            armorIgnore = armorIgnore,
            critChance = critChance,
            critValue = critValue,
            isCritical = isCritical
        };
    }
}

[Serializable]
public abstract class DamageModifier : Organism<Behaviour, Fighter>, IAzariumLockable
{
    public float chance;
    protected bool roll { get { return UnityEngine.Random.value <= chance; } }
    public int azariumNeed { get; set; }
}


[Serializable]
abstract class DamageDealingMod : DamageModifier
{
    public override void Spread()
    {
        base.Spread();
        carrier.damagePreparing.Listen(damage =>
        {
            if (roll) Affect(damage);
        });
    }

    public abstract void Affect(DamageInfo damage);
}

[Serializable]
abstract class ActivationMod : DamageModifier
{
    public override void Spread()
    {
        base.Spread();
        carrier.activation.Listen(() =>
        {
            if (roll) Affect();
        });
    }

    public abstract void Affect();
}

[Serializable]
public abstract class TargetDamagedMod : DamageModifier
{
    public override void Spread()
    {
        base.Spread();
        carrier.damageDealing.Listen(damage =>
        {
            if (roll) Affect(damage.target);
        });
    }

    public abstract void Affect(Fighter target);
}

[Serializable]
class MatchBetterWith : DamageDealingMod
{
    [ParseAttrib(1)] public DamageType additionalType;
    public override void Affect(DamageInfo damage)
    {
        var at = damage.target.info.armorType;
        if (Battle.ClashDamageTypes(damage.type, at) < Battle.ClashDamageTypes(additionalType, at))
        {
            damage.type = additionalType;
        }
    }
}

[Serializable]
class MatchBestAll : DamageDealingMod
{
    public override void Affect(DamageInfo damage)
    {
        var at = damage.target.info.armorType;
        damage.type = Battle.BestDamageTypeFor(at);
    }
}

[Serializable]
class ToAoeWithCrit : DamageDealingMod
{
    [ParseAttrib(1), DistanceAttrib] public float aoeRange;
    [ParseAttrib(2)] public float damageMult;
    public override void Affect(DamageInfo damage)
    {
        damage.applyAoe = true;
        damage.aoeRange = aoeRange;
        damage.damage *= damageMult;
        damage.isCritical = true;
    }
}

[Serializable]
class ToAoe : DamageDealingMod
{
    [ParseAttrib(1), DistanceAttrib] public float aoeRange;
    [ParseAttrib(2)] public int maxEnemies;
    public override void Affect(DamageInfo damage)
    {
        damage.applyAoe = true;
        damage.aoeRange = aoeRange;
        damage.aoeMaxEnemyCount = maxEnemies;
    }
}

public enum DamageCondition
{
    Buildings,
    Ranged,
    Melee
}

[Serializable]
class ConditionalDamageBuff : DamageDealingMod
{
    [ParseAttrib(1)] public DamageCondition condition;
    [ParseAttrib(2)] public float mult;
    public override void Affect(DamageInfo damage)
    {
        var buff =
            (condition == DamageCondition.Buildings && damage.target.info.type == UnitType.Building)
            || (condition == DamageCondition.Ranged && damage.target.info.type == UnitType.Ranged)
            || (condition == DamageCondition.Melee && damage.target.info.type == UnitType.Melee);
        damage.damage *= buff ? mult : 1;
    }
}

[Serializable]
class Amplify : DamageDealingMod
{
    [ParseAttrib(1)] public float mult;
    public override void Affect(DamageInfo damage)
    {
        damage.damage *= mult;
    }
}

[Serializable]
class ArmorDiffDamage : DamageDealingMod
{
    [ParseAttrib(1)] public float mult;
    public override void Affect(DamageInfo damage)
    {
        var diff = damage.source.armor.value - damage.target.armor.value;
        if (diff > 0) damage.damage += diff;
    }
}

[Serializable]
class CantBeHealed : DamageDealingMod
{
    [ParseAttrib(1)] public float mult;
    public override void Affect(DamageInfo damage)
    {
        damage.cantBeHealed = true;
    }
}

[Serializable]
class ArmorIgnore : DamageDealingMod
{
    public override void Affect(DamageInfo damage)
    {
        damage.armorIgnore = true;
    }
}

[Serializable]
class AddCdToTargetDamaged : TargetDamagedMod
{
    [ParseAttrib(1)] public float cdAdd;
    public override void Affect(Fighter target)
    {
        target.AddCooldownToBehaviours(cdAdd);
    }
}


[Serializable]
class ApplyStunOnDamage : TargetDamagedMod
{
    [ParseAttrib(1)] public float stun;
    public override void Affect(Fighter target)
    {
        target.Stun(stun);
    }
}

[Serializable]
class AddEffectTargetDamaged : TargetDamagedMod
{
    [ParseAttrib(1)] public float duration;
    [ParseAttrib(2), NotAChild] public PassiveAbility effect;
    public override void Affect(Fighter target)
    {
        var eff = effect.Clone();
        eff.timeLeft += duration;
        target.GiveEffect(eff);
    }
}

[Serializable]
class RefreshCdWhenTargetDies : TargetDamagedMod
{
    public override void Affect(Fighter target)
    {
        collect = target.dead.Listen(() =>
        {
            root.AddCooldownToBehaviours(-1000);
        });
    }
}

[Serializable]
class AddEffectSelf : ActivationMod
{
    [ParseAttrib(1)] public float duration;
    [ParseAttrib(2), NotAChild] public PassiveAbility effect;
    public override void Affect()
    {
        var eff = effect.Clone();
        eff.timeLeft += duration;
        root.GiveEffect(eff);
    }
}

[Serializable]
class AddEffectTarget : TargetDamagedMod
{
    [ParseAttrib(1)] public float duration;
    [ParseAttrib(2), NotAChild] public PassiveAbility effect;

    public override void Affect(Fighter target)
    {
        var eff = effect.Clone();
        eff.timeLeft += duration;
        target.GiveEffect(eff);
    }
}
[Serializable]
class AddEffectRandomTargets : TargetDamagedMod
{
    [ParseAttrib(1)] public float range;
    [ParseAttrib(2)] public int targets;
    [ParseAttrib(3)] public float duration;
    [ParseAttrib(4), NotAChild] public PassiveAbility effect;

    public override void Affect(Fighter target)
    {
        foreach (var t in target.AlliesInRange(range, false).RandomElements(targets))
        {
            var eff = effect.Clone();
            eff.timeLeft += duration;
            t.GiveEffect(eff);
        }
    }
}


[Serializable]
public class StealPositiveEffect : TargetDamagedMod
{
    public override void Affect(Fighter target)
    {
        var eff = target.passives.Where(e => e.isPositive && e.removable).RandomElement();
        target.passivesWithDuration.Remove(eff);
        if (eff != null)
        {
            target.passives.Remove(eff);
            root.GiveEffect(eff);
        }
    }
}

