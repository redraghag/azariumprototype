﻿using UnityEngine;
using System.Collections.Generic;
using Pathfinding;

public abstract class PathAIBase : MovementBase {

    private List<GraphNode> locked = new List<GraphNode>();
    public float size = 0.4f;

    protected void Awake()
    {
        base.Awake();
    }

    public override void ReparseNodes()
    {
        AstarPath.active.UpdateGraphs(new Bounds(tr.position, new Vector3(size, size, 10)));
    }

    public override System.Action ReparseAction()
    {
        var pos = tr.position;
        var s = size;
        return () => AstarPath.active.UpdateGraphs(new Bounds(pos, new Vector3(s, s, 10)));
    }


    void Lock(Vector3 pos)
    {
        var lockedNode = (GridNode) AstarPath.active.GetNearest(pos).node;
        lockedNode.Walkable = false;
        locked.Add(lockedNode);
    }

    protected override void Lock()
    {
        Unlock();
        var delta = 0.3f;
        var pos = tr.position;
        Lock(pos + new Vector3(-delta, 0, -delta));
        Lock(pos + new Vector3(delta, 0, -delta));
        Lock(pos + new Vector3(-delta, 0, delta));
        Lock(pos + new Vector3(delta, 0, delta));
        //locked.GetConnections(node => connections.Add(node));
        //connections.ForEach(locked.RemoveConnection);
    }

    protected override void Unlock()
    {
        if (locked.Count == 0) return;
        foreach (var node in locked)
        {
            node.Walkable = true;
        }
        locked.Clear();
    }
}
