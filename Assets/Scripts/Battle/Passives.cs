﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public abstract class PassiveAbility : FighterSubOrgan, IAzariumLockable
{
    // can it be removed by purge and other stuff
    public bool removable = false;
    public bool silent;
    // Any time >0 means effect will disappear after that time.
    public float timeLeft { get; set; }
    public int azariumNeed { get; set; }
    public abstract bool isPositive { get; }
    [NonSerialized, NotAChild]
    public Fighter owner;
}

[Serializable]
public class LightningShield : PassiveAbility
{
    [ParseAttrib(1)] public float damage;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.directDamageReceived.Listen(d =>
        {
            if (d.isMelee == false) return;
            var feedback = new DamageInfo
            {
                damage = damage,
                isSideEffect = true,
                type = DamageType.magic,
                armorIgnore = true
            };
            feedback.DamageInfoCommit(d.target);
        });
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class Invulnerability : PassiveAbility
{
    public override void Spread()
    {
        base.Spread();
        collect = carrier.damageReceivePreparing.Listen(d =>
        {
            if (d.damage > 0) d.damage = 0;
        }, Priority.Post);
        carrier.debuffTimeMult.Affect(this, 0);
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class Spikes : PassiveAbility
{
    [ParseAttrib(1)] public float damageBack;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.directDamageReceived.Listen(damage =>
        {
            if (damage.isMelee == false) return;
            if (damage.source == null) return;
            var back = Mathf.Min(damage.damage, damageBack);
            carrier.DoGeneralDamageTo(damage.source, new DamageInfo {damage = back, armorIgnore = true, isSideEffect = true, type = DamageType.light});
        });
    }
    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class DamageReduction : PassiveAbility
{
    [ParseAttrib(1)] public float damageRediction;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.anyDamageFinalized.Listen(d =>
        {
            d.damage *= (1 - damageRediction);
        });
    }
    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class DebuffEndure : PassiveAbility
{
    [ParseAttrib(1)] public float debuffTimeMult;
    public override void Spread()
    {
        base.Spread();
        carrier.debuffTimeMult.Affect(this, debuffTimeMult);
    }
    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class AddArmor : PassiveAbility
{
    [ParseAttrib(1)] public float armor;
    public override void Spread()
    {
        base.Spread();
        carrier.armor.Affect(this, armor);
    }
    public override bool isPositive
    {
        get { return armor > 0; }
    }
}

[Serializable]
public class StunImmune : PassiveAbility
{
    public override void Spread()
    {
        base.Spread();
        carrier.stunnImmune.Affect(this, true);
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class AddMaxHp : PassiveAbility
{
    [ParseAttrib(1)] public float hp;
    public override void Spread()
    {
        base.Spread();
        carrier.maxHp.Affect(this, hp);
    }
    public override bool isPositive
    {
        get { return hp > 0; }
    }
}

[Serializable]
public class AddAttack : PassiveAbility
{
    [ParseAttrib(1)] public float attack;
    public override void Spread()
    {
        base.Spread();
        carrier.damage.Affect(this, attack);
    }
    public override bool isPositive
    {
        get { return attack > 0; }
    }
}

[Serializable]
public class ProjectileReflection : PassiveAbility
{
    [ParseAttrib(1)] public float chance;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.onProjectileHit.Listen(p =>
        {
            if (p.positive == false && UnityEngine.Random.value <= chance) p.reflected = true;
        });
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class OnDeathDamageAndHealAoe : PassiveAbility
{
    [ParseAttrib(1)] public float damage;
    [ParseAttrib(2)] public float heal;
    [ParseAttrib(3), DistanceAttrib] public float range;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.dead.Listen(() =>
        {
            var allies = carrier.AlliesInRange(range, false);
            foreach (var fighter in allies)
            {
                carrier.DoGeneralDamageTo(fighter, new DamageInfo {damage = -heal, isSideEffect = true});
            }
            var enemies = carrier.EnemiesInRange(range, FlyingMask.All);
            foreach (var fighter in enemies )
            {
                carrier.DoGeneralDamageTo(fighter, new DamageInfo {damage = damage, isSideEffect = true});
            }
        });
    }

    public override bool isPositive
    {
        get { throw new NotImplementedException(); }
    }
}

[Serializable]
public class DotPercent : PassiveAbility
{
    [ParseAttrib(1)] public float percent;
    public override bool isPositive
    {
        get { return false; }
    }

    public float buffer;
    public float step = 0.5f;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.tick.Listen(val =>
        {
            buffer += val;
            while (buffer > step)
            {
                Battle.ReceiveDamage(carrier, new DamageInfo {damage = carrier.maxHp.value * percent * step, armorIgnore = true, isSideEffect = true});
                buffer -= step;
            }
        });
    }
}

[Serializable]
public class LightningStrike : PassiveAbility
{
    [ParseAttrib(1)] public float damage;
    [ParseAttrib(2)] public float cooldown;
    [ParseAttrib(3), DistanceAttrib] public float range;
    [ParseAttrib(4)] public float stun;
    public override bool isPositive
    {
        get { return false; }
    }

    public float buffer;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.tick.Listen(val =>
        {
            buffer += val;
            while (buffer > cooldown)
            {
                var target = carrier.NearesEnemy(range);
                if (target == null)
                {
                    buffer -= 0.1f;
                    continue;
                }
                target.ReceiveDamage(new DamageInfo {damage = damage, source = carrier, armorIgnore = true, isSideEffect = true, type = DamageType.magic});
                if (stun > 0)
                {
                    target.Stun(stun);
                }
                buffer -= cooldown;
            }
        });
    }
}

[Serializable]
public class Regeneration : PassiveAbility
{
    [ParseAttrib(1)] public float rate;
    public float buffer;
    public float step = 0.4f;
    public override void Spread()
    {
        base.Spread();
        collect = carrier.tick.Listen(val =>
        {
            buffer += val;
            while (buffer > step)
            {
                if (carrier.healed == false) carrier.ReceiveDamage(new DamageInfo {damage = -rate * step, isSideEffect = true});
                buffer -= step;
            }
        });
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
class AddCdToTargetDamagedByUnit : PassiveAbility
{
    [ParseAttrib(1)] public float cdAdd;
    public override void Spread()
    {
        base.Spread();
        carrier.damageDealPreparing.Listen(d => { d.target.AddCooldownToBehaviours(cdAdd); });
    }

    public override bool isPositive
    {
        get { return true; }
    }
}

[Serializable]
public class Slowness : PassiveAbility
{
    [ParseAttrib(1)] public float percent;

    public override bool isPositive
    {
        get { return false; }
    }

    public override void Spread()
    {
        base.Spread();
        carrier.speed.Affect(this, 1 - percent);
        carrier.attackSpeedDebuff.Affect(this, 1 - percent);
    }
}