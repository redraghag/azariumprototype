﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

class FlyingMovement : MovementBase
{
    public override void ReparseNodes()
    {
    }

    public override Action ReparseAction()
    {
        return () => { };
    }

    protected override void UpdateMovement(float dt)
    {
        if (target != null)
        {
            if (target.valid == false) target = null;
            else targetPos = target.position;
        }
        MoveTo(targetPos, dt);
    }

    protected override void Lock()
    {
    }

    protected override void Unlock()
    {
    }

    private Vector2 targetPos;
    private Fighter target;

    protected override void EngageInner(Fighter engageTarget)
    {
        target = engageTarget;
        targetPos = target.position;
    }

    protected override void MoveDirectionInner(Vector2 direction)
    {
        target = null;
        targetPos = tr.position.ToFlatVector() + direction.normalized * 10000;
    }
}
