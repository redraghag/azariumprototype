﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;

public enum Side
{
    Red,
    Blue
}

public enum TargetType
{
    Enemies,
    Friends,
    All
}

class SolderAI : BattleUnit {

	// Use this for initialization

    public override void OnStartClient()
    {
        if (info == null)
        {
            Debug.Log("WTF");
            RefreshStats();
        }
        var viewObj = Instantiate(Resources.Load<GameObject>("UnitView"), transform, false) as GameObject;
        clientView = viewObj.GetComponent<BaseUnitView>();
    }
}
