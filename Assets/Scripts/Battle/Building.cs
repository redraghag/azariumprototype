﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

public class Building : BadassNetworkBehaviour
{
    [SyncronizedCell] public Cell<BuildingInfo> info = new Cell<BuildingInfo>();
    [NetworkStatic] public int slotIndex;
    [NetworkRef] public PlayerState owner;

    public BuildSlot slot;

    [NetworkDynamic]
    public IntCell azariumUp = new IntCell();
    [NetworkDynamic]
    public FloatCell currentSpawnBeginning = new FloatCell();
    Transform model;

    void Start()
    {
        Debug.Log("Building start");
    }

    private List<BuildingsObj> currEffects = new List<BuildingsObj>();

    public void UpdateInfo(BuildingInfo i)
    {
        info.value = i;
        foreach (var effect in currEffects)
        {
            effect.expired = true;
        }
        currEffects.Clear();
        foreach (var obj in info.value.effects)
        {
            var eff = obj.Clone();
            eff.side = owner.side;
            currEffects.Add(eff);
            owner.system.RegisterGlobalEffect(eff);
        }
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        currentSpawnBeginning.value = -30;
        slot = owner.slot.buildingSlots[slotIndex];
    }

    public override void OnStartClient()
    {
        slot = owner.slot.buildingSlots[slotIndex];
        // Load building model on client
        info.Bind(_ => UpdateModel());
    }

    public void UpdateModel()
    {
        if (model != null)
        {
            Destroy(model.gameObject);
        }
        var obj = Instantiate(Resources.Load<GameObject>("Buildings/" + info.value.model), slot.transform, false) as GameObject;
        obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        obj.transform.localPosition = Vector3.zero;
        model = obj.transform;
    }

    // This function runs only on server
    public void SpawnUnit()
    {
        if (string.IsNullOrEmpty(info.value.unitToSpawn)) return;
        var uinfo = owner.gameData.uInfo.AtOrDefault(info.value.unitToSpawn);
        if (uinfo == null)
        {
            Debug.LogError("unit not found with id: " + info.value.unitToSpawn);
            return;
        }
        bool flying = uinfo.type == UnitType.Flying;
        var uObj = BattleSystem.BattlePrefabInst<SolderAI>(flying == false ? "Unit" : "FlyingUnit");
        uObj.side = owner.side;
        //uObj.GetComponent<MovementBase>().enemyCastle = owner.system.WhatIsMyTarget(owner.side).tr;
        uObj.transform.position = slot.spawnPoint.transform.position;
        if (flying)
        {
            uObj.transform.SetLocalPositionY(5);
        }
        var unit = uObj.GetComponent<SolderAI>();
        unit.azariumBought.value = azariumUp.value;
        unit.id = uinfo.name;
        unit.info = uinfo;
        BattleSystem.NetworkInstUnit(uObj);
    }
}
