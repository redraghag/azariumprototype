﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Map : MonoBehaviour
{
    public static Map current;
    public CenterArea center;
    public SideSlot red;
    public SideSlot blue;

    public Transform redCylinder;
    public Transform blueCylinder;
    public Transform neutralCylinder;

    public int needPlayers { get { return blue.playerSlots.Count*2; } }

    void Awake()
    {
        current = this;
    }
}

