﻿//C# Example
using UnityEngine;
using System.Linq;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using UniRx;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using Random = UnityEngine.Random;

public class GameDataLoader
{
    static public GameData getGameData()
    {
        GameData game = null;
        var data = Resources.Load("gd") as TextAsset;
        if (data != null)
        {
            var stream = new MemoryStream(data.bytes);
            IFormatter formatter = new BinaryFormatter();
            game = formatter.Deserialize(stream) as GameData;
            return game;
        }
        else
        {
            game = new GameData();
            saveGameData(game);
            return game;
        }
    }

    public static void saveGameData(GameData d)
    {
        try
        {
            using (var f = File.Open("Assets/Resources/gd.bytes", FileMode.Create))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(f, d);
                f.Close();
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Serialization error: " + e.Message);
            throw;
        }
    }
}


public static class DataImporter
{
    public static Dictionary<string, string> pagesToLoad = new Dictionary<string, string>
    {
        {"settings", "762219909"},
        {"units", "0"},
        {"buildings", "79960289"},
        {"spells", "2041555191"},
    }; 

    static string baseDataUrl = "https://docs.google.com/spreadsheets/d/1BYl6Tu5qIx2V357gbJC9oHAeuDzIYS5lncTl2ENaQSs/pub?gid=";
    static string urlEnding = "&single=true&output=csv";

    public static string UrlForPageId(string id)
    {
        return baseDataUrl + id + urlEnding;
    }

    static GameData d;
    public static GameSettings settings;

    static T ReadOptionalSmth<T>(CsvCell location) 
        where T : class, IAzariumLockable
    {
        int azIndex = 0;
        var name = location.GetValue();
        if (name.StartsWith("AZ1"))
        {
            azIndex = 1;
            name = name.Substring(4);
        }
        else if (name.StartsWith("AZ2"))
        {
            azIndex = 2;
            name = name.Substring(4);
        }
        var abil = CsvExt.ClassFromName(name);
        if (abil == null) return null;
        CsvExt.ParseAttribs(abil, location);
        var optAbil = abil as IAzariumLockable;
        if (optAbil == null)
        {
            Debug.Log("ability " + name + " is not azarium up optional");
            return null;
        }
        optAbil.azariumNeed = azIndex;
        var t = abil as T;
        if (t == null)
        {
            Debug.Log("ability " + name + " is not of type " + typeof(T).Name);
            return null;
        }
        return t;
    }

    static float ReadRange(string range)
    {
        if (string.IsNullOrEmpty(range)) return 0;
        if (range == "melee") return settings.MeleeAttackRange * settings.DistanceMultiplier;
        return range.CastTo<float>() * settings.DistanceMultiplier;
    }

    static GameSettings ReadSettings(CsvReader reader)
    {
        settings = new GameSettings();
        foreach (var field in settings.GetType().GetFields())
        {
            foreach (var row in reader)
            {
                if (row.AtOrDefault("Type", "") == field.Name)
                {
                    field.SetValue(settings, row.CastOrDefault("Value", 0f));
                    break;
                }
            }
        }
        return settings;
    }

    static UnitInfo ReadUnit(CsvCell location)
    {
        var row = location.GetRow();
        UnitInfo unit = new UnitInfo
        {
            name = row["Name"],
            hp = row["Health"].CastTo<int>(),
            armor = row.CastOrDefault("Armor", 0),
            movementSpeed = settings.GlobMovementSpeed > 0 ? settings.GlobMovementSpeed : row.CastOrDefault("Speed", 10) * settings.DistanceMultiplier,
            armorType = row.EnumOrDefault("Armor type", ArmorType.none),
            damageType = row.EnumOrDefault("DamageType", DamageType.none),
            model = row.AtOrDefault("UnitModel", "Archer"),
            size = row.CastOrDefault("Size", 0.31f),
            aggroRange = settings.AggroRange * settings.DistanceMultiplier,
            totalCost = ParseCost(row["TotalCost"]),
            type = row.EnumOrDefault("UnitType", UnitType.Melee),
            baseDamage = row.CastOrDefault("BaseDamage", 1f)
        };

        unit.actions.Add(new MoveToEnemyBase());
        //if (unit.type == UnitType.Melee || unit.type == UnitType.Flying)
        {
            var mte = new MoveToEngage();
            mte.range = settings.MeleeMoveToEngageRange * settings.DistanceMultiplier;
            unit.actions.Add(mte);
        }
        if (unit.type == UnitType.Flying)
        {
            unit.movementSpeed *= 1.5f;
        }

        DamageModifier currDamageMod = null;
        Behaviour currAbility = null;
        do
        {
            row = location.GetRow();
            if (row.has("Abilities"))
            {
                if (currAbility != null)
                {
                    unit.actions.Add(currAbility);
                }
                currAbility = ReadOptionalSmth<Behaviour>(row.GetCell("Abilities"));
                if (currAbility != null)
                {
                    currAbility.range = ReadRange(row.AtOrDefault("Range", ""));
                    currAbility.cooldown = row.CastOrDefault("CD", 0);
                    currAbility.damageType = row.EnumOrDefault("DamageType", DamageType.none);

                    if (currAbility is SimpleStayStillAction)
                    {
                        (currAbility as SimpleStayStillAction).timeToCast = 1/row.CastOrDefault("Freq", 1f);
                    }
                }
            }
            if (currAbility != null && row.has("DamageMod"))
            {
                var mod = ReadOptionalSmth<DamageModifier>(row.GetCell("DamageMod"));
                if (mod != null)
                {
                    mod.chance = row.CastOrDefault("DMChance", 1f);
                    currAbility.damageMod.Add(mod);
                }
            }
            if (row.has("Passives"))
            {
                var passive = ReadOptionalSmth<PassiveAbility>(row.GetCell("Passives"));
                if (passive != null)
                    unit.effects.Add(passive);
            }
            if (row.has("Aura"))
            {
                var aura = ReadOptionalSmth<Aura>(row.GetCell("Aura"));
                if (aura != null)
                {
                    aura.range = row.CastOrDefault("AuraRange", 10f) * settings.DistanceMultiplier;
                    unit.auras.Add(aura);
                }
            }
            location = location.GetOffset(1, 0);
        } while (location.valid && location.hasValue() == false);

        if (currAbility != null)
        {
            unit.actions.Add(currAbility);
        }
        return unit;
    }

    static Cost ParseCost(string str)
    {
        var costPart = str.Split(',');
        var cost = new Cost();
        cost.gold = costPart[0].CastTo<float>();
        if (costPart.Length == 1) return cost;
        cost.azarium = costPart[1].Trim(new char[] {' ', 'A', 'Z'}).CastTo<float>();
        return cost;
    }

    static Dictionary<string, BuildingInfo> ReadBuildings(CsvReader csv, CsvReader specials)
    {
        var rows = csv.rows;
        Dictionary<string, BuildingInfo> dict = new Dictionary<string, BuildingInfo>();

        BuildingInfo current = null;
        foreach (var row in rows)
        {
            if (row.has("Name"))
            {
                var unit = ReadUnit(row.GetCell("Name"));
                d.uInfo[unit.name] = unit;
                current = new BuildingInfo
                {
                    name = row["Name"],
                    cost = ParseCost(row["Cost"]),
                    unitToSpawn = unit.name,
                    icon = row.AtOrDefault("Icon", "rookie"),
                    model = row.AtOrDefault("BuildingModel", "Building1")
                };
                dict[current.name] = current;

                Action<string> addAbilAzShort = s =>
                {
                    if (row.has(s))
                        current.azariupUpgradeDecriptionShort.Add(row[s]);
                };
                for (int i = 1; i <= 2; i++)
                {
                    addAbilAzShort("AzAbilityName" + i);
                }

                Action<string> addAbilShort = s =>
                {
                    if (row.has(s))
                        current.abilityDescriptionShort.Add(row[s]);
                };
                for (int i = 1; i <= 3; i++)
                {
                    addAbilShort("Ab" + i + "Short");
                }

                if (row.has("Ability 1"))
                    current.abilityDescription.Add(row["Ability 1"]);
                if (row.has("Ability 2"))
                    current.abilityDescription.Add(row["Ability 2"]);
                if (row.has("Ability 3"))
                    current.abilityDescription.Add(row["Ability 3"]);
                if (row.has("Aza. ability 1"))
                    current.azariupUpgradeDecription.Add(row["Aza. ability 1"]);
                if (row.has("Aza. ability 2"))
                    current.azariupUpgradeDecription.Add(row["Aza. ability 2"]);

                if (row.has("Previous"))
                {
                    var key = row["Previous"];
                    if (dict.ContainsKey(key))
                    {
                        dict[key].next.Add(current.name);
                    }
                    else
                    {
                        Debug.Log("previous building with ke: " + key + " not found");
                    }
                }
                else
                {
                    current.initial = true;
                }
            }
        }

        rows = specials.rows;

        foreach (var row in rows)
        {
            if (row.has("Name"))
            {
                current = new BuildingInfo
                {
                    name = row["Name"],
                    cost = ParseCost(row["Cost"]),
                    icon = row.AtOrDefault("Icon", "Icon4"),
                    model = row.AtOrDefault("BuildingModel", "Building1")
                };
                dict[current.name] = current;
                current.abilityDescription.Add(row.AtOrDefault("Description"));
                var eff = CsvExt.ParseClass(row.GetCell("Effect")) as BuildingsObj;
                eff.cooldown = row.CastOrDefault("EffectCD", 10f);
                current.effects.Add(eff);
                if (row.has("Previous"))
                {
                    var key = row["Previous"];
                    if (dict.ContainsKey(key))
                    {
                        dict[key].next.Add(current.name);
                    }
                    else
                    {
                        Debug.Log("previous building with key: " + key + " not found");
                    }
                }
                else
                {
                    current.initial = true;
                }
            }
        }
        return dict;
    }

    public static List<Spell> ReadSpells(CsvReader reader)
    {
        var rows = reader.rows;
        var spells = new List<Spell>();
        foreach (var row in rows)
        {
            if (row.has("Name"))
            {
                var spell = CsvExt.ParseClass(row.GetCell("Effect")) as Spell;
                spell.name = row["Name"];
                spell.desctiption = row.AtOrDefault("Description");
                spell.targetType = row.EnumOrDefault("TargetType", SpellTargetType.EnemyUnit);
                spell.manaCost = row.CastOrDefault("Cost", 20);
                spells.Add(spell);
            }
        }
        return spells;
    } 

    public static GameData ParseData(Func<string, CsvReader> pageLoader)
    {
        d = new GameData();
        d.uInfo = new Dictionary<string, UnitInfo>();
        var settingsPage = pageLoader("settings");
        d.settings = ReadSettings(settingsPage);

        var castleInfo = new UnitInfo();
        castleInfo.name = "Castle";
        castleInfo.hp = (int)settings.CastleHp;
        castleInfo.armor = (int)settings.CastleArmor;
        castleInfo.model = "BuildingMain";
        castleInfo.type = UnitType.Building;
        d.uInfo["Castle"] = castleInfo;
        var towerInfo = new UnitInfo();
        towerInfo.name = "Tower";
        towerInfo.hp = (int)settings.TowerHp;
        towerInfo.model = "BuildingMain";
        towerInfo.baseDamage = settings.TowerDamage;
        towerInfo.type = UnitType.Building;
        towerInfo.effects.Add(new StunImmune());
        towerInfo.actions.Add(new RangeSplashAttack {range = settings.TowerRange * settings.DistanceMultiplier,
            timeToCast = settings.TowerAttackCooldown, damageType = DamageType.none,
            damageMod = new BioCollection<DamageModifier>{new ArmorIgnore()}});
        d.uInfo["Tower"] = towerInfo;
        d.availableRace = new List<RaceInfo>
        {
           new RaceInfo {bInfo = ReadBuildings(pageLoader("units"), pageLoader("buildings")), name = "default"}
        };
        d.spells = ReadSpells(pageLoader("spells"));
        return d;
    }

}

