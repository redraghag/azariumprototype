﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.IO;
using System.Linq;
using UnityEngine;

public class Row
{
    public OrderedDictionary data;
    public int Length { get { return data.Count; } }
    public int index = -1;
    public List<Row> table;

    public string this[string i]
    {
        get { return data[i] as string; }
    }

    public string this[int i]
    {
        get { return data[i] as string; }
    }

    public Row(OrderedDictionary fill)
    {
        data = fill;
    }

    public bool has(string key)
    {
        return data.Contains(key) && (data[key] as string) != "";
    }

    public T CastOrDefault<T>(string key, T def = default(T)) where T : new()
    {
        if (has(key))
            return this[key].CastTo<T>();
        return def;
    }

    public string AtOrDefault(string key, string def = "")
    {
        return has(key) ? this[key] : def;
    }

    public T EnumOrDefault<T>(string key, T def = default(T))
    {
        if (has(key))
            return (T)Enum.Parse(typeof(T), this[key]);
        return def;
    }

    public int Count { get { return data.Count; } }

    public string GetLast(string Key)
    {
        if (has(Key))
            return this[Key];
        else
        {
            var currentRow = this;
            while (currentRow.prev != null)
            {
                currentRow = currentRow.prev;

                if (currentRow.has(Key))
                    return currentRow[Key];
            }

            throw new FormatException(Key + " not found; at[" + index+"]");
        }
    }

    public CsvCell GetCell(string key)
    {
        return new CsvCell(index, data.GetIndex(key), table);
    }

    public Row next = null;
    public Row prev = null;
}

public class CsvCell
{
    private int x = 0, y = 0;
    List<Row> table;

    public CsvCell(int _x, int _y, List<Row> _table)
    {
        x = _x;
        y = _y;
        table = _table;
    }

    public CsvCell(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

    public Row GetRow()
    {
        return table[x];
    }

    public CsvCell GetOffset(int _x, int _y)
    {
        return new CsvCell(x + _x, y + _y, table);
    }

    public bool hasValue()
    {
        return x < table.Count && y < table[x].Count && table[x][y] != "";
    }
    
    public bool valid { get { return x < table.Count && y < table[x].Count; } }

    public string GetValue()
    {
        try
        {
            if (x < table.Count && y < table[x].Count)
            {
                return table[x][y];
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        return "";
    }
}


public class CsvReader : IEnumerable<Row>
{
    List<string> columns;
    public List<Row> rows = new List<Row>();

    public IEnumerator<Row> GetEnumerator()
    {
        return rows.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }

    public int rowCount { get { return rows.Count; } }
    public int colCount { get { return columns.Count; } }

    public Row atRow(int rowIndex)
    {
        return rows[rowIndex];
    }

    public Row AtRealRow(int rowIndex)
    {
        return atRow(rowIndex - 2);
    }

    public List<string> GetColumns()
    {
        return columns;
    }

    //static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public List<string> Column(string name)
    {
        return rows.Select(row => row[name]).ToList();
    }

    //public List<string> Column(int index)
    //{
    //    return rows.Select(row => row[alphabet[index].ToString()]).ToList();
    //}

    public CsvReader(string fileName)
    {
        using (StreamReader r = new StreamReader(fileName))
        {
            string line;
            line = r.ReadLine();

            rows.Clear();

            if (line != null)
            {
                //columns = line.Split(',').Where(v => v.Length > 0).Select(v => v.Trim()).ToList();
                columns = line.Split(',').Select(v => v.Trim()).ToList();

                Debug.Log(fileName + " Header: " + line);
                int rowIndex = 0;
                while ((line = r.ReadLine()) != null)
                {
                    var rawCells = new List<string>();

                    try
                    {
                        bool good = true;
                        int lastBad = 0;
                        string accumulatedCell = "";
                        for (int i = 0; i < line.Length; i++)
                        {
                            if (line[i] == ',' && good)
                            {
                                rawCells.Add(accumulatedCell.Trim());
                                accumulatedCell = "";
                            }
                            else if (line.Length > (i + 1) && line[i] == '"' && line[i + 1] == '"')
                            {
                                accumulatedCell += '"';
                                i++;
                            }
                            else if (line[i] == '"')
                            {
                                good = !good;
                            }
                            else
                                accumulatedCell += line[i];
                        }

                        rawCells.Add(accumulatedCell);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message + e.StackTrace + ", at row " + rowIndex);
                    }

                    OrderedDictionary cells = new OrderedDictionary();

                    try
                    {
                        int indexCounter = 0;
                        foreach (string cell in rawCells)
                        {
                            if (indexCounter < columns.Count)
                                cells[columns[indexCounter]] = cell;
                            //if (indexCounter < alphabet.Length)
                            //    cells[alphabet[indexCounter].ToString()] = cell;
                            indexCounter++;
                        }

                        rows.Add(new Row(cells));
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e.Message + e.StackTrace + ", at row " + rowIndex);
                    }

                    rowIndex++;
                }

                for (int i = 0; i < rows.Count; i++)
                {
                    if (i > 0)
                    {
                        rows[i].prev = rows[i - 1];
                        rows[i - 1].next = rows[i];
                    }

                    rows[i].index = i;
                    rows[i].table = rows;
                }
            }
        }
    }
}

static class CsvExt
{
    private static List<string> namespaces = new List<string> {};

    public static void ParseAttribs(object target, CsvCell location)
    {
        var fields = target.GetType().GetFields();
        var assignableAttributes = new List<Tuple<FieldInfo, int>>();

        foreach (var f in fields)
        {
            foreach (var a in f.GetCustomAttributes(true))
            {
                if (a is ParseAttrib)
                {
                    //f.SetValue(abil, attribInfo.CastTo<typeof(f.FieldType)>());
                    assignableAttributes.Add(new Tuple<FieldInfo, int>(f, (a as ParseAttrib).index - 1));
                }
            }
        }

        var sortedAttributes = assignableAttributes.OrderBy(val => val.Item2);


        int index = -1;
        foreach (var attribute in sortedAttributes)
        {

            //skip if attrib index is not contiguous
            while (index < attribute.Item2)
            {
                location = location.GetOffset(0, 1);
                index++;
            }

            string attribInfo = location.GetValue();

            if (attribInfo.Contains("="))
                attribInfo = attribInfo.Substring(attribInfo.IndexOf("=") + 1);

            attribInfo = attribInfo.TrimStart(' ');

            if (attribInfo == "")
                continue;

            if (attribute.Item1.FieldType == typeof(float))
            {
                var mult = attribute.Item1.HasAttribute<DistanceAttrib>() ? DataImporter.settings.DistanceMultiplier : 1;
                attribute.Item1.SetValue(target, attribInfo.CastTo<float>() * mult);
            }
            else if (attribute.Item1.FieldType == typeof(int))
            {
                attribute.Item1.SetValue(target, attribInfo.CastTo<int>());
            }
            else if (attribute.Item1.FieldType == typeof(bool))
            {
                attribute.Item1.SetValue(target, attribInfo.CastTo<bool>());
            }
            else if (attribute.Item1.FieldType == typeof(string))
            {
                attribute.Item1.SetValue(target, attribInfo);
            }
            else if (attribute.Item1.FieldType.IsEnum)
            {
                attribute.Item1.SetValue(target, Enum.Parse(attribute.Item1.FieldType, attribInfo));
            }
            else if (typeof(object).IsAssignableFrom(attribute.Item1.FieldType))
            {
                attribute.Item1.SetValue(target, ParseClass(location, out location));
            }
        }
    }


    public static object ClassFromName(string name)
    {
        Assembly ass = Assembly.GetAssembly(typeof(UnitInfo));
        Type t = ass.GetType(name, false, true);

        if (t == null)
        {
            foreach (var ns in namespaces)
            {
                if ((t = ass.GetType(ns + "." + name, false, true)) != null)
                {
                    break;
                }
            }
        }

        if (t != null)
        {
            var abil = Activator.CreateInstance(t);
            return abil;
        }
        Debug.Log("Unknown type of ability: " + name);
        return null;
    }

    public static object ParseClass(CsvCell location)
    {
        CsvCell outCell;
        return ParseClass(location, out outCell);
    }

    public static object ParseClass(CsvCell location, out CsvCell newLocation)
    {
        if (String.IsNullOrEmpty(location.GetValue()))
        {
            Debug.Log("Warning, empty ParseClass;");
            newLocation = location;
            return null;
        }
        var abil = ClassFromName(location.GetValue());
        if (abil != null)
        {
            ParseAttribs(abil, location);
        }
        newLocation = location;
        return abil;
    }

    public static object ParseClassFlat(CsvCell location, out CsvCell newLocation)
    {
        Assembly ass = Assembly.GetAssembly(typeof(UnitInfo));
        Type t = ass.GetType(location.GetValue(), false, true);

        if (t == null)
            foreach (var ns in namespaces)
            {
                if ((t = ass.GetType(ns + "." + location.GetValue(), false, true)) != null)
                {
                    break;
                }
            }

        if (t != null)
        {
            var abil = Activator.CreateInstance(t);

            var fields = abil.GetType().GetFields();
            var assignableAttributes = new List<Tuple<FieldInfo, ParseAttrib>>();

            foreach (var f in fields)
            {
                foreach (var a in f.GetCustomAttributes(true))
                {
                    if (a is ParseAttrib)
                    {
                        //f.SetValue(abil, attribInfo.CastTo<typeof(f.FieldType)>());
                        assignableAttributes.Add(new Tuple<FieldInfo, ParseAttrib>(f, (a as ParseAttrib)));
                    }
                }
            }

            var fieldInfos = new List<Tuple<string, string>>();

            while (true)
            {
                location = location.GetOffset(0, 1);

                if (location.hasValue() == false || location.GetValue().Contains("Attribute") == false)
                    break;

                string attribInfo = location.GetValue();
                string attribName = "";

                if (attribInfo.Contains("="))
                {
                    attribName = attribInfo.Split('=')[0].TrimEnd(' ');
                    attribInfo = attribInfo.Split('=')[1].TrimStart(' ');
                }

                if (attribInfo == "")
                    continue;

                fieldInfos.Add(new Tuple<string, string>(attribName, attribInfo));
            }

            foreach (var attribute in assignableAttributes)
            {
                string attribInfo = "";

                if (attribute.Item2.index != -1)
                {
                    if (fieldInfos.Count > attribute.Item2.index)
                        attribInfo = fieldInfos[attribute.Item2.index].Item2;
                }
                else if (attribute.Item2.name != "")
                {
                    var found = fieldInfos.FirstOrDefault(val => val.Item1 == attribute.Item2.name);
                    if (found != null)
                        attribInfo = found.Item2;
                }
                else
                {
                    var found = fieldInfos.FirstOrDefault(val => val.Item1 == attribute.Item1.Name);
                    if (found != null)
                        attribInfo = found.Item2;
                }


                if (attribInfo == "")
                {
                    continue;
                }

                if (attribute.Item1.FieldType == typeof(float))
                {
                    attribute.Item1.SetValue(abil, attribInfo.CastTo<float>());
                }
                else if (attribute.Item1.FieldType == typeof(int))
                {
                    attribute.Item1.SetValue(abil, attribInfo.CastTo<int>());
                }
                else if (attribute.Item1.FieldType == typeof(bool))
                {
                    attribute.Item1.SetValue(abil, attribInfo.CastTo<bool>());
                }
                else if (attribute.Item1.FieldType == typeof(string))
                {
                    attribute.Item1.SetValue(abil, attribInfo);
                }
                else if (attribute.Item1.FieldType.IsEnum)
                {
                    attribute.Item1.SetValue(abil, Enum.Parse(attribute.Item1.FieldType, attribInfo));
                }
            }

            newLocation = location;
            return abil;
        }
        else
        {
            Debug.Log("Unknown type of ability: " + t + ", " + location.GetValue());
        }

        newLocation = location;
        return null;
    }
}
