using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class AbstractGameObject : MonoBehaviour, ILivingState
{
    public ConnectionCollector connections = new ConnectionCollector();
    public int visibleIndex = 0;
    public Cell<int> currentIndex = new Cell<int>(); 
    public int previousLifeIndex = 0;

    public bool isZombie = false;
    public bool isFresh = true;
    public bool isDying = false;

    public string prefabName = "";

    public virtual void OnAppear()
    {
        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
        if (canvasGroup == null)
            canvasGroup = gameObject.AddComponent<CanvasGroup>();

        canvasGroup.alpha = 0.0f;
        canvasGroup.DOFade(1.0f, 0.3f);

        gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
        gameObject.GetComponent<RectTransform>().DOScale(1, 0.3f).SetEase(Ease.InOutElastic, 1.25f).ChangeStartValue(0);
    }

    public virtual void OnReplace()
    {
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.25f, 0.25f, 0.25f);
        gameObject.GetComponent<RectTransform>().DOScale(1, 0.5f).SetEase(Ease.InOutElastic, 1.25f).ChangeStartValue(0);
    }

    public bool isAlive
    {
        get { return isZombie; }
    }

    public virtual IEnumerator<IProcess> OnDispose()
    {
        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
        canvasGroup.DOFade(0.0f, 0.3f);

        yield return new BattleWait(0.3f);
    }
}