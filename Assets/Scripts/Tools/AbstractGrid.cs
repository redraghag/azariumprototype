﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using UniRx;
using ZergRush;
using DG.Tweening;

public enum AbstractDirection
{
    Forward,
    Backwards
}

public enum AbstractAlignment
{
    Center,
    Left,
    Right
}

public enum AbstractOrientation
{
    Vertical,
    Horizontal
}

public class AbstractGrid : MonoBehaviour {
    public List<AbstractGameObject> elements;

    public AbstractOrientation orientation;
    public AbstractDirection direction;
    public AbstractAlignment alignment = AbstractAlignment.Center;

    public int itemsPerRow = 0;
    public float horizontalSpacing = 5.0f;
    public float verticalSpacing = 5.0f;
    public bool alwaysRefill = true;
    public bool debug = false;
    public bool smooth = false;

    public ConnectionCollector connections = new ConnectionCollector();
    private int aliveCount { get { return elements.Count(val => val.isZombie == false); } }
    private float lastUpdateTime = 0;

    public IProcessExecution smoothUpdate;

    public void Link<T>(IEnumerable<T> data, Func<T, AbstractGameObject> fill)
    {
        Clear();
        Repopulate<T>(data, fill);
    }

    public void Link<T>(IEnumerable<T> data, string prefabName, Action<T, AbstractGameObject> fill)
    {
        Link(data, (d) =>
        {
            var slot = GetSlot(prefabName);
            slot.connections.DisconnectAll();

            fill(d, slot);
            return slot;
        });
    }

    public void Link<T>(IObservableCollection<T> data, string prefabName, Action<T, AbstractGameObject> fill)
    {
        Link(data, (d) =>
        {
            var slot = GetSlot(prefabName);
            slot.connections.DisconnectAll();

            fill(d, slot);
            return slot;
        });
    }

    public void Link<T>(IObservableCollection<T> data, Func<T, AbstractGameObject> fill)
    {
        Clear();
        Repopulate(data, fill);

        connections.add = data.ObserveAdd().Listen(val =>
        {
            if (alwaysRefill)
                Repopulate(data, fill);
            else
                Add(val.Value, fill);
        });
        connections.add = data.ObserveRemove().Listen(val =>
        {
            if(alwaysRefill)
                Repopulate(data, fill);
            else
                GameController.instance.executiveProducer.Execute(Remove(val.Index), null);
        });
        connections.add = data.ObserveReplace().Listen(val =>
        {
            Replace(val.NewValue, val.Index, fill);
        });
        connections.add = data.ObserveReset().Listen(val => 
        Repopulate(data, fill)
        );
    }

    public void Link(ICell<float> amount, float target, string prefabName, Action<AbstractGameObject, bool> fill, bool animate = false)
    {
        Clear();

        RepopulateWrap(0, (int)target, prefabName, fill);

        connections.add = amount.Bind(val =>
        {
            for (int i = 0; i < (int) target; i++)
            {
                bool active = i < val;
                fill(elements[i], active);

                if(animate && active)
                    elements[i].OnAppear();
            }
        });
    }

    public void LinkSubFill(ICell<float> amount, float target, string prefabName, Action<AbstractGameObject, float> fill, bool animate = false)
    {
        Clear();

        RepopulateWrap(0, (int)target, prefabName, fill);

        connections.add = amount.Bind(val =>
        {
            for (int i = 0; i < (int)target; i++)
            {
                float active = Mathf.Clamp(val - (float)i, 0, 1);
                fill(elements[i], active);
            }
        });
    }

    public IDisposable Link(ICell<int> amount, string prefabName, Action<AbstractGameObject> fill)
    {
        Clear();
        connections.add = amount.Bind(val =>
        {
            RepopulateWrap(val, val, prefabName, (v, b) => { fill(v); });
        });

        return Disposable.Create(() =>
        {
            Clear();
        });
    }

    void RepopulateWrap(int val, int target, string prefabName, Action<AbstractGameObject, bool> fill)
    {
        Repopulate(Enumerable.Range(0, target), (d) =>
        {
            var slot = GetSlot(prefabName);
            slot.connections.DisconnectAll();

            fill(slot, d < val);

            return slot;
        }, true);
    }

    void RepopulateWrap(float val, int target, string prefabName, Action<AbstractGameObject, float> fill)
    {
        Repopulate(Enumerable.Range(0, target), (d) =>
        {
            var slot = GetSlot(prefabName);
            slot.connections.DisconnectAll();

            fill(slot, (float)d);

            return slot;
        }, true);
    }

    public void Add<T>(T e, Func<T, AbstractGameObject> fill)
    {
        var newElement = fill(e);

        newElement.isFresh = true;
        newElement.gameObject.SetActive(true);

        if (elements.Contains(newElement))
        {
            elements.Remove(newElement);
            elements.Add(newElement);
        } else
            elements.Add(newElement);

        newElement.isZombie = false;
        Recalculate();
//        Debug.Log("Add " + newElement.visibleIndex);
        newElement.OnAppear();
    }

    public AbstractGameObject GetSlot(string prefab, bool revive = true)
    {
        foreach(var e in elements)
        {
            if(e.isZombie && e.isDying == false && e.prefabName == prefab)
            {
//                Debug.Log("Reused Slot " + e.visibleIndex);

                if (revive)
                {
                    e.gameObject.SetActive(true);
                    e.isZombie = false;
                    e.isDying = false;
                }

                return e;
            }
        }

        if (debug)
            Debug.Log("New Slot");

        GameObject obj = Instantiate(Resources.Load(prefab)) as GameObject;
        if (obj == null)
        {
            Debug.LogError("prefab not loaded");
        }
        AbstractGameObject actualThing = obj.GetComponent<AbstractGameObject>();
        actualThing.prefabName = prefab;
        return actualThing;
    }

    public void Update()
    {
        if (debug && (Time.realtimeSinceStartup - lastUpdateTime > 10))
        {
            Recalculate();
            lastUpdateTime = Time.realtimeSinceStartup;
        }
    }

    public IEnumerator<IProcess> Remove(AbstractGameObject e)
    {
        if (elements.Contains(e))
        {
            e.previousLifeIndex = e.visibleIndex;
            e.isDying = true;
            e.connections.DisconnectAll();

            yield return GameController.instance.executiveProducer.Execute(e.OnDispose(), e).Wait();

            if(e.gameObject != null)
                e.gameObject.SetActive(false);

            e.isZombie = true;
            e.isDying = false;

            Recalculate();
        }
    }

    public IEnumerator<IProcess>  Remove(int ix)
    {
        var els = elements.Where(val => val.isZombie == false && val.isDying == false).ToList();

        if(debug)
            Debug.Log("Remove " + ix + ", " + (els.Count > ix ? "ok" : "fucked"));

        var e = els.FirstOrDefault(val => val.visibleIndex == ix);

        yield return GameController.instance.executiveProducer.Execute(Remove(e), null).Wait();
    }

    public void Recalculate()
    {
        int index = 0;
        float maxWidth = 0.0f;
        int lineCount = 0;

        var sorted = elements.Where(val => val.isZombie == false && val.isDying == false).ToList();

        float columnLength = 0.0f;
        float currentRowLength = 0.0f;
        float tableWidth = gameObject.GetComponent<RectTransform>().sizeDelta.x;

        if (sorted.Count == 0)
            return;

        Vector2 elementSize = sorted[0].gameObject.GetComponent<RectTransform>().sizeDelta;
        List<float> lineWidth = new List<float>();

        List<int> elementCount = new List<int> ();

        int localIndex = 0;
        foreach (var obj in sorted)
        {
            if (itemsPerRow == 0)
            {
                maxWidth += orientation == AbstractOrientation.Horizontal
                    ? sorted[index].gameObject.GetComponent<RectTransform>().sizeDelta.x + horizontalSpacing
                    : sorted[index].gameObject.GetComponent<RectTransform>().sizeDelta.y + verticalSpacing;
            }
            else
            {
                currentRowLength += sorted[index].gameObject.GetComponent<RectTransform>().sizeDelta.x + (localIndex == 0 ? 0.0f : horizontalSpacing);
            }

            if (itemsPerRow > 0 && (++localIndex == itemsPerRow || obj == sorted.Last()))
            {
                if (currentRowLength > maxWidth)
                    maxWidth = currentRowLength;

                lineWidth.Add(currentRowLength);
                currentRowLength = 0.0f;

                elementCount.Add(localIndex);
                localIndex = 0;
                lineCount++;
            }

            obj.visibleIndex = index;
            obj.currentIndex.value = index;
            index++;
        }

        if (lineCount == 0)
        {
            elementCount.Add(sorted.Count);
            if(orientation == AbstractOrientation.Horizontal)
                lineWidth.Add(maxWidth);
            else
                lineWidth.Add(elementSize.x);
            lineCount = 1;
        }

        var layoutElement = gameObject.GetComponent<LayoutElement>();
        if (layoutElement != null)
        {
            if (orientation == AbstractOrientation.Horizontal)
                layoutElement.preferredWidth = maxWidth;
            else
                layoutElement.preferredHeight = maxWidth;
        }

        List<Vector2> lineOffset = new List<Vector2>();
        Vector2 localOffset = Vector2.zero;

        for (int l = 0; l < lineCount; l++)
        {
            switch (alignment)
            {
                case AbstractAlignment.Center:
                    lineOffset.Add(new Vector2(-lineWidth[l]*0.5f, -elementSize.y*0.5f));
                break;

                case AbstractAlignment.Left:
                    lineOffset.Add(new Vector2(0, -elementSize.y*0.5f));
                break;

                case AbstractAlignment.Right:
                    lineOffset.Add(new Vector2(lineWidth[l] * 0.5f, 0.0f) + new Vector2(-elementSize.x * 0.5f, -elementSize.y*0.5f) +
                                   new Vector2(-horizontalSpacing, 0.0f));
                break;
            }
        }

        Vector2 length = Vector2.zero;
        Vector2 lengthIncrement = Vector2.zero;

        foreach (var obj in sorted)
        {
            RectTransform rect = obj.gameObject.GetComponent<RectTransform>();
            lengthIncrement = orientation == AbstractOrientation.Horizontal ? new Vector2(rect.sizeDelta.x + horizontalSpacing, 0.0f) : new Vector2(0.0f, rect.sizeDelta.y + verticalSpacing);

            length += lengthIncrement;
        }

        length += lengthIncrement; //one more

        index = 0;
        for (int l = 0; l < lineCount; l++)
        {
            for (int i = 0; i < elementCount[l]; i++)
            {
                var obj = sorted[index];

                obj.transform.SetParent(this.transform, false);

                RectTransform rect = obj.gameObject.GetComponent<RectTransform>();

                rect.anchorMin = new Vector2(0.5f, 0.5f);
                rect.anchorMax = new Vector2(0.5f, 0.5f);

                Vector2 finalPosition = Vector2.zero;

                switch (alignment)
                {
                    case AbstractAlignment.Left:
                        finalPosition = new Vector2(localOffset.x + rect.sizeDelta.x * 0.5f, localOffset.y + rect.sizeDelta.y * 0.5f) + lineOffset[l];
                        break;

                    case AbstractAlignment.Right:
                        finalPosition = new Vector2(-localOffset.x, localOffset.y + rect.sizeDelta.y * 0.5f) + lineOffset[l];
                        break;

                    case AbstractAlignment.Center:
                        finalPosition = new Vector2(localOffset.x + rect.sizeDelta.x * 0.5f, localOffset.y + rect.sizeDelta.y * 0.5f) + lineOffset[l];
                        break;
                }


                if (smooth == false)
                    rect.anchoredPosition = finalPosition;
                else
                {
                    if (obj.isFresh)
                    {
                        rect.anchoredPosition = direction == AbstractDirection.Forward ? -length : length;
                    }
                    rect.DOAnchorPos(finalPosition, 0.4f);
                }

                if (orientation == AbstractOrientation.Vertical)
                {
                    localOffset.y += (rect.sizeDelta.y + verticalSpacing) * (direction == AbstractDirection.Forward ? 1 : -1);
                }
                else
                {
                    localOffset.x += (rect.sizeDelta.x + horizontalSpacing) * (direction == AbstractDirection.Forward ? 1 : -1);
                }


                obj.gameObject.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
                obj.isFresh = false;

                index++;
            }

            localOffset.y -= elementSize.y + verticalSpacing;
            localOffset.x = 0;
        }
    }

    void Resize<T>(IEnumerable<T> data, Func<T, AbstractGameObject> fill)
    {
        foreach (var e in elements)
        {
            e.connections.DisconnectAll();
            Destroy(e.gameObject);
        }

        elements.Clear();
        foreach (var obj in data)
        {
            var result = fill(obj);

            if (result != null)
            {
                elements.Add(result);
            }
        }
        Recalculate();

        elements.ForEach(val => {
            if (val.isZombie == false)
                val.OnAppear();
        });
    }

    IEnumerator<IProcess> ResizeCoro<T>(IEnumerable<T> data, Func<T, AbstractGameObject> fill)
    {

        int newCount = data.Count();
        int diff = newCount - aliveCount;

        if (diff > 0)
        {
            var list = data.Skip(aliveCount).ToList();
            for (int i = 0; i < diff; i++)
            {
                yield return null;
                Add(list[i], fill);
            }
        }
        else if (diff < 0)
        {
            var sorted = elements.Where(val => val.isZombie == false && val.isDying == false).ToList();
            var toRemove = new List<AbstractGameObject>();

            for (int i = 1; i < Math.Abs(diff) + 1; i++)
            {
                toRemove.Add(sorted[sorted.Count - i]);
            }

            for (int i = 0; i < Math.Abs(diff); i++)
            {
                yield return new CoroProcess(Remove(toRemove[i]), null);
            }
        }
    }

    public void ForeachWithIndices(Action<AbstractGameObject, int> iterateAction)
    {
        elements.ForeachWithIndices(iterateAction);
    }

    void Repopulate<T>(IEnumerable<T> data, Func<T, AbstractGameObject> fill, bool forceCreate = false)
    {
        if (debug)
            Debug.Log("Repopulate");

        if (!alwaysRefill && !forceCreate)
            GameController.instance.executiveProducer.Execute(ResizeCoro(data, fill), null);
        else
            Resize(data, fill);
        Recalculate();
    }

    void Replace<T>(T newObject, int index, Func<T, AbstractGameObject> fill)
    {
        if (debug)
            Debug.Log("Replace " + index);

        elements[index].connections.DisconnectAll();
        elements[index].isZombie = true;

        var e = fill(newObject);
        e.OnReplace();

        Recalculate();
    }

    public void Clear()
    {
        connections.DisconnectAll();

        foreach (var e in elements)
        {
            if (e != null)
            {
                e.connections.DisconnectAll();
                Destroy(e.gameObject);
            }
        }
        elements.Clear();
    }
}
