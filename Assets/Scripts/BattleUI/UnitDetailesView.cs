﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class UnitDetailesView : BattleUIBase
{
    public Text name;
    public Text costText;

    public Text statsText;
    public Text abilityDescription;
    public Text azariumDescription;
    public AbstractGrid buffs;

    public void Init(BattleUnit unit)
    {
        connections.DisconnectAll();
        string abilDesc = "";
        name.text = unit.info.name;
        costText.text = unit.info.totalCost.ToString();
        var buildingInfo = gameData.availableRace[0].bInfo[unit.id];
        foreach (var abil in buildingInfo.abilityDescription)
        {
            abilDesc += abil + "\n";
        }
        abilityDescription.text = abilDesc;
        if (buildingInfo.azariupUpgradeDecription.Count > 0)
        {
            azariumDescription.text = unit.azariumBought.value == 0 ? "" : buildingInfo.azariupUpgradeDecription[unit.azariumBought.value - 1];
        }
        else
        {
            azariumDescription.text = "";
        }

        connections.add = statsText.SetContent("Hp: {0}/{4}\nAttack: {1}\nArmor: {2}\nDebuffResist: {3}\n", unit.hp,
            unit.attack, unit.armor, unit.debuffResist, unit.maxHp);
        buffs.Link(unit.buffs, "BuffDetailes", (str, view) =>
        {
            view.GetComponent<Text>().text = str;
        });
    }
}
