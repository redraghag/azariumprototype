﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class BuildingsStateView : BattleUIBase
{
    public AbstractGrid grid;
    public EmptyStream requestNew = new EmptyStream();
    public IntCell selectedIndex = new IntCell(-1);
    public BuildingDescriptionView descView;

    public void Init(PlayerState state)
    {
        UpdateBuildigns();
        state.buildings.ObserveCountChanged().Listen(_ =>
        {
            UpdateBuildigns();
        });
        selectedIndex.Bind(val =>
        {
            if (val == -1)
            {
                descView.Hide();
                return;
            }
            descView.Fill(player.buildings[val]);
        });
    }

    public void OnEnter()
    {
        selectedIndex.value = -1;
    }

    void UpdateBuildigns()
    {
        selectedIndex.value = -1;
        connections.DisconnectAll();
        grid.Link(Enumerable.Range(0, player.buildings.Count + 1), "BuldingOption", (index, view) =>
        {
            var v = view as BuildingOptionView;
            v.connections.DisconnectAll();
            v.SetState(BuildingOptionState.Normal);
            if (index < player.buildings.Count)
            {
                var bState = player.buildings[index];
                v.connections.add = bState.info.Bind(v.InitWithOption);
                v.connections.add = v.button.ClickStream().Listen(() =>
                {
                    if (selectedIndex.value == index)
                    {
                        selectedIndex.value = -1;
                        return;
                    }
                    selectedIndex.value = index;
                });
                v.connections.add = selectedIndex.Bind(val => v.SetSelected(val == index));
                var canPay = 
                    from info in bState.info
                    from cp in info.next.Count > 0 ? player.CanPayCell(gameData.availableRace[0].bInfo[info.next[0]].cost) : new StaticCell<bool>()
                    select cp;

                var good = player.azarium.Merge(bState.azariumUp as ICell<int>,
                    (pAz, currAz) => pAz >= settings.AbilityAzariumCost && currAz == 0);
                var canAzUp = 
                    bState.info.Select(info => info.azariupUpgradeDecription.Count > 0 ? good : new StaticCell<bool>())
                    .Join();
                //var canAzUp = 
                //    from info in bState.info
                //    from ap in (info.azariupUpgradeDecription.Count > 0 ? good : (new StaticCell<bool>() as ICell<bool>)) as ICell<bool>
                //    select ap;

                v.connections.add = canAzUp.MergeBind(canPay, (ap, cp) =>
                {
                    if (cp)
                        v.SetState(BuildingOptionState.CanBeUpgraded);
                    else if (ap)
                        v.SetState(BuildingOptionState.CanBeUpgradedAz);
                    else 
                        v.SetState(BuildingOptionState.Normal);
                });
            }
            else
            {
                v.name.text = "New";
                v.SetIcon("Icon4");
                v.connections.add = v.button.ClickStream().Listen(() =>
                {
                    requestNew.Send();
                    selectedIndex.value = -1;
                });
            }
        });
    }
}
