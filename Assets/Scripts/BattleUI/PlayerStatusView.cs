﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

class PlayerStatusView : BattleUIBase
{
    public Text goldText;
    public Text azText;
    public Text waveTimeText;
    public Text announcement;

    private ConnectionCollector connections = new ConnectionCollector();

    public void Init(PlayerState state)
    {
        connections.DisconnectAll();
        var nextWaveGold = state.system.waveNumner.Select(num =>
        {
            return (settings.BaseMinGain + num * settings.BaseMinGainGrowth) + "-" +
                   (settings.BaseMaxGain + num * settings.BaseMaxGainGrowth);
        });

        connections.add = goldText.SetContent("{0} (+{1})", state.gold.Select(val => val.ToString()), nextWaveGold);
        var azCaptured = state.system.azariumMineState.Select(val => val == (state.side == Side.Blue ? 1 : -1));
        azCaptured.When().Listen(() => ShowAnnouncement("Center captured. Azarium income doubled!", new Color(0, 0.7f, 0, 1)));
        azCaptured.WhenUpdatedToSatisfy(val => !val).Listen(() => ShowAnnouncement("Center lost.", Color.white));
        var azTextCell = player.azarium.Merge(azCaptured, (a, capt) =>
        {
            return a + " (" + (capt ? 2 : 1) * settings.AzariumRegen + "/sec)";
        });
        connections.add = azText.SetContent("<color=#{0}ff>{1}</color>", azCaptured.Select(val => val ? "00ff00" : "ffffff"), azTextCell);
        waveTimeText.SetContent("Spawn: {0:0}",
            state.system.lastSpawn.Merge(state.system.serverTime, (ls, t) => ls - t + settings.WaveSpawnInterval));
    }

    
    public void ShowAnnouncement(string text, Color color)
    {
        StopAllCoroutines();
        StartCoroutine(ShowAnnouncementCoro(text, color));
    }

    public IEnumerator ShowAnnouncementCoro(string text, Color color)
    {
        announcement.SetActiveSafe(true);
        announcement.color = color;
        announcement.text = text;
        yield return new WaitForSeconds(3);
        announcement.SetActiveSafe(false);
    }
}
