﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BattleBehaviour : BadassNetworkBehaviour
{
    public GameSettings settings { get { return GameController.instance.gameData.settings; } }
    public GameData gameData { get { return GameController.instance.gameData; } }
    public BattleSystem system { get { return BattleSystem.instance; } }
    public Map map { get { return Map.current; } }

}
