﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class ChooseBuildingTypeView : BattleUIBase {

    public AbstractGrid buildingOptionsGrid;
    public BuildingDescriptionView details;
    public Cell<BuildingInfo> selectedBuilding = new Cell<BuildingInfo>();

    public void Init(RaceInfo race)
    {
        selectedBuilding.Bind(val =>
        {
            if (val == null)
            {
                details.Hide();
                return;
            }
            details.Fill(val);
        });
        buildingOptionsGrid.Link(race.bInfo.Values.Where(b => b.initial), "BuldingOptionNew", (option, view) =>
        {
            var v = view as BuildingOptionView;
            v.connections.add = v.button.ClickStream().Listen(() =>
            {
                if (selectedBuilding.value == option)
                {
                    //requestBuild.Send(option);
                    selectedBuilding.value = null;
                    return;
                }
                selectedBuilding.value = option;
            });
            var selected = selectedBuilding.Select(val => val == option);
            var canPay = player.CanPayCell(option.cost);
            v.connections.add = selected.MergeBind(canPay, (s, cp) =>
            {
                v.SetSelected(s);
                if (s && cp) v.SetState(BuildingOptionState.Normal);
                else if (!s && cp) v.SetState(BuildingOptionState.CanBeBought);
                else v.SetState(BuildingOptionState.NotEnoughMoney);
            });
            v.InitWithOption(option);
            connections.add = canPay
                .Bind(val => v.SetState(val ? BuildingOptionState.CanBeBought : BuildingOptionState.NotEnoughMoney));
        });
    }
}
