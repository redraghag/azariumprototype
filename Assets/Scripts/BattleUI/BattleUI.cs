﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Prototype.NetworkLobby;
using UnityEngine.Assertions.Must;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ZergRush;

class BattleUI : BattleBehaviour
{
    [HideInInspector]
    public PlayerState state;
    [HideInInspector, NonSerialized]
    public GameData data;

    public PlayerStatusView status;
    public BuildingDescriptionView description;
    public ChooseBuildingTypeView choose;
    public BuildingsStateView stateView;
    public UnitDetailesView unitView;
    public SpellPanel spells;
    public Button backButton;
    public Button exitGameButton;
    public Button pauseButton;
    public Text pausedText;
    public CameraScript camera;
    public Text serverStatusText;
    public Text spawnWaveText;
    public Image endingPanel;
    public Button endingLeaveButton;

    public GameObject overlayPanel;
    public Text overlayText;


    public Cell<BattleUnit> currentSelection = new Cell<BattleUnit>(); 

    public static BattleUI instance;

    List<GameObject> views = new List<GameObject>();
    ConnectionCollector connections = new ConnectionCollector();

    private InorganicCollection<State> navigation = new InorganicCollection<State>();
    abstract class State
    {
        public MonoBehaviour view;
        public abstract void Go(BattleUI self);
    }

    class NormalState : State
    {
        public override void Go(BattleUI self)
        {
        }
    }
    class ChooseState : State
    {
        public override void Go(BattleUI self)
        {
        }
    }
    class UnitDetailesState : State
    {
        public BattleUnit unit;
        public override void Go(BattleUI self)
        {
            self.unitView.Init(unit);
        }
    }
    class DetailesState : State
    {
        public BuildingInfo info;
        public Building building;
        public UpgradeRequest request;
        public override void Go(BattleUI self)
        {
            self.description.Fill(info, building, request);
        }
    }

    void Awake()
    {
        instance = this;
    }

    public BattleSystem system;
    public void Start()
    {
        data = GameController.instance.gameData;
        navigation.ObserveCountChanged().Listen(v => backButton.SetActiveSafe(v != 0));
        backButton.ClickStream().Listen(GoBack);
        views.Add(description.gameObject);
        views.Add(choose.gameObject);
        views.Add(stateView.gameObject);
        views.Add(unitView.gameObject);

        serverStatusText.SetContent(system.state.Select(s => s == 0 ? "Waiting for players" : ""));
    }

    public void ShowHint(RectTransform source, string text)
    {
        overlayText.text = text;
        overlayPanel.SetActiveSafe(true);
        Vector3 finalPos = source.position;
        Vector2 panelSizeDelta = overlayPanel.GetComponent<RectTransform>().sizeDelta;
        finalPos.y += source.sizeDelta.y + panelSizeDelta.y / 2;
        finalPos.x += panelSizeDelta.x * 0.4f;
        overlayPanel.transform.position = finalPos;
    }

    public void HideHint()
    {
        overlayPanel.SetActiveSafe(false);
    }

    void Update()
    {
        if (state == null)
        {
            return;
        }
        for (int i = 1; i < 7; i++)
        {
            if (Input.GetKeyDown(i.ToString()))
            {
                if (current is NormalState)
                {
                    if (state.buildings.Count >= i)
                    {
                        var b = state.buildings[i - 1];
                        ShowBuildingDetailes(b.info.value, b, null);
                    }
                    else
                    {
                    }
                }
                else if (current is ChooseState)
                {
                    ShowBuildingDetailes(state.race.bInfo.Values.Where(v => v.initial).ElementAt(i - 1), null, null);
                }
                else if (current is DetailesState)
                {
                    var d = current as DetailesState;
                    if (d.building != null)
                    {
                        if (d.info.next.Count >= i)
                        {
                            //description.upgradeRequest.Send(new UpgradeRequest {b = d.building, upIndex = i - 1});
                        }
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (current is DetailesState)
            {
                var d = current as DetailesState;
                if (d.building == null)
                {
                    if (d.request != null)
                    {
                        //description.upCommit.Send(d.request);
                    }
                    else
                    {
                        //description.buildRequest.Send(d.info);
                    }
                }
            }
            if (current is NormalState)
            {
                ShowFreeSlotOptions();
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo;
            var rayPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayPoint, out hitInfo, 10000f, 1024))
            {
                var isUnit = hitInfo.transform.GetComponent<BaseUnitView>();
                if (isUnit != null)
                {
                    var unit = isUnit.transform.parent.GetComponent<BattleUnit>();
                    ShowUnitDetailes(unit);
                    currentSelection.value = unit;
                }
                else
                {
                    // then it is spawning node and can be moved 
                    selected = hitInfo.transform;
                    currentSelection.value = null;
                }
            }
            else if (current is UnitDetailesState && EventSystem.current.IsPointerOverGameObject() == false)
            {
                ShowNormalState();
                currentSelection.value = null;
            }
            if (EventSystem.current.IsPointerOverGameObject() == false)
            {
                ResetSelects();               
            }
        }
        if (Input.GetMouseButton(0) && selected != null)
        {
            RaycastHit hitGround;
            var rayPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(rayPoint, out hitGround, 10000f, 1))
            {
                selected.position = hitGround.point;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (selected != null)
            {
                state.CmdSetSpawnPosition(selected.gameObject, selected.position);
            }
            selected = null;
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            GoBack();
        }
    }

    public void ResetSelects()
    {
        choose.selectedBuilding.value = null;
        stateView.selectedIndex.value = -1;
        description.upView.upSelect.value = null;
        description.statView.azSelect.value = -1;
    }

    public Transform selected;

    public void Init(PlayerState p)
    {
        Debug.Log("battle ui init called");
        state = p;
        connections.DisconnectAll();
        status.Init(state);
        choose.Init(p.race);
        stateView.Init(state);
        spells.Init(state);

        //Commands
        description.requestBuild.Listen(bInfo => p.CmdBuild(bInfo.name));
        description.upView.upCommit.Listen(up => p.CmdUpgrade(state.buildings.IndexOf(up.b), up.upIndex));
        description.statView.azUpgradeRequest.Listen(up => p.CmdAzUnlock(stateView.selectedIndex.value, up));

        //connections.add = stateView.requestDetails.Listen(b => ShowBuildingDetailes(b.info.value, b, null));
        connections.add = stateView.requestNew.Listen(ShowFreeSlotOptions);
        //connections.add = choose.requestBuildDetails.Listen(info => ShowBuildingDetailes(info, null, null));
        //connections.add = description.upgradeRequest.Listen(up => ShowBuildingDetailes(state.race.bInfo.AtOrDefault(up.b.info.value.next[up.upIndex]), null, up));

        endingLeaveButton.SetActiveSafe(false);
        endingPanel.color = new Color(0,0,0,0);
        connections.add = endingLeaveButton.ClickStream().Listen(() => { LobbyManager.s_Singleton.GoBackButton(); });

        ShowNormalState();
        camera.SetSide(p.side);

        connections.add = pauseButton.ClickStream().Listen(() => state.CmdPause(BattleSystem.instance.paused.value));
        BattleSystem.instance.paused.Bind(pause => pausedText.text = pause ? "Paused" : "Pause");
    }

    public void ShowNormalState()
    {
        ShowState(new NormalState {view = stateView});
        navigation.Clear();
    }

    public void ShowUnitDetailes(BattleUnit unit)
    {
        ShowState(new UnitDetailesState{view = unitView, unit = unit});
        navigation.Clear();
    }

    public void ShowFreeSlotOptions()
    {
        ShowState(new ChooseState {view = choose});
    }

    public void ShowBuildingDetailes(BuildingInfo info, Building state, UpgradeRequest request)
    {
        ShowState(new DetailesState {view = description, building = state, info = info, request = request});
    }

    public void ShowUpView(BuildingInfo info, UpgradeRequest upRequest)
    {
        ShowState(new DetailesState {view = description, building = null, info = info, request = upRequest});
    }

    public void ShowResult(bool victory)
    {
        endingLeaveButton.SetActiveSafe(true);
        endingPanel.SetActiveSafe(true);
        endingPanel.sprite = Resources.Load<Sprite>(victory ? "victory" : "defeat");
        endingPanel.color = new Color(1, 1, 1, 0);
        endingPanel.DOColor(Color.white, 1);
    }

    private State current;
    void ShowState(State s)
    {
        ResetSelects();
        Activate(s.view);
        s.Go(this);
        if (current != null)
            navigation.Add(current);
        current = s;
    }
    public void GoBack()
    {
        if (navigation.Count == 0) ShowNormalState();
        var s = navigation[navigation.Count - 1];
        current = s;
        navigation.Remove(s);
        Activate(s.view);
        s.Go(this);
    }

    void Activate(MonoBehaviour view)
    {
        view.SetActiveSafe(true);
        foreach (var s in views)
        {
            if (view.gameObject != s)
            {
                s.SetActiveSafe(false);
            }
        }
    }
}
