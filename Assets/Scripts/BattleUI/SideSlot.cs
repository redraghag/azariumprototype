﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SideSlot : MonoBehaviour
{
    public Transform towerPos;
    public Transform castlePosition;
    public List<PlayerSlot> playerSlots;

    public Transform minPlacement;
    public Transform maxPlacement;
}
