﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum AzarioumButtonState
{
    Locked,
    Normal,
    Bought,
}

public class AzariumButton : MonoBehaviour
{
    public Button button;
    public Image locked;
    public Image bought;
    public Text text;

    public void SetState(AzarioumButtonState state)
    {
        //text.color = state == AzarioumButtonState.Disabled ? Color.gray : Color.white;
        //bought.color = Color.white;
        //button.interactable = state != AzarioumButtonState.Normal;
        //button.GetComponent<Image>().color = Color.white;
        bought.SetActiveSafe(state == AzarioumButtonState.Bought);
        //locked.SetActiveSafe(state == AzarioumButtonState.Locked);
        button.interactable = state != AzarioumButtonState.Locked;
        //if (state == AzarioumButtonState.Locked)
        //{
        //    bought.SetActiveSafe(false);
        //    button.interactable = false;
        //}
        //else if (state == AzarioumButtonState.Normal)
        //{
        //    button.interactable = true;
        //    bought.SetActiveSafe(false);
        //}
        //else if (state == AzarioumButtonState.Bought)
        //{
            
        //}
    }
}
