﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public enum BuildingOptionState
{
    Disabled,
    CanBeBought,
    Normal,
    CanBeUpgraded,
    CanBeUpgradedAz,
    NotEnoughMoney,
    ReadyToBuild,
    NotReadyToBuild
}

class BuildingOptionView : AbstractGameObject
{
    public Text name;
    public Button button;
    public PriceView cost;
    public Image border;
    public Image hammer;
    public Image tagImage;

    public void SetIcon(string icon)
    {
        button.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + icon + "_norm" );
        button.spriteState = new SpriteState
        {
            highlightedSprite = Resources.Load<Sprite>("Icons/" + icon + "_up"),
            pressedSprite = Resources.Load<Sprite>("Icons/" + icon + "_down")
        };
    }

    public void InitWithOption(BuildingInfo info)
    {
        name.text = info.name;
        SetIcon(info.icon);
        if (cost != null)
        {
            cost.InitWithCost(info.cost);
        }
    }

    public void SetState(BuildingOptionState state)
    {
        button.interactable = state != BuildingOptionState.Disabled;
        hammer.SetActiveSafe(state == BuildingOptionState.ReadyToBuild || state == BuildingOptionState.NotReadyToBuild);
        tagImage.SetActiveSafe(true);
        hammer.sprite = state == BuildingOptionState.NotReadyToBuild
            ? UIRes.instance.hammerRed
            : UIRes.instance.hammerGreen;
        //if (state == BuildingOptionState.CanBeBought)
        //{
        //    tagImage.sprite = UIRes.instance.plus;
        //}
        if (state == BuildingOptionState.CanBeUpgraded)
        {
            tagImage.sprite = UIRes.instance.upArrow;
        }
        else if (state == BuildingOptionState.CanBeUpgradedAz)
        {
            tagImage.sprite = UIRes.instance.upArrowAz;
        }
        //else if (state == BuildingOptionState.NotEnoughMoney)
        //{
        //    tagImage.sprite = UIRes.instance.notEnoughGoldIcon;
        //}
        else
        {
            tagImage.SetActiveSafe(false);
        }
        if (cost != null)
        {
            var color = state == BuildingOptionState.NotEnoughMoney ? Color.red : Color.white;
            cost.count1.color = color;
            cost.count2.color = color;
        }
    }

    public void SetSelected(bool selected)
    {
        border.SetActiveSafe(selected);
    }

    public void SetBuildMode(bool buildMode)
    {
        hammer.SetActiveSafe(buildMode);
    }
}
