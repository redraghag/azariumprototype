﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

class UpgradeRequest
{
    public Building b;
    public int upIndex;
}

class AzUnlockRequest
{
    public Building b;
    public int azIndex;
}

class BuildingDescriptionView : BattleUIBase
{
    public BuildingOptionView iconView;
    public UpgradeView upView;
    public StatsView statView;
    public Stream<BuildingInfo> requestBuild = new Stream<BuildingInfo>();

    public void Fill(Building building)
    {
        connections.DisconnectAll();
        this.SetActiveSafe(true);
        //iconView.InitWithOption(building.info.value);
        upView.Init(building);
        statView.Init(building.info.value);
        statView.InitAzariumState(building.azariumUp);
    }

    public void Fill(BuildingInfo info)
    {
        connections.DisconnectAll();
        this.SetActiveSafe(true);
        iconView.SetActiveSafe(true);
        iconView.InitWithOption(info);
        iconView.SetState(BuildingOptionState.Normal);
        var canPay = player.CanPayCell(info.cost);
        connections.add = canPay.Bind(can => 
        {
            if (can) iconView.SetState(BuildingOptionState.ReadyToBuild);
            else iconView.SetState(BuildingOptionState.NotReadyToBuild);
        });
        connections.add = iconView.button.ClickStream().Listen(() => requestBuild.Send(info));
        statView.Init(info);
        upView.InitNew(info);
        statView.DisableAzButtons();
    }

    public void Hide()
    {
        this.SetActiveSafe(false);
    }

    public void Fill(BuildingInfo info, Building state, UpgradeRequest request)
    {
        connections.DisconnectAll();

        //name.text = info.name;
        //icon.sprite = Resources.Load<Sprite>("Icons/" + info.icon + "_norm");
        //if (info.functional)
        //    statsText.text = "";
        //else
        //    statsText.text = ComposeUnitDescription(player.gameData.uInfo[info.unitToSpawn]);
        //costText.text = info.cost.ToString();
        //abilityDescription.text = abilDesc;
        //azButtons.InitWithCorrespondingList(info.azariupUpgradeDecription, (azDesc, azButton) =>
        //{
        //    azButton.text.text = azDesc;
        //});
        //if (state == null)
        //{
        //    foreach (var azariumButton in azButtons)
        //    {
        //        azariumButton.SetState(AzarioumButtonState.Disabled);
        //    }
        //    foreach (var upButton in upButtons)
        //    {
        //        upButton.SetActiveSafe(false);
        //    }
        //    buildButton.SetActiveSafe(true);
        //    upgradesNode.SetActive(false);
        //    if (request == null)
        //        connections.add = buildButton.ClickStream().Listen(() => buildRequest.Send(info));
        //    else
        //        connections.add = buildButton.ClickStream().Listen(() => upCommit.Send(request));
        //}
        //else
        //{
        //    buildButton.SetActiveSafe(false);
        //    upgradesNode.SetActive(true);
        //}
    }

    string ComposeUnitDescription(UnitInfo unit)
    {
        var str = "";
        str += string.Format("Name: {0}\n", unit.name);
        str += string.Format("Health: {0}\n", unit.hp);
        str += string.Format("Armor: {0}\n", unit.armor);
        str += string.Format("ArmorType: {0}\n", unit.armorType);
        str += string.Format("Damage: {0}\n", unit.baseDamage);
        return str;
    }
}
