﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class BattleUIBase : MonoBehaviour
{
    protected PlayerState player
    {
        get { return BattleUI.instance.state; }
    }
    protected GameSettings settings
    {
        get { return BattleUI.instance.data.settings; }
    }
    protected GameData gameData
    {
        get { return BattleUI.instance.data; }
    }
    protected ConnectionCollector connections = new ConnectionCollector();
}
