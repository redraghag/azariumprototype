﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class SideState : BattleBehaviour
{
    public Castle castle;
    public BuildingAI tower;
    public SideSlot slot;
    public BattleSystem system;
    [NetworkStatic] public Side side;
    public List<PlayerState> states;
    
    [HideInInspector]
    public List<PlayerState> players = new List<PlayerState>(); 

    [Server]
    public void Init(List<PlayerState> p)
    {
        players = p;
        //castle = BattleSystem.BattlePrefabInst<Castle>("Castle", slot.castlePosition);
        //castle.side = side;
        //castle.id = "Castle";
        //castle.RefreshStats();
        //BattleSystem.NetworkInstUnit(castle);

        tower = BattleSystem.BattlePrefabInst<BuildingAI>("Tower", slot.towerPos);
        tower.side = side;
        tower.id = "Tower";
        tower.RefreshStats();
        tower.hp.value = 0;
        BattleSystem.NetworkInstUnit(tower);
    }
}
