﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

class MainMenu : MonoBehaviour
{
    public Text infoText; 
    public void StartGame()
    {
        LobbyManager.s_Singleton.mainMenuPanel.GetComponent<LobbyMainMenu>().OnClickOpenServerList();
    }

    public void LocalGame()
    {
        LobbyManager.s_Singleton.mainMenuPanel.GetComponent<LobbyMainMenu>().OnClickHost();
        LobbyManager.s_Singleton.StartCoroutine(HostCoro());
    }

    IEnumerator HostCoro()
    {
        yield return new WaitForSeconds(0.2f);
        LobbyManager.s_Singleton.AddLocalPlayer();
    }

    public void ShowMessage(string msg)
    {
        infoText.text = msg;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
