﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

class UpgradeView : BattleUIBase
{
    public List<BuildingOptionView> upButtons;
    public BuildingOptionView iconView;
    public StatsView statsView;
    public Cell<BuildingInfo> upSelect = new Cell<BuildingInfo>();
    public Stream<UpgradeRequest> upCommit = new Stream<UpgradeRequest>();
    public PriceView price;
    public GameObject azariumNode;
    public List<AzariumButton> azariumButtons = new List<AzariumButton>(); 

    public void Init(Building building)
    {
        var info = building.info.value;
        InitNew(info);
        upSelect.value = info;
        //connections.add = iconView.button.ClickStream().Listen(() => upSelect.value = info);
        if (info.next.Count > 0)
        {
            iconView.SetActiveSafe(false);
            this.SetActiveSafe(true);
        }
        else
        {
            iconView.InitWithOption(building.info.value);
            iconView.SetActiveSafe(true);
            iconView.SetBuildMode(false);
            iconView.SetSelected(false);
            iconView.SetState(BuildingOptionState.Normal);
            this.SetActiveSafe(false);
        }

        for (int i = 0; i < info.next.Count; i++)
        {
            var button = upButtons[i];
            var bInfo = gameData.availableRace[0].bInfo[info.next[i]];
            var iCopy = i;
            connections.add = button.button.ClickStream().Listen(() =>
            {
                if (upSelect.value == bInfo)
                {
                    if (player.CanPay(bInfo.cost)) upCommit.Send(new UpgradeRequest {b = building,upIndex = iCopy});
                }
                else
                {
                    upSelect.value = bInfo;
                }
            });
            var state = from selected in upSelect.Select(u => u == bInfo)
                from enoughtMoney in player.CanPayCell(bInfo.cost)
                select
                    selected && enoughtMoney
                        ? BuildingOptionState.ReadyToBuild
                        : (!selected && enoughtMoney
                            ? BuildingOptionState.CanBeUpgraded
                            : BuildingOptionState.NotEnoughMoney);
            connections.add = state.Bind(button.SetState);
        }

        connections.add = upSelect.Bind(up =>
        {
            if (up == null) return;
            statsView.Init(up);
        });
    }
    public void InitNew(BuildingInfo info)
    {
        connections.DisconnectAll();

        if (info.next.Count > 0)
        {
            var next = info.next[0];
            var bInfo = gameData.availableRace[0].bInfo[next];
            price.InitWithCost(bInfo.cost);
            connections.add = player.CanPayCell(bInfo.cost).Bind(val => price.SetCantPay(!val));
        }
        else
        {
        }

        for (int i = 0; i < upButtons.Count; i++)
        {
            var button = upButtons[i];
            if (info.next.Count > i)
            {
                var next = info.next[i];
                var bInfo = gameData.availableRace[0].bInfo[next];
                button.SetActiveSafe(true);
                button.InitWithOption(bInfo);
                button.SetState(BuildingOptionState.Disabled);
            }
            else
            {
                button.SetActiveSafe(false);
            }
        }
    }
}
