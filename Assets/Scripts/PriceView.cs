﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class PriceView : MonoBehaviour
{
    public Image icon1;
    public Image icon2;

    public Text count1;
    public Text count2;

    public void InitWithCost(Cost cost)
    {
        icon2.gameObject.SetActiveSafe(false);
        icon1.gameObject.SetActiveSafe(false);
        count1.gameObject.SetActiveSafe(false);
        count2.gameObject.SetActiveSafe(false);
        if (cost.gold > 0)
        {
            icon1.sprite = UIRes.instance.gold;
            icon1.gameObject.SetActiveSafe(true);

            count1.text = cost.gold.ToString("0.");
            count1.gameObject.SetActiveSafe(true);
            if (cost.azarium > 0)
            {
                icon2.sprite = UIRes.instance.az;
                icon2.gameObject.SetActiveSafe(true);
                count2.text = cost.azarium.ToString("0.");
                count2.gameObject.SetActiveSafe(true);
            }
        }
        else if (cost.azarium > 0)
        {
            icon1.sprite = UIRes.instance.az;
            icon1.gameObject.SetActiveSafe(true);
            count1.text = cost.azarium.ToString("0.");
            count1.gameObject.SetActiveSafe(true);
        }
    }

    public void SetCantPay(bool cant)
    {
        var color = cant ? Color.red : Color.white;
        count1.color = color;
        count2.color = color;
    }
}
