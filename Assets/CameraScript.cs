﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
using DG.Tweening;

public class CameraScript : MonoBehaviour
{
    public float speed = 50;
    public float ySpeed = 2;
    public float yMin = 5;
    public float yMax = 30;
    public float minAngle = 34;
    public float maxAngle = 53;

    public float startAngleY;
    public float startX;

    private Transform tr;
    private Side side;

    void Awake()
    {
        tr = transform;
        futureYPos = 6.4f;
    }

    public void SetSide(Side s)
    {
        side = s;
        var angle = tr.localEulerAngles;
        var pos = tr.localPosition;
        pos.x = startX;
        angle.y = -startAngleY;
        if (s == Side.Red)
        {
            angle.y = -angle.y;
            pos.x = -pos.x;
        }
        tr.localEulerAngles = angle;
        tr.localPosition = pos;
    }

    public float scrollEmulDelta = 2;
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
	    {
	        Move(-1);
	    }
	    else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
	    {
	        Move(1);
	    }
	    if (Input.GetKey(KeyCode.W)) 
	    {
	        ChangeHeights(scrollEmulDelta, false);
	    }
        else if (Input.GetKey(KeyCode.S))
        {
	        ChangeHeights(-scrollEmulDelta, false);
        }
	    if (Input.mouseScrollDelta.y != 0)
	    {
	        ChangeHeights(Input.mouseScrollDelta.y, true);
	    }
	}

    void Move(float dir)
    {
         dir = -dir;
        var pos = tr.localPosition;
        pos.x += dir*speed*Time.deltaTime;
        tr.localPosition = pos;
    }

    private Tween tween;
    private float rotTime = 0.5f;
    private float futureYPos;
    void ChangeHeights(float dir, bool anim)
    {
        var posY = Mathf.Clamp(futureYPos + dir*ySpeed, yMin, yMax);
        futureYPos = posY;
        if (anim)
            tr.DOMoveY(posY, rotTime);
        else
            tr.SetLocalPositionY(posY);
        var rot = tr.localEulerAngles;
        rot.x = Mathf.Lerp(minAngle, maxAngle, (posY - yMin)/(yMax - yMin));
        if (anim)
            tr.DORotate(rot, rotTime);
        else
            tr.localEulerAngles = rot;
    }
}
