﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections;
using System.Collections.Generic;

namespace Prototype.NetworkLobby
{
    public class LobbyServerList : MonoBehaviour
    {
        public LobbyManager lobbyManager;

        public RectTransform serverListRect;
        public GameObject serverEntryPrefab;
        public GameObject noServerFound;

        protected int currentPage = 0;
        protected int previousPage = 0;

        static Color OddServerColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        static Color EvenServerColor = new Color(.94f, .94f, .94f, 1.0f);

        void OnEnable()
        {
            currentPage = 0;
            previousPage = 0;

            foreach (Transform t in serverListRect)
                Destroy(t.gameObject);

            noServerFound.SetActive(false);

            RequestPage(0);
        }

		public void OnGUIMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
		{
		    var mm = FindObjectOfType<MainMenu>();
			if (matches.Count == 0)
			{
                if (currentPage == 0)
                {
                    noServerFound.SetActive(true);
                }

                lobbyManager.GoBackButton();
                if (mm != null)
                    mm.infoText.text = "Can't find game. Create a new one.";
                currentPage = previousPage;

                LobbyManager.s_Singleton.mainMenuPanel.GetComponent<LobbyMainMenu>().OnClickCreateMatchmakingGame();
                return;
            }

            if (mm != null)
                mm.infoText.text = "";

            noServerFound.SetActive(false);
            foreach (Transform t in serverListRect)
                Destroy(t.gameObject);

			for (int i = 0; i < 1; ++i)
			{
                GameObject o = Instantiate(serverEntryPrefab) as GameObject;
                var entry = o.GetComponent<LobbyServerEntry>();
				entry.Populate(matches[i], lobbyManager, (i % 2 == 0) ? OddServerColor : EvenServerColor);
				o.transform.SetParent(serverListRect, false);
                entry.JoinMatch(matches[i].networkId, lobbyManager);
            }
        }

        public void ChangePage(int dir)
        {
            int newPage = Mathf.Max(0, currentPage + dir);

            //if we have no server currently displayed, need we need to refresh page0 first instead of trying to fetch any other page
            if (noServerFound.activeSelf)
                newPage = 0;

            RequestPage(newPage);
        }

        public void RequestPage(int page)
        {
            previousPage = currentPage;
            currentPage = page;
			lobbyManager.matchMaker.ListMatches(page, 6, "", true, 0, 0, OnGUIMatchList);
		}
    }
}