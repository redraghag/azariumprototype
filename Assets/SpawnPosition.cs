﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SpawnPosition : BadassNetworkBehaviour {

    public TextMesh name;

    [ClientRpc]
    public void RpcInhabit(int index, string nameStr)
    {
        gameObject.SetActiveSafe(true);
        name.text = nameStr + " " + index;
        GetComponent<MeshRenderer>().material.color = Color.white;
    }
}
