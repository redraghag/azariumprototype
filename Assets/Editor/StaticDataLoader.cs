﻿using System;
using UnityEngine;
using System.Collections;
using System.Net;
using UnityEditor;

class StaticDataLoader : MonoBehaviour {

    static string PathForPage(string name)
    {
        return "Assets/Row/" + name + ".csv";
    }

    [MenuItem("GDTools/ParseData")]
    public static void StaticReloadData()
    {
       try
       {
            GameData d = DataImporter.ParseData(page => new CsvReader(PathForPage(page)));
            GameDataLoader.saveGameData(d);
            AssetDatabase.Refresh();

            GameDataDescriptor gameDataDescriptorFinal = ScriptableObject.CreateInstance<GameDataDescriptor>();
            gameDataDescriptorFinal.data = d;

            AssetDatabase.CreateAsset(gameDataDescriptorFinal, "Assets/Resources/gameData.asset");
            AssetDatabase.SaveAssets();

            //var controller = GameObject.FindObjectOfType<GameController>();
            //if (controller != null)
            //{
            //    Debug.Log("GameController found");
            //    controller.game = gameDataDescriptorFinal;
            //}
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message + e.StackTrace);
        }
    }

    [MenuItem("GDTools/DownloadFromNet")]
    public static void StaticDownloadData()
    {
        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        var client = new WebClient();
        foreach (var page in DataImporter.pagesToLoad)
        {
            Debug.Log("Downdloding " + page.Key + " .... ");
            client.DownloadFile(DataImporter.UrlForPageId(page.Value), PathForPage(page.Key));
        }
        Debug.Log("Download complete");
    } 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
